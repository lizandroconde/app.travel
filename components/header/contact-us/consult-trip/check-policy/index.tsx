//# React
import { Fragment } from "react";
//# React Hooks form
import { FieldError } from "react-hook-form";
//# Icons
import { HiOutlineCheck } from "react-icons/hi";
//# Styles components
import * as C from "./styled";

//# Types props
type ICheckPolicy = {
  value: boolean;
  setValue: (i: boolean) => void;
  error: FieldError | undefined;
};

//# Component => policy
export const CheckPolicy = ({
  value,
  setValue,
  error,
}: ICheckPolicy): JSX.Element => {
  //# Methods
  const onChecked = (): void => {
    setValue(!value);
  };

  return (
    <C.Container>
      <C.Content onClick={onChecked}>
        <C.Checked check={value}>
          {value && (
            <C.Check>
              <HiOutlineCheck />
            </C.Check>
          )}
        </C.Checked>
        <C.Text>
          Al envíar este formulario estás aceptando nuestra
          <C.Linked>Política de privacidad</C.Linked>
        </C.Text>
      </C.Content>
      <C.Error>{error && <Fragment>{error?.message}</Fragment>}</C.Error>
    </C.Container>
  );
};

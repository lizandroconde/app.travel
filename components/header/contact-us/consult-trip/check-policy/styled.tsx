//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: auto;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  column-gap: ${({ theme: { toRem } }) => toRem(8)};
  width: 100%;
  margin-bottom: ${({ theme: { toRem } }) => toRem(30)};
  margin-top: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Checked = styled.div<{ check?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ theme: { toRem } }) => toRem(20)};
  height: ${({ theme: { toRem } }) => toRem(20)};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  border-style: solid;
  background: ${({ check, theme: { colors } }) =>
    check && colors.black.rgb.BLk2C3E50};
`;
export const Check = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  line-height: 120%;
  text-align: left;
`;
export const Linked = styled.span`
  color: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  line-height: 120%;
  margin-left: ${({ theme: { toRem } }) => toRem(4)};
  cursor: pointer;
`;
export const Error = styled.div`
  width: auto;
  height: ${({ theme: { toRem } }) => toRem(14)};
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  font-weight: normal;
  line-height: 120%;
  text-align: left;
  margin-bottom: ${({ theme: { toRem } }) => toRem(5)};
`;

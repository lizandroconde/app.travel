//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Content = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(1)};
`;
export const Selected = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: ${({ theme: { toRem } }) => toRem(45)};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  border-style: solid;
  border-bottom-right-radius: ${({ active }) => active && 0};
  border-bottom-left-radius: ${({ active }) => active && 0};
`;
export const Text = styled.span`
  display: flex;
  flex: 1;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: normal;
  line-height: 120%;
  align-items: center;
  padding-left: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Down = styled.span`
  display: flex;
  width: ${({ theme: { toRem } }) => toRem(25)};
  padding-right: ${({ theme: { toRem } }) => toRem(5)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(10)};
  text-align: center;
  cursor: pointer;
`;
export const Group = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1050;
  width: auto;
  min-width: 100%;
  display: block;
  overflow-y: auto;
  height: auto;
  max-height: ${({ theme: { toRem } }) => toRem(240)};
  border-bottom-right-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-bottom-left-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  border-style: solid;
  margin-top: ${({ theme: { toRem } }) => toRem(-1)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;
export const Item = styled.div<{ active?: boolean }>`
  width: 100%;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: ${({ active }) => active && 600};
  line-height: 120%;
  background: ${({ active, theme: { colors } }) =>
    active && colors.black.rgba.BK000(0.05)};
  padding-top: ${({ theme: { toRem } }) => toRem(8)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(8)};
  padding-left: ${({ theme: { toRem } }) => toRem(10)};
  padding-right: ${({ theme: { toRem } }) => toRem(25)};
  cursor: pointer;
  transition: background 0.2s ease-out;
  &:hover {
    background: ${({ theme: { colors } }) => colors.black.rgba.BK000(0.05)};
  }
`;
export const Error = styled.div`
  width: auto;
  height: ${({ theme: { toRem } }) => toRem(14)};
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  font-weight: normal;
  line-height: 120%;
  text-align: left;
  margin-bottom: ${({ theme: { toRem } }) => toRem(5)};
`;

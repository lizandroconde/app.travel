//# React
import {
  ReactNode,
  useState,
  Children,
  cloneElement,
  PropsWithChildren,
  ReactElement,
  Fragment,
} from "react";
//# React Hooks form
import { FieldError } from "react-hook-form";
//# Rooks
import useOutsideClickRef from "react-cool-onclickoutside";
//# Icons
import { BsFillCaretDownFill } from "react-icons/bs";
//# Styles componets
import * as C from "./styled";

//# Types props
type ISelected = {
  value: string;
  text: React.ReactNode;
};
type ISubjectTrip = {
  value: string;
  setValue: (i: string) => void;
  error: FieldError | undefined;
  children: ReactNode[];
};

//# Component => subject trip
export const SubjectTrip = ({
  value,
  setValue,
  error,
  children,
}: ISubjectTrip) => {
  //# Def
  const def = children.find((child) => {
    const item = child as ReactElement<PropsWithChildren<ISubjectItemProps>>;
    return item.props.children === value;
  }) as ReactElement<PropsWithChildren<ISubjectItemProps>>;
  //# Active
  const active = children.find((child) => {
    const item = child as ReactElement<PropsWithChildren<ISubjectItemProps>>;
    return item.props.selected && item;
  }) as ReactElement<PropsWithChildren<ISubjectItemProps>>;
  //# State
  const [show, setShow] = useState<boolean>(false);
  const [selected, setSelected] = useState<ISelected>();
  //# Methods
  const ref = useOutsideClickRef(() => {
    setShow(false);
  });
  const onShow = (): void => setShow(!show);
  const onSelect = (value: string, text: React.ReactNode): void => {
    setSelected({ value, text });
    setValue(value);
    setShow(false);
  };

  return (
    <C.Container>
      <C.Content ref={ref}>
        <C.Selected active={show} onClick={onShow}>
          <C.Text>{selected?.text || active.props.children || def}</C.Text>
          <C.Down>
            <BsFillCaretDownFill />
          </C.Down>
        </C.Selected>
        {show && (
          <C.Group>
            {Children.map(children, (child) => {
              const item = child as ReactElement<
                PropsWithChildren<ISubjectItemProps>
              >;
              const active: boolean = selected
                ? item.props.value === selected.value
                : item.props.selected
                ? true
                : false;
              const onClick = (): void => {
                onSelect(item.props.value, item.props.children);
              };
              return cloneElement(item, {
                active,
                onClick,
              });
            })}
          </C.Group>
        )}
      </C.Content>
      <C.Error>{error && <Fragment>{error.message}</Fragment>}</C.Error>
    </C.Container>
  );
};

//# Types props
type ISubjectItemProps = {
  value: string;
  active?: boolean;
  selected?: boolean;
  children: ReactNode;
  onClick?: () => void;
};

//# Component => subject item
export const SubjectItem = ({
  active,
  children,
  onClick,
}: ISubjectItemProps): JSX.Element => {
  return (
    <C.Item active={active} onClick={onClick}>
      {children}
    </C.Item>
  );
};

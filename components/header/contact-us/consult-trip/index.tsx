//# React
import { Fragment, useState } from "react";
//# Apollo client
import { useMutation } from "@apollo/client";
import { NEW_ENQUIRY } from "@gql/global/mutations";
//# React Hooks form
import { useForm, Controller } from "react-hook-form";
//# Components
import { CheckPolicy } from "./check-policy";
import { SubjectTrip, SubjectItem } from "./subject-trip";
//# Styles components
import * as C from "./styled";
import { useTranslation } from "next-i18next"

//# Types props
type IStatus = {
  status: number;
  message: string;
};
type IConsultTrip = {
  name: string;
  email: string;
  trip: string;
  subject: string;
  query: string;
  policy: boolean;
};
type IConsultTripProps = {
  onCancel?: () => void;
};

//# Component => consult trip
export const ConsultTrip = ({ onCancel }: IConsultTripProps): JSX.Element => {

  const { t } = useTranslation("common");
  //# States
  const [response, setResponse] = useState<IStatus | undefined>();
  const [loading, setLoading] = useState<boolean>(false);
  //# Hooks
  const {
    reset,
    control,
    register,
    formState: { errors, isDirty, isValid },
    handleSubmit,
  } = useForm<IConsultTrip>({
    mode: "all",
    defaultValues: {
      email: "",
      name: "",
      query: "",
      subject: "",
      trip: "",
      policy: false,
    },
  });
  //# Apollo mutations
  const [newEnquiry] = useMutation(NEW_ENQUIRY);
  //# Methods
  const onBooking = async (data: IConsultTrip): Promise<void> => {
    setLoading(true);
    const { email, name, trip, subject, query } = data;
    const res = await newEnquiry({
      variables: {
        prefix: "es",
        email,
        name,
        trip,
        subject,
        query,
      },
    });
    const status: IStatus = res.data.newInquiry;
    setResponse(status);
    setLoading(false);
    setTimeout(() => {
      setResponse(undefined);
      reset({
        email: "",
        name: "",
        query: "",
        subject: "",
        trip: "",
        policy: false,
      });
    }, 4000);
  };

  return (
    <C.Container>
      <C.Group>
        <C.FormControl>
          <C.Label>{t("contact_us.consultour.name")}</C.Label>
          <C.InputControl
            {...register("name", {
              required: `${t("contact_us.consultour_error.name")}`,
            })}
          />
          <C.Error>
            {errors.name && <Fragment>{errors.name.message}</Fragment>}
          </C.Error>
        </C.FormControl>
        <C.FormControl>
          <C.Label>{t("contact_us.consultour.enteremail")}</C.Label>
          <C.InputControl
            placeholder={t("contact_us.consultour.email")}
            {...register("email", {
              required: `${t("contact_us.consultour_error.email")}`,
              pattern: {
                value: /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/,
                message: `${t("contact_us.consultour_error.email_valid")}`,
              },
            })}
          />
          <C.Error>
            {errors.email && <Fragment>{errors.email.message}</Fragment>}
          </C.Error>
        </C.FormControl>
      </C.Group>
      <C.FormControl>
        <C.Label>{t("contact_us.consultour.trip")}</C.Label>
        <C.InputControl
          {...register("trip", {
            required: `${t("contact_us.consultour_error.trip")}`,
          })}
        />
        <C.Error>
          {errors.trip && <Fragment>{errors.trip.message}</Fragment>}
        </C.Error>
      </C.FormControl>
      <Controller
        control={control}
        name="subject"
        rules={{ required: `${t("contact_us.consultour_error.subject")}` }}
        render={({ field, formState }) => (
          <SubjectTrip
            value={field.value}
            setValue={field.onChange}
            error={formState.errors.subject}
          >
            <SubjectItem key="0" value="" selected>
              {t("contact_us.consultour.asunto.select")}
            </SubjectItem>
            <SubjectItem key="1" value={t("contact_us.consultour.asunto.question")}>
              {t("contact_us.consultour.asunto.question")}
            </SubjectItem>
            <SubjectItem key="2" value={t("contact_us.consultour.asunto.problems")}>
              {t("contact_us.consultour.asunto.problems")}
            </SubjectItem>
            <SubjectItem key="3" value={t("contact_us.consultour.asunto.qoute")}>
              {t("contact_us.consultour.asunto.qoute")}
            </SubjectItem>
          </SubjectTrip>
        )}
      />
      <C.FormControl>
        <C.Label>   {t("contact_us.consultour.consult")}</C.Label>
        <C.TextControl
          rows={5}
          {...register("query", {
            required: `${t("contact_us.consultour_error.query")}`,
          })}
        />
        <C.Error>
          {errors.query && <Fragment>{errors.query.message}</Fragment>}
        </C.Error>
      </C.FormControl>
      <Controller
        control={control}
        name="policy"
        rules={{ required: `${t("contact_us.consultour_error.policy")}` }}
        render={({ field, formState }) => (
          <CheckPolicy
            value={field.value}
            setValue={field.onChange}
            error={formState.errors.policy}
          />
        )}
      />
      <C.Footer>
        {response && (
          <C.Response>
            <C.Status>{response?.status}</C.Status>
            <C.Message>{response?.message}</C.Message>
          </C.Response>
        )}
        <C.Back onClick={onCancel}> {t("contact_us.back")}</C.Back>
        <C.Send
          disabled={!isDirty || !isValid || loading}
          onClick={handleSubmit(onBooking)}
        >
          {loading ? (
            <Fragment>Enviando...</Fragment>
          ) : (
            <Fragment>Enviar</Fragment>
          )}
        </C.Send>
      </C.Footer>
    </C.Container>
  );
};

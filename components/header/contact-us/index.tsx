//# React
import {
  Fragment,
  FormEvent,
  ReactNode,
  useState,
  cloneElement,
  ReactElement,
  PropsWithChildren,
} from "react";
//# Rooks
import useOutsideClickRef from "react-cool-onclickoutside";
//# React modal
import ReactModal from "react-modal";
ReactModal.setAppElement("#__next");
//# Icons
import { IoCloseOutline } from "react-icons/io5";
import { BsBookmarkCheck } from "react-icons/bs";
import { TiThLargeOutline } from "react-icons/ti";
import { AiOutlineComment } from "react-icons/ai";
//# Components
import { CheckBooking } from "@components/header/contact-us/check-booking";
import { ConsultTrip } from "@components/header/contact-us/consult-trip";
import { OtherSubject } from "@components/header/contact-us/other-subject";
//# Styles components
import * as C from "./styled";

import { useTranslation } from "next-i18next"

//# Types props
type ICancelProps = {
  onCancel?: () => void;
};
type IConsult = {
  icon: ReactNode;
  text: string;
  children: ReactNode;
};
type IProps = {
  show: boolean;
  setShow: (i: boolean) => void;
};

//# Component => contact us
export const ContactUs = ({ show, setShow }: IProps): JSX.Element => {
  //# States
  const [index, setIndex] = useState<number>(-1);

  const { t } = useTranslation("common")

  //# Data
  const consult: IConsult[] = [
    {
      icon: <BsBookmarkCheck />,
      text: t("contact_us.query"),
      children: <CheckBooking />,
    },
    {
      icon: <TiThLargeOutline />,
      text: t("contact_us.tourquery"),
      children: <ConsultTrip />,
    },
    {
      icon: <AiOutlineComment />,
      text: t("contact_us.others"),
      children: <OtherSubject />,
    },
  ];

  //# Hooks
  const ref = useOutsideClickRef((): void => {
    setShow(false);
    onCancel();
    document.body.removeAttribute("style");
  });
  //# methods
  const onClose = (e: FormEvent<HTMLSpanElement>): void => {
    e.stopPropagation();
    setShow(false);
    onCancel();
    document.body.removeAttribute("style");
  };
  const onIndex = (i: number): void => setIndex(i);
  const onCancel = (): void => setIndex(-1);
  const children: ReactNode = index !== -1 && consult[index].children;
  const child = children as ReactElement<PropsWithChildren<ICancelProps>>;



  return (
    <ReactModal
      onAfterOpen={() => {
        document.body.style.overflow = "hidden";
      }}
      overlayElement={() => (
        <C.Container>
          <C.Center ref={ref}>
            <C.Content>
              <C.Header>
                <C.Title>{t("contact_us.title")}</C.Title>
                <C.Close onClick={onClose}>
                  <IoCloseOutline />
                </C.Close>
              </C.Header>
              {index !== -1 ? (
                <Fragment>
                  {cloneElement(child, {
                    onCancel,
                  })}
                </Fragment>
              ) : (
                <C.Contact>
                  {consult.map(
                    ({ icon, text }: IConsult, e: number): JSX.Element => {
                      const active: boolean = index === e;
                      return (
                        <C.Topic
                          key={e}
                          active={active}
                          onClick={() => onIndex(e)}
                        >
                          <C.Icon>{icon}</C.Icon>
                          <C.Text>{text}</C.Text>
                        </C.Topic>
                      );
                    }
                  )}
                </C.Contact>
              )}
            </C.Content>
          </C.Center>
        </C.Container>
      )}
      isOpen={show}
    />
  );
};


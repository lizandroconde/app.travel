//# Styled component
import styled from "styled-components";

export const Container = styled.div`
  background: ${({ theme }) => theme.colors.white.rgba.W255(0.65)};
  width: 100%;
  height: 100%;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  position: fixed;
  z-index: 10104;
`;
export const Center = styled.div`
  max-width: 100%;
  height: 100%;
  position: fixed;
  top: 50%;
  left: 50%;
  bottom: auto;
  right: auto;
  transform: translate(-50%, -50%);
  z-index: 10104;
  height: auto;
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    height: auto;
  }
`;
export const Content = styled.div`
  display: block;
  width: ${({ theme: { toRem } }) => toRem(850)};
  max-width: 100%;
  padding: ${({ theme: { toRem } }) => toRem(34)};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  overflow: auto;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  box-shadow: ${({ theme: { toRem, colors } }) => `
  0 ${toRem(11)} ${toRem(15)} ${toRem(-7)} ${colors.black.rgba.BK000(0.2)},
    0 ${toRem(24)} ${toRem(38)} ${toRem(3)} ${colors.black.rgba.BK000(0.14)},
    0 ${toRem(9)} ${toRem(46)} ${toRem(8)} ${colors.black.rgba.BK000(0.12)};
  `};
`;
export const Header = styled.div`
  position: relative;
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK555};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
  border-bottom-width: ${({ theme: { toRem } }) => toRem(1)};
  border-bottom-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYDDD};
  border-bottom-style: solid;
  padding-bottom: ${({ theme: { toRem } }) => toRem(15)};
  margin-top: ${({ theme: { toRem } }) => toRem(-5)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(20)};
`;
export const Close = styled.span`
  display: flex;
  position: absolute;
  cursor: pointer;
  top: ${({ theme: { toRem } }) => toRem(-10)};
  right: ${({ theme: { toRem } }) => toRem(-15)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK555};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  line-height: 120%;
`;
export const Contact = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-left: ${({ theme: { toRem } }) => toRem(-8)};
  flex-wrap: wrap;
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    flex-wrap: nowrap;
  }
`;
export const Topic = styled.div<{ active?: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: ${({ theme: { toRem } }) => toRem(15)};
  width: 100%;
  padding: ${({ theme: { toRem } }) => toRem(15)};
  margin-left: ${({ theme: { toRem } }) => toRem(8)};
  margin-right: ${({ theme: { toRem } }) => toRem(8)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYDDD};
  border-style: solid;
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  background: ${({ active, theme: { colors } }) =>
    active && colors.gray.rgb.GRYF2F3F5};
  cursor: pointer;
  transition: background 0.1s ease-in-out;
  &:hover {
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYF2F3F5};
  }
`;
export const Icon = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK555};
  font-size: ${({ theme: { toRem } }) => toRem(65)};
  line-height: 120%;
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  line-height: 120%;
  text-align: center;
`;

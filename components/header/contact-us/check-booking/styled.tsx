//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: auto;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Group = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(30)};
`;
export const FormControl = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
`;
export const Label = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 500;
  line-height: 120%;
`;
export const InputControl = styled.input`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(45)};
  padding-left: ${({ theme: { toRem } }) => toRem(15)};
  padding-right: ${({ theme: { toRem } }) => toRem(15)};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  border-style: solid;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
  font-weight: 500;
  line-height: 120%;
  transition: border-color 0.3s, border 0.3s ease-in-out;
  &:focus {
    border-color: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  }
  &::placeholder {
    color: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  }
`;
export const Error = styled.div`
  width: auto;
  height: ${({ theme: { toRem } }) => toRem(14)};
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  font-weight: normal;
  line-height: 120%;
  text-align: left;
`;
export const Footer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(30)};
`;
export const Message = styled.div`
  width: 100%;
  margin-bottom: ${({ theme: { toRem } }) => toRem(30)};
  margin-top: ${({ theme: { toRem } }) => toRem(10)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 300;
  line-height: 120%;
`;
export const Back = styled.button`
  width: ${({ theme: { toRem } }) => toRem(200)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(17)};
  font-weight: 500;
  line-height: 120%;
  cursor: pointer;
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYDDD};
  border-style: solid;
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  padding-top: ${({ theme: { toRem } }) => toRem(15)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(15)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  box-shadow: ${({ theme: { toRem, colors } }) => `
   ${toRem(3)} ${toRem(3)} ${toRem(3)} 0 ${colors.black.rgba.BK000(0.1)}
  `};
`;
export const Send = styled(Back)`
  background: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-color: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  float: right;
  &:disabled {
    opacity: 0.45;
    cursor: not-allowed;
  }
`;

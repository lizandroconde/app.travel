//# React
import { Fragment, useState } from "react";
//# React Hooks form
import { useForm } from "react-hook-form";
import { useTranslation } from "next-i18next"
//# Styles components
import * as C from "./styled";

//# Types props
type ICheckBooking = {
  booking: string;
  email: string;
};
type ICheckBookingProps = {
  onCancel?: () => void;
};

//# Component => check booking
export const CheckBooking = ({ onCancel }: ICheckBookingProps): JSX.Element => {
  //# States
  const [loading, setLoading] = useState<boolean>(false);

  const { t } = useTranslation("common")
  //# Hooks
  const {
    register,
    formState: { errors, isDirty, isValid },
    handleSubmit,
  } = useForm<ICheckBooking>({
    mode: "all",
  });
  //# Methods
  const onBooking = async (data: ICheckBooking): Promise<void> => {
    setLoading(true);
    
    setLoading(false);
  };

  return (
    <C.Container>
      <C.Group>
        <C.FormControl>
          <C.Label>{t("contact_us.consult.cod")}</C.Label>
          <C.InputControl
            placeholder="CT-XXXXXX"
            {...register("booking", {
              required: `${t("contact_us.consult_error.cod")}`,
            })}
          />
          <C.Error>
            {errors.booking && <Fragment>{errors.booking.message}</Fragment>}
          </C.Error>
        </C.FormControl>
        <C.FormControl>
          <C.Label>{t("contact_us.consult.email")}</C.Label>
          <C.InputControl
            placeholder="micorreo@gmail.com"
            {...register("email", {
              required: `${t("contact_us.consult_error.email")}`,
            })}
          />
          <C.Error>
            {errors.email && <Fragment>{errors.email.message}</Fragment>}
          </C.Error>
        </C.FormControl>
      </C.Group>
      <C.Message>{t("contact_us.consult_error.click")}</C.Message>
      <C.Footer>
        <C.Back onClick={onCancel}>{t("contact_us.back")}</C.Back>
        <C.Send
          disabled={!isDirty || !isValid || loading}
          onClick={handleSubmit(onBooking)}
        >
          {t("contact_us.next")}
        </C.Send>
      </C.Footer>
    </C.Container>
  );
};

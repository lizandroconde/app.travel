//# React
import { useState, useEffect, useContext } from "react";
//# Next
import Image from "next/image";
import {useRouter} from "next/router";
import { Router,Link } from "@routes";
 
//# Media
import { useMedia } from "@hooks/useMedia";
//# Interfaces
import { ICountry } from "@interfaces/country";
import { ITravelStyles } from "@interfaces/travel-styles";
//# Apollo client
import { useQuery } from "@apollo/client";
import { GET_ATTRACTTION_HEADER, GET_DESTINATIONS } from "@gql/global/querys";
//# Components
import { Navbar } from "@components/header/navbar";
import { MenuItem } from "@components/header/menu-item";
import {
  MenuNavDestinations,
  MenuNavTravelStyles,
} from "@components/header/menu-nav";
import { MenuList, ListItem, LanguageMenu } from "@components/header/menu-list";
import { ContactUs } from "@components/header/contact-us";
//# Translate
import { useTranslation } from "next-i18next";
//# Styles components
import * as C from "./styled";
import { HeaderContext } from "@hooks/useContext";
import { ILangueages } from "@interfaces/langueages";

//# Types props
type IProps = {};

//# Component => header
export const Header = ({}: IProps): JSX.Element => {
  
  const { t } = useTranslation("common");
  const [menu, setMenu] = useState<boolean>(false);
  const [contact, setContact] = useState<boolean>(false);
  const [destinations, setDestinations] = useState<ICountry[]>([]);
  const [attracctions, setAttraction] = useState<ITravelStyles[]>([]);
  const match: boolean = useMedia("(max-width: 1024px)");
    const { languages } = useContext(HeaderContext);

  const onContact = (): void => setContact(true);
  const onMenu = (): void => setMenu(true);

  const router = useRouter()

  const locale = router.query?.locale
  const { data: getDesti } = useQuery(GET_DESTINATIONS, {variables:{ slug: locale || "en"}});
  const { data: getAttactions } = useQuery(GET_ATTRACTTION_HEADER, {variables:{ slug: locale || "en"}});
 
  //# Prerender
  useEffect(() => {
    const getDestinations: ICountry[] =
      getDesti && getDesti.getDestinations.data;
    const getAtracctions: ITravelStyles[] =
    getAttactions && getAttactions.getAllAtractionsHeader.data;
    if (getDestinations && getAtracctions) {
      setDestinations(getDestinations);
      setAttraction(getAtracctions);
    } else {
      setDestinations([]);
      setAttraction([]);
    }
   
  }, [getDesti, getAttactions]);
 

  const changeLange = ( ) =>{
   
    router.push(`/${locale|| "en"}`)
  }

  return (
    <C.Container>
      {match && (
        <Navbar
          destinations={destinations}
          travelStyles={attracctions}
          active={menu}
          setMenu={setMenu}
        />
      )}
      <C.Auto>
        {match && (
          <C.Burger onClick={onMenu}>
            <C.King />
            <C.King />
            <C.King />
          </C.Burger>
        )}
        <C.Logo onClick={changeLange}>
          <Image
            width={140}
            height={35}
            layout="fixed"
            src="/logo.jpeg"
            alt="Conde Travel E.I.R.L"
          />
        </C.Logo>
        {!match && (
          <C.Menu>
            <MenuItem title={t("header.travelstyles")}>
              <MenuNavTravelStyles travelStyles={attracctions} />
            </MenuItem>

            <MenuItem title={t("header.destinations")}>
              <MenuNavDestinations destinations={destinations} />
            </MenuItem>
            
            <MenuItem relative title={t("header.laguages")}>
            <LanguageMenu>
                
                {languages?.map(
                  ({ cod, slug, url }: ILangueages, index: number) => {

                    return (
                     <Link key={index} route={url}>
                       <a>
                      <C.SectionDeal >
                       
                        <Image
                          src={`https://flagcdn.com/32x24/${cod}.png`}
                          alt={slug}
                          width={23}
                          height={18}
                          layout="fixed"
                        /> &nbsp; 
                        {t(`header.${slug}`)}</C.SectionDeal>
                        </a>
                      </Link>
                    );
                  }
                )}
                </LanguageMenu>
            </MenuItem>
            <C.Item border onClick={onContact}>
              <C.Text>{t("header.contact_us")}</C.Text>
            </C.Item>
          </C.Menu>
        )}
        <ContactUs show={contact} setShow={setContact} />
      </C.Auto>
    </C.Container>
  );
};

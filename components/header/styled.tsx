//# Styled components
import styled from "styled-components";

export const Container = styled.header`
  position: relative;
  z-index: 4;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-bottom-style: solid;
  border-top-style: solid;
`;
export const Auto = styled.div`
  display: flex;
  width: 100%;
  max-width: ${({ theme: { toRem } }) => toRem(1440)};
  height: ${({ theme: { toRem } }) => toRem(56)};
  padding-left: ${({ theme: { toRem } }) => toRem(10)};
  padding-right: ${({ theme: { toRem } }) => toRem(10)};
  margin-left: auto;
  margin-right: auto;
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(40)};
    padding-right: ${({ theme: { toRem } }) => toRem(40)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(40)};
    padding-right: ${({ theme: { toRem } }) => toRem(40)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(36)};
    padding-right: ${({ theme: { toRem } }) => toRem(36)};
  }
`;
export const Burger = styled.div`
  position: relative;
  padding-top: ${({ theme: { toRem } }) => toRem(15)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(15)};
  margin-right: ${({ theme: { toRem } }) => toRem(16)};
  cursor: pointer;
`;
export const King = styled.div`
  border-radius: ${({ theme: { toRem } }) => toRem(100)};
  width: ${({ theme: { toRem } }) => toRem(25)};
  height: ${({ theme: { toRem } }) => toRem(2)};
  margin-top: ${({ theme: { toRem } }) => toRem(5)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYC7D0D9};
  transition: background 0.15s ease-in-out;
  ${Burger}:hover & {
    background: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  }
`;
export const Logo = styled.div`
  display: flex;
  cursor: pointer;
  align-items: center;
  width: auto;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  height: ${({ theme: { toRem } }) => toRem(54)};
  padding-left: 0;
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
  margin-right: auto;
  margin-top: ${({ theme: { toRem } }) => toRem(1)};
`;
export const Menu = styled.ul`
  width: auto;
  height: 100%;
  list-style: none;
  display: flex;
  align-items: center;
`;
type IItemProps = {
  relative?: boolean;
  border?: boolean;
};
export const Item = styled.li<IItemProps>`
  position: ${({ relative }) => relative && "relative"};
  width: auto;
  height: 100%;
  float: left;
  align-items: center;
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  padding-right: ${({ theme: { toRem } }) => toRem(8)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: transparent;
  border-style: solid;
  margin-bottom: ${({ theme: { toRem } }) => toRem(-1)};
  &:hover {
    border-left-color: ${({ border, theme: { colors } }) =>
      !border && colors.gray.rgb.GRYEBEEF2};
    border-right-color: ${({ border, theme: { colors } }) =>
      !border && colors.gray.rgb.GRYEBEEF2};
    border-bottom-color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
    cursor: pointer;
  }
`;
export const Text = styled.span`
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: ${({ theme: { toRem } }) => toRem(56)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  ${Item}:hover & {
    color: ${({ theme: { colors } }) => colors.blue.rgb.BL286283};
  }
`;
export const NavDeal = styled.div`
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  margin-top: ${({ theme: { toRem } }) => toRem(1)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-style: solid;
  border-top-style: none;
  position: absolute;
  top: 100%;
  right: 0;
  z-index: 5;
`;
export const ToDeal = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  height: 100%;
  a{
    text-decoration: none;
    color: inherit;
  }
`;
export const SectionDeal = styled.li`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: ${({ theme: { toRem } }) => toRem(150)};
  white-space: nowrap;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  text-align: left;
  font-weight: 400;
  line-height: ${({ theme: { toRem } }) => toRem(17)};
  padding-top: ${({ theme: { toRem } }) => toRem(12)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  transition: background 0.1s linear, color 0.1s linear;
  &:hover {
    background: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
    color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  }
`;
export const Nav = styled.div`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(415)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-top: ${({ theme: { toRem } }) => toRem(35)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(35)};
  cursor: auto;
  overflow: hidden;
  margin-top: ${({ theme: { toRem } }) => toRem(1)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-style: solid;
  border-top-style: none;
  position: absolute;
  top: 100%;
  right: 0;
  z-index: 5;
  box-shadow: 0 16px 24px rgb(3 54 63 / 16%), 0 8px 16px rgb(3 54 63 / 4%), 0 4px 8px rgb(3 54 63 / 4%), 0 2px 4px rgb(3 54 63 / 2%), 0 1px 2px rgb(3 54 63 / 4%), 0 -1px 2px rgb(3 54 63 / 4%);
`;

export const Top = styled.ul`
  display: flex;
  justify-content: flex-start;
  flex-direction: row;
  max-width: ${({ theme: { toRem } }) => toRem(1164)};
  height: 100%;
  margin: 0 auto;
  position: relative;
  list-style: none;
`;


export const TopAttractions = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
 
  max-width: ${({ theme: { toRem } }) => toRem(1164)};
 
  margin: 0 auto;
  list-style: none;
  
`;

export const Paragrpah = styled.p`
  display: flex;
  align-items: center;
`



export const Section = styled.li`
  width: ${({ theme: { toRem } }) => toRem(170)};
  max-width: ${({ theme: { toRem } }) => toRem(185)}; 
  float: left;
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  padding-right: ${({ theme: { toRem } }) => toRem(8)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: transparent;
  border-style: solid;
 
  margin-bottom: ${({ theme: { toRem } }) => toRem(-1)};
`;


export const SectionAtracction = styled.li`
  margin-top: 10px;
  float: left;
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  padding-right: ${({ theme: { toRem } }) => toRem(8)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: transparent;
  border-style: solid;
 
  margin-bottom: ${({ theme: { toRem } }) => toRem(-1)};
`;
export const Title = styled.h3`
  display: inline-block;
  white-space: nowrap;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  line-height: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 700;
  text-align: left;
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  padding-top: ${({ theme: { toRem } }) => toRem(13)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(13)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(10)};
  margin-right: auto;
  margin-left: auto;
  transition: color 0.1s linear;
  cursor: pointer;
  &:hover {
    color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  }
`;


export const TitleImage = styled.div`
  cursor: pointer;
  display: grid;
  grid-template-columns: 100px 200px;
  grid-gap: 1rem;;
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  font-weight: 700;
   img{
    border-radius: 10px;
   }

   &:hover {
    color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  }

`;


export const SubTop = styled.ul`
  width: 100%;
  max-height: ${({ theme: { toRem } }) => toRem(330)};
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  margin-top: ${({ theme: { toRem } }) => toRem(30)};
  list-style: none;
  a{
    text-decoration: none;
    color:inherit;
  }
`;
export const SubSection = styled.li`
text-decoration: none;
  display: block;
  width: 100%;
  height: auto;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  text-align: left;
  font-weight: 400;
  line-height: ${({ theme: { toRem } }) => toRem(17)};
  padding-top: ${({ theme: { toRem } }) => toRem(12)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  transition: background 0.1s linear, color 0.1s linear;
  cursor: pointer;
  a{
    text-decoration:none;
    color: inherit;
  }
  &:hover {
    color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
    background: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  }
`;
export const AllSection = styled.li`
  width: 100%;
  height: auto;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: ${({ theme: { toRem } }) => toRem(23)};
  text-align: left;
  font-weight: 700;
  padding-top: ${({ theme: { toRem } }) => toRem(13)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(13)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  border-top-width: ${({ theme: { toRem } }) => toRem(1)};
  border-top-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-top-style: solid;
  transition: color 0.1s linear;
  cursor: pointer;
  &:hover {
    color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  }
`;

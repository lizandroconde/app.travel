//# React
import { useState, ReactNode } from "react";
//# Styles components
import * as C from "@components/header/styled";

//# Types props
type IItemProps = {
  relative?: boolean;
  title: string;
  children?: ReactNode;
};

//# Component => menu item
export const MenuItem = ({
  relative,
  title,
  children,
}: IItemProps): JSX.Element => {
  //# Hooks
  const [show, setShow] = useState<boolean>(false);
  //# Methods
  const onShow = (): void => setShow(true);
  const onHide = (): void => setShow(false);

  return (
    <C.Item relative={relative} onMouseOver={onShow} onMouseLeave={onHide}>
      <C.Text>{title}</C.Text>
      {show && children}
    </C.Item>
  );
};

//# Styled components
import styled from "styled-components";

export const Container = styled.div<{ active?: boolean }>`
  position: fixed;
  left: ${({ active, theme: { toRem } }) => (active ? 0 : toRem(-260))};
  top: 0;
  z-index: 1001;
  width: ${({ theme: { toRem } }) => toRem(260)};
  height: 100%;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  overflow: hidden;
  transition: all 0.2s ease-in-out;
`;
export const Auth = styled.div`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(110)};
  padding-top: ${({ theme: { toRem } }) => toRem(30)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(30)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  background: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const LogIn = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ theme: { toRem } }) => toRem(50)};
  height: ${({ theme: { toRem } }) => toRem(50)};
  background: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  border-width: ${({ theme: { toRem } }) => toRem(2)};
  border-color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-style: solid;
  border-radius: 50%;
  margin: auto;
`;
export const User = styled.span`
  display: flex;
  font-size: ${({ theme: { toRem } }) => toRem(30)};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;
export const Group = styled.div`
  height: calc(100% - ${({ theme: { toRem } }) => toRem(110)});
  overflow-y: scroll;
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
`;
export const List = styled.div<{ bold?: boolean }>`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(43)};
  font-weight: ${({ bold }) => (bold ? 700 : 400)};
  text-align: center;
  cursor: pointer;
`;
export const Item = styled.span`
  display: block;
  padding-top: ${({ theme: { toRem } }) => toRem(13)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(13)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-right: ${({ theme: { toRem } }) => toRem(20)};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  line-height: 120%;
`;
export const Space = styled.div`
  border-top-width: ${({ theme: { toRem } }) => toRem(1)};
  border-top-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYC7D0D9};
  border-top-style: solid;
  margin-top: ${({ theme: { toRem } }) => toRem(10)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Drop = styled.span`
  display: flex;
  float: right;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
`;
export const Backdrop = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  width: 100%;
  left: 0;
  top: ${({ theme: { toRem } }) => toRem(110)};
  padding: ${({ theme: { toRem } }) => toRem(15)};
  height: ${({ theme: { toRem } }) => toRem(50)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;
export const Back = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(26)};
  margin-right: ${({ theme: { toRem } }) => toRem(40)};
`;
export const TabGroup = styled.div<{ active?: boolean }>`
  width: 100%;
  background-color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  position: absolute;
  left: ${({ active, theme: { toRem } }) => (active ? 0 : toRem(256))};
  top: ${({ theme: { toRem } }) => toRem(160)};
  z-index: 6;
  transition: all 0.2s ease-in-out;
  height: calc(100% - ${({ theme: { toRem } }) => toRem(160)});
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
`;
export const TabSubGroup = styled.div<{ active?: boolean }>`
  width: 100%;
  background-color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  position: absolute;
  left: ${({ active, theme: { toRem } }) => (active ? 0 : toRem(260))};
  top: 0;
  z-index: 6;
  transition: all 0.2s ease-in-out;
  overflow-x: hidden;
  height: 100%;
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
`;
export const SubBackdrop = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  width: 100%;
  left: 0;
  top: ${({ theme: { toRem } }) => toRem(-50)};
  padding: ${({ theme: { toRem } }) => toRem(15)};
  height: ${({ theme: { toRem } }) => toRem(50)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;

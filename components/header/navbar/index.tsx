//# React
import { FormEvent, useContext, useState } from "react";
//# Next routes
import { useRouter } from "next/router";
//# Rooks
import useOutsideClickRef from "react-cool-onclickoutside";
//# Interfaces
import { ICountry } from "@interfaces/country";
import { ICity } from "@interfaces/city";
import { ITravelStyles } from "@interfaces/travel-styles";
//# Icons
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
import { AiOutlineUser } from "react-icons/ai";
//# Styles components
import * as C from "./styled";
import { useTranslation } from "next-i18next"
import { HeaderContext } from "@hooks/useContext";

//# Types props
type ITab = {
  menu: number;
  submenu: number;
};
type INavbarProps = {
  destinations: ICountry[];
  travelStyles: ITravelStyles[];
  active?: boolean;
  setMenu: (i: boolean) => void;
};

//# Component => navbar
export const Navbar = ({
  destinations,
  travelStyles,
  active,
  setMenu,
}: INavbarProps): JSX.Element => {
  const { languages } = useContext(HeaderContext);
  const { t } = useTranslation("common")
  //# Hooks
  const router = useRouter();
  const ref = useOutsideClickRef((): void => {
    setMenu(false);
  });
  //# States
  const [tab, setTab] = useState<ITab>({
    menu: -1,
    submenu: -1,
  });
  //# Methods
  const onRoute = (path: string): void => {
    router.push(path);
  };
  const onBack = (e: FormEvent, tab: ITab): void => {
    e.stopPropagation();
    setTab(tab);
  };
  const onTab = (e: FormEvent, tab: ITab): void => {
    e.stopPropagation();
    setTab(tab);
  };

  return (
    <C.Container ref={ref} active={active}>
      <C.Auth>
        <C.LogIn>
          <C.User>
            <AiOutlineUser />
          </C.User>
        </C.LogIn>
      </C.Auth>
      <C.Group>
        <C.List bold onClick={(e) => onTab(e, { menu: 3, submenu: -1 })}>
          <C.Item>
            {t("header.laguages")}
            <C.Drop>
              <FiChevronRight />
            </C.Drop>
            {tab.menu === 3 && tab.submenu === -1 && (
              <C.Backdrop onClick={(e) => onBack(e, { menu: -1, submenu: -1 })}>
                <C.Back>
                  <FiChevronLeft />
                </C.Back>
              </C.Backdrop>
            )}
          </C.Item>
           
          <C.TabGroup active={tab.menu === 3}>
            {languages?.map((item,index) =>(
              <C.List bold key={index} onClick={() => onRoute(item.url)}>
              <C.Item> {t(`header.${item?.slug}`)}</C.Item>
            </C.List>
            ))}
            
            
          </C.TabGroup>
        </C.List>
        <C.Space />
        <C.List>
          <C.Item> {t("header.login")}</C.Item>
        </C.List>
        <C.List>
          <C.Item> {t("header.signup")}</C.Item>
        </C.List>

        <C.Space />
        <C.List bold onClick={(e) => onTab(e, { menu: 2, submenu: -1 })}>
          <C.Item>
            {t("header.travelstyles")}
            <C.Drop>
              <FiChevronRight />
            </C.Drop>
            {tab.menu === 2 && tab.submenu === -1 && (
              <C.Backdrop onClick={(e) => onBack(e, { menu: -1, submenu: -1 })}>
                <C.Back>
                  <FiChevronLeft />
                </C.Back>
              </C.Backdrop>
            )}
          </C.Item>
          <C.TabGroup active={tab.menu === 2}>
            {travelStyles.map(
              (travel: ITravelStyles, e: number): JSX.Element => {
                return (
                  <C.List bold key={e} onClick={() => onRoute(travel.Url)}>
                    <C.Item>{travel.Name}</C.Item>
                  </C.List>
                );
              }
            )}
          </C.TabGroup>
        </C.List>
        <C.List bold onClick={(e) => onTab(e, { menu: 1, submenu: -1 })}>
          <C.Item>
            {t("header.destinations")}
            <C.Drop>
              <FiChevronRight />
            </C.Drop>
            {tab.menu === 1 && tab.submenu === -1 && (
              <C.Backdrop onClick={(e) => onBack(e, { menu: -1, submenu: -1 })}>
                <C.Back>
                  <FiChevronLeft />
                </C.Back>
              </C.Backdrop>
            )}
            <C.TabGroup active={tab.menu === 1}>
              {destinations.map((d: ICountry, index: number): JSX.Element => {
                return (
                  <C.List
                    bold
                    key={index}
                    onClick={(e) => onTab(e, { menu: 1, submenu: index })}
                  >
                    <C.Item>
                      {d.Name}
                      <C.Drop>
                        <FiChevronRight />
                      </C.Drop>
                      {tab.submenu === index && (
                        <C.SubBackdrop
                          onClick={(e) => onBack(e, { menu: 1, submenu: -1 })}
                        >
                          <C.Back>
                            <FiChevronLeft />
                          </C.Back>
                        </C.SubBackdrop>
                      )}
                      <C.TabSubGroup active={tab.submenu === index}>
                        {d.Citys.map((city: ICity, e: number): JSX.Element => {
                          return (
                            <C.List key={e} onClick={() => onRoute(city.Url)}>
                              <C.Item>{city.Name}</C.Item>
                            </C.List>
                          );
                        })}
                      </C.TabSubGroup>
                    </C.Item>
                  </C.List>
                );
              })}
            </C.TabGroup>
          </C.Item>
        </C.List>
       

        <C.List bold>
          <C.Item>   {t("header.contact_us")}</C.Item>
        </C.List>
      </C.Group>
    </C.Container>
  );
};

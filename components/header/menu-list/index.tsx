//# React
import {
  ReactNode,
  Children,
  cloneElement,
  ReactElement,
  PropsWithChildren,
} from "react";
//# Next
import { useRouter } from "next/router";
//# Styles components
import * as C from "@components/header/styled";

//# Types props
type IMenuListProps = {
  children: React.ReactNode;
};

export const LanguageMenu = ({children}:IMenuListProps) =>{
 
    return (
      <C.NavDeal>
      <C.ToDeal>
        {children}
      </C.ToDeal>
    </C.NavDeal>
    )
}

//# Component => nav list
export const MenuList = ({ children }: IMenuListProps): JSX.Element => {
  //# Hooks
  const router = useRouter();

  return (
    <C.NavDeal>
      <C.ToDeal>
        {Children.map(children, (child) => {
          const item = child as ReactElement<PropsWithChildren<IListProps>>;
          const onClick = (): void => {
            //router.replace(item.props.value);
            
          };
          return cloneElement(item, {
            onClick,
          });
        })}
      </C.ToDeal>
    </C.NavDeal>
  );
};

//# Types props
type IListProps = {
  onClick?: () => void;
  children: ReactNode;
  value: string;
};

//# Component => list
export const ListItem = ({ onClick, children }: IListProps): JSX.Element => {
  return <C.SectionDeal onClick={onClick}>{children}</C.SectionDeal>;
};

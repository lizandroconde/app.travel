//# React router
import { useRouter } from "next/router";
//# Interfaces
import { ICountry } from "@interfaces/country";
import { ICity } from "@interfaces/city";
import { ITravelStyles } from "@interfaces/travel-styles";
//# Styled components
import * as C from "@components/header/styled";
import { Router, Link } from "@routes";
import Image from "next/image";

//# Types props
type INavDestinationsProps = {
  destinations: ICountry[];
};

//# Component => menu nav destinations
export const MenuNavDestinations = ({
  destinations,
}: INavDestinationsProps): JSX.Element => {
  //# Hooks
  const router = useRouter();
  //# Methods
  const onRoute = (path: string): void => {
    router.push(path);
  };
  return (
    <C.Nav>
      <C.Top>
        {destinations.map((travel: ICountry, e: number): JSX.Element => {
          const splitCity: ICity[] =
            travel.Citys.length > 6 ? travel.Citys.slice(0, 6) : travel.Citys;
          const seeAll: boolean = travel.Citys.length > 6 ? true : false;
          return (
            <C.Section key={e}>
               
              <C.Title>
                
                {travel.Name}
                
              </C.Title>
               
              <C.SubTop>
                {splitCity.map((city: ICity, f: number): JSX.Element => {
                  return (
                   
                    <Link route={city.Url}  key={f} >
                    <a> 
                      <C.SubSection>
                     
                      {city.Name}
                    
                    </C.SubSection>
                    </a>
                    </Link>
                    
                  );
                })}
                {seeAll && (
                  <C.AllSection onClick={() => onRoute(travel.Url)}>
                    See all
                  </C.AllSection>
                )}
              </C.SubTop>
            </C.Section>
          );
        })}
      </C.Top>
    </C.Nav>
  );
};

//# Types props
type INavTravelStylesProps = {
  travelStyles: ITravelStyles[];
};

//# Component => menu nav travel styles
export const MenuNavTravelStyles = ({
  travelStyles,
}: INavTravelStylesProps): JSX.Element => {
  //# Hooks
  const router = useRouter();
  //# Methods
  const onRoute = (path: string): void => {
    router.push(path);
  };
  return (
    <C.Nav>
      <C.TopAttractions>
        {travelStyles.map((travel: ITravelStyles, e: number): JSX.Element => {
          return (
            <C.SectionAtracction key={e}>
              <C.TitleImage onClick={() => onRoute(travel.Url)}>
                <Image src={`${travel.Image}`} alt={travel.Name} width="100px" height="60px"  layout="fixed" />
                <C.Paragrpah> {travel.Name}</C.Paragrpah>
              </C.TitleImage>
            </C.SectionAtracction>
          );
        })}
      </C.TopAttractions>
    </C.Nav>
  );
};

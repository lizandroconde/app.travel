//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  box-shadow: ${({ theme: { toRem, colors } }) =>
    `0 ${toRem(1)} ${toRem(2)} 0 ${colors.black.rgba.BK000(0.25)}`};
  overflow: hidden;
`;
export const Poster = styled.div`
  position: relative;
  width: 100%;
  cursor: pointer;
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  height: ${({ theme: { toRem } }) => toRem(185)};
`;
export const Badge = styled.div<{ bg?: string }>`
  position: absolute;
  top: ${({ theme: { toRem } }) => toRem(16)};
  left: ${({ theme: { toRem } }) => toRem(16)};
  z-index: 1;
  width: auto;
  background: ${({ bg, theme: { colors } }) =>
    bg ? bg : colors.blue.rgb.BL4775250};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: 120%;
  text-align: center;
  padding-top: ${({ theme: { toRem } }) => toRem(4)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(4)};
  padding-left: ${({ theme: { toRem } }) => toRem(12)};
  padding-right: ${({ theme: { toRem } }) => toRem(12)};
  border-radius: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Heart = styled.div<{ active?: boolean }>`
  position: absolute;
  top: ${({ theme: { toRem } }) => toRem(16)};
  right: ${({ theme: { toRem } }) => toRem(16)};
  z-index: 1;
  display: flex;
  color: ${({ active, theme: { colors } }) =>
    active ? colors.yellow.rgb.YWffe958 : colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(30)};
  cursor: pointer;
  filter: drop-shadow(
    ${({ theme: { toRem, colors } }) =>
      `0 ${toRem(1)} ${toRem(2)} ${colors.black.rgba.BK000(0.35)}`}
  );
`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Route = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const World = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
`;
export const TextRoute = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: 120%;
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  text-decoration: none;
  font-weight: 800;
  line-height: 120%;
  a{
    text-decoration: none;
    color: inherit;
  }
`;
export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
`;
export const Reviews = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Star = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
`;
export const Duration = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Time = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
`;
export const TextDuration = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
`;
export const Prices = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  column-gap: ${({ theme: { toRem } }) => toRem(8)};
`;

export const Discount = styled.div`
  display: flex;
  align-items: center;
  color: #ed6d6d;
  text-decoration: line-through;
`;

export const TextPrice = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: 120%;
`;
export const Price = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  line-height: 120%;
  font-weight: 600;
`;

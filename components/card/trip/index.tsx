//# React
import { useState } from "react";
//# Next routes
import { Router, Link } from "@routes";
//# React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
//# Hooks
import { useRating } from "@hooks/useRating";
//# Interfaces
import { ITrip } from "@interfaces/trip";
//# Icons
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";
import { GiWorld } from "react-icons/gi";
import { IoTime } from "react-icons/io5";
//# Styles components
import * as C from "./styled";
import { useTranslation } from "next-i18next";
import {
  ValidateDiscount,
  ValidateSeparatePrice,
} from "@hooks/validate/index.";
import {useRouter} from "next/router";


//# Types props
type ITripProps = {
  trip: ITrip;
};



//# Component => trip
export const Trip = ({ trip }: ITripProps): JSX.Element => {
  //# Destroy
  const {
    Url,
    Badge,
    Image,
    Location,
    Name,
    Valoration,
    Price,
    Duration,
    Discount,
  } = trip;

  const router = useRouter()
  //# States
  const [like, setLike] = useState<boolean>(false);
  //# Hooks
  const [ratings, reviews] = useRating(Valoration?.Ratio, Valoration?.Cant);
  //# Methods
  const onLike = (): void => {
    setLike(!like);
  };
  const onRoute = (): void => {
    router.push(Url);
  };

  const { t } = useTranslation("common");

  return (
    <C.Container onClick={onRoute}>
      <C.Poster>
        {Badge && <C.Badge bg={Badge.Color}>{Badge.Text}</C.Badge>}
        <LazyLoadImage effect="blur" width="100%" height="100%" src={Image} />
        <C.Heart active={like} onClick={onLike}>
          {like ? <AiFillHeart /> : <AiOutlineHeart />}
        </C.Heart>
      </C.Poster>
      <C.Content>
        <C.Route>
          <C.World>
            <GiWorld />
          </C.World>
          <C.TextRoute>{Location}</C.TextRoute>
        </C.Route>
       <C.Title> <Link route={Url}><a>{Name}</a></Link></C.Title>
        <C.Footer>
          <C.Reviews>
            {ratings?.map(
              (Star, e: number): JSX.Element => (
                <C.Star key={e}>
                  <Star />
                </C.Star>
              )
            )}
            ({reviews} {t("trip.reviews")})
          </C.Reviews>
          <C.Duration>
            <C.Time>
              <IoTime />
            </C.Time>
            <C.TextDuration>
              {Duration.Count} {Duration.Type}
            </C.TextDuration>
          </C.Duration>
          <C.Prices>
            <C.TextPrice>{t("trip.from")}</C.TextPrice>

            {Number(Discount) === 0 ? (
              <C.Price>{ValidateSeparatePrice({ price: Price })} USD</C.Price>
            ) : (
              <>
                <C.Price>
                  {ValidateDiscount({ prices: Price, discount: Discount })} USD
                </C.Price>
                <C.Discount>
                  {ValidateSeparatePrice({ price: Price })} USD
                </C.Discount>
              </>
            )}
          </C.Prices>
        </C.Footer>
      </C.Content>
    </C.Container>
  );
};

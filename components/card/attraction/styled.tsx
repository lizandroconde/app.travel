import styled from 'styled-components'


export const Box = styled.div`
    //box-shadow: rgb(152 167 204 / 12%) 0px 3px 3px -2px, rgb(152 167 204 / 14%) 0px 3px 4px 0px, rgb(152 167 204 / 20%) 0px 1px 8px 0px;
    height: 120px;
    cursor: pointer;
    overflow: hidden;
    position: relative;
    display: grid;
    grid-template-columns: 1fr 2fr;
 
`

export const Image = styled.div`
   // position: absolute;
    .lazy-load-image-background img{
        object-fit: cover;
        border-radius: 14px;
    }
    
`

export const Title = styled.div`
    top: 0;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    height:100%;
    padding: 15px;
    z-index:3;
`

export const Name = styled.h3`
    font-size: ${({ theme: { toRem } }) => toRem(23)};
`


export const Count = styled.span`
    font-weight: 600;
`
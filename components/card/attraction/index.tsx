import * as C from './styled'
import { useRouter, NextRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";
import React from 'react';


export const Attraction = ({ attraction }: any) => {
    const { Image, Name, Trips, Url } = attraction
    const router: NextRouter = useRouter();

    const onRoute = (e:any): void => {
        e.preventDefault()
        router.push(Url);
    };

    return (
        <C.Box onClick={onRoute}>
            <C.Image>
                <LazyLoadImage effect="blur"
                    width="100%"
                    height="100px"
                    src={Image} />
            </C.Image>
            <C.Title>
                <C.Name>
                    {Name}
                </C.Name>
                <C.Count>
                    {Trips} Tours
                </C.Count>
            </C.Title>
        </ C.Box >
    )
}
import * as C from './styled'
import { useRouter, NextRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";


export const City = ({ city }: any) => {
    const { Image, Name, Trips, Url } = city
    const router: NextRouter = useRouter();

    const onRoute = (): void => {
        router.push(Url);
    };

    return (
        <C.Box onClick={onRoute}>
            <C.Image>
                <LazyLoadImage effect="blur"
                    width="100%"
                    height="100%"
                    src={Image} />
            </C.Image>
            <C.Title>
                <C.Name>
                    {Name}
                </C.Name>
                <C.Count>
                    {Trips} Tours
                </C.Count>
            </C.Title>
        </ C.Box >
    )
}
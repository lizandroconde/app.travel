import styled from 'styled-components'


export const Box = styled.div`
    border-radius: 14px;
    box-shadow: rgb(152 167 204 / 12%) 0px 3px 3px -2px, rgb(152 167 204 / 14%) 0px 3px 4px 0px, rgb(152 167 204 / 20%) 0px 1px 8px 0px;
    display: block;
    height: 230px;
    cursor: pointer;
    overflow: hidden;
    position: relative;
`

export const Image = styled.div`
    position: absolute;
    z-index:1;
    width: 100%;
   height:230px;
   
     
    .lazy-load-image-background{
        width: 100%;
        height: 100%;
        img{
            object-fit: cover;
        }
    }
`

export const Title = styled.div`
     
    top: 0;
    color: white;
    background-image: linear-gradient(rgba(32, 27, 27, 0.062), rgba(0, 0, 0, 0.478));
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    height:100%;
    padding: 15px;
    position: relative;
    z-index:1;
`

export const Name = styled.h3`
    font-size: ${({ theme: { toRem } }) => toRem(23)};
`


export const Count = styled.span`
    font-weight: 600;
`
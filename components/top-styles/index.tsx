//# React
import { useContext } from "react";
//# hooks
import { HomeContext } from "@hooks/useContext";
//# Translate
import { useTranslation } from "next-i18next";
//# Interfaces
import { IResponsive } from "@interfaces/scroll-snap";

//# Components
import { ScrollSnap } from "@components/scroll-snap";
//# Styles components
import * as C from "./styled";
import { Center } from "@themes/global";
import { Attraction } from "@components/card/attraction";

//# Component => top trips
export const TopStyles = (): JSX.Element => {
  const { styles } = useContext(HomeContext);
  const { t } = useTranslation("home");

  return (
    <C.Container>
      <Center>
        <C.Title>{t("top_styles")} </C.Title>
        <C.Content>
          <ScrollSnap defaultSize={0.33} responsive={responsive}>
            {styles.map((style: any, e: number): JSX.Element => {
              return <Attraction key={e} attraction={style} />;
            })}
          </ScrollSnap>
        </C.Content>
      </Center>
    </C.Container>
  );
};

//# Responsive
const responsive: IResponsive[] = [
  {
    media: 1184,
    space: 16,
    firstMargin: 24,
    size: 0.25,
  },
  {
    media: 1024,
    space: 8,
    firstMargin: 24,
    size: 0.33333,
  },
  {
    media: 767,
    space: 8,
    firstMargin: 24,
    size: 0.66667,
  },
];

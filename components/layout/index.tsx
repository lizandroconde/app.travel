import { Header } from "@components/header";
import { Footer } from "@components/footer";
import styled from "styled-components";

interface LayoutProps {
  children: React.ReactNode;
}

const LayoutItem = styled.div`
  background: white;
`

const Layout = ({ children }: LayoutProps) => {
  return (
    <LayoutItem>
      <Header />
      <main>{children}</main>
      <Footer />
    </LayoutItem>
  );
};

export default Layout;

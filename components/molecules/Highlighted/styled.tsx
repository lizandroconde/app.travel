import styled from "styled-components";

export const Layout = styled.div`
    display: grid;
    ${({ theme }) => theme.breakpoints.sm} {
        grid-template-columns: 1fr;
        margin-top: 16rem;
    }
    grid-gap: 1rem;
    ${({ theme }) => theme.breakpoints.md} {
        grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
        margin-top: 5rem;
    }

     
    
   
`   

export const Content = styled.div`
    
    display: grid;
    grid-template-columns: 3fr 6fr;
`

export const Image = styled.div`
    padding: 10px;
`

export const Text = styled.div`
display: flex;
align-items: center;
`
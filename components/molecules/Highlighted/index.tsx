import * as Theme from "./styled"
import Image from "next/image"
import { useTranslation } from "react-i18next"

 

const Highlighted = () =>{
    const {t} = useTranslation("common")
    return( 
        <Theme.Layout>
            <Theme.Content>
                <Theme.Image>
                    <Image width="85%"  height="100%" src="https://www.conde.travel/wp-content/uploads/2019/09/ico-amiglabes.svg" alt="conde travel" />
                </Theme.Image>
                <Theme.Text>
                    <h3>{t("hinglight.ambiente")}</h3>
                </Theme.Text>
            </Theme.Content>
            <Theme.Content>
               <Theme.Image>
               <Image width="85%"  height="100%" src="https://www.conde.travel/wp-content/uploads/2019/09/ico-respeto.svg" alt="conde travel" />
               </Theme.Image>
                <Theme.Text> <h3>{t("hinglight.respet")}</h3></Theme.Text>
            </Theme.Content>
            <Theme.Content>
                <Theme.Image>
                <Image width="85%"  height="100%" src="https://www.conde.travel/wp-content/uploads/2019/09/ico-conciencia.svg" alt="conde travel" />
                </Theme.Image>
                <Theme.Text> <h3>{t("hinglight.cultura")}</h3></Theme.Text>
            </Theme.Content>
            <Theme.Content>
                 <Theme.Image>
                 <Image width="85%"  height="100%" src="https://www.conde.travel/wp-content/uploads/2019/09/ico-aventura.svg" alt="conde travel" />
                 </Theme.Image>
                <Theme.Text> <h3>{t("hinglight.aventura")}</h3></Theme.Text>
            </Theme.Content>
        </Theme.Layout>
    )
}

export default Highlighted
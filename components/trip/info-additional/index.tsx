// Styles components
import { TripContext } from "@hooks/useContext";
import { useTranslation } from "next-i18next";
import { useContext } from "react";
import * as Brick from "./styled";

// Types props
type IProps = {};

// Component => info additional
export const InfoAdditional = (props: IProps): JSX.Element => {
  const {
    info: { Aditional },
  } = useContext(TripContext);
  const { t } = useTranslation("trip");
  let array = [];

  try {
    array = JSON.parse(Aditional);
  } catch (error) {
    console.log(error);
  }

  return (
    <Brick.Container>
      <Brick.Title>{t("additional")}</Brick.Title>
      <Brick.List>
        {array?.map((item: string, index: number) => {
          return <Brick.Item key={index}>{item}</Brick.Item>;
        })}
      </Brick.List>
    </Brick.Container>
  );
};

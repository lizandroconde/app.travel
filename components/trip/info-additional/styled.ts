// Styles components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(17)};
  margin-top: ${({ theme: { toRem } }) => toRem(67)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
`;
export const List = styled.ul`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(4)};
  list-style: circle;
  margin-left: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Item = styled.li`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 300;
  line-height: 120%;
`;

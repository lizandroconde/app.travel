// Interfaces
import { IResponsive } from "@interfaces/scroll-snap";
// Components
import { ScrollSnap } from "@components/scroll-snap";
// React icons
import { AiFillCheckCircle, AiFillCloseCircle } from "react-icons/ai";
// Styles components
import * as Brick from "./styled";
import { useContext } from "react";
import { TripContext } from "@hooks/useContext";
import * as Aditional from "../info-additional/styled";
import { useTranslation } from "next-i18next";
import Icons from "@hooks/icon/index"
// Types props
type IAdded = {
  Title: string;
  Icon: string;
  Content: string;
};

// Component => whats included
export const WhatsIncluded = (): JSX.Element => {
  const {
    info: {
      Include: { Include },
    },
  } = useContext(TripContext);
  const { t } = useTranslation("trip");

  return (
    <Brick.Container>
      <Brick.Title>{t("include")}</Brick.Title>
      <Brick.Content>
        <ScrollSnap defaultSize={0.25} responsive={responsive}>
          {Include?.map(
            ({ Title,Icon, Content }: IAdded, e: number): JSX.Element => {
              let array = [];
              try {
                array = JSON.parse(Content);
              } catch (e) {
                console.log(e);
              }
              

              return (
                <Brick.Card key={e}>
                  <Brick.Head>
                    <Brick.Icon>
                      {Icons[Icon]}
                    </Brick.Icon>
                    <Brick.Text>{Title}</Brick.Text>
                  </Brick.Head>
                  <Brick.List>
                    {array.map((item: string, e: number): JSX.Element => {
                      return <Brick.Item key={e}>{item}</Brick.Item>;
                    })}
                  </Brick.List>
                </Brick.Card>
              );
            }
          )}
        </ScrollSnap>
      </Brick.Content>
    </Brick.Container>
  );
};
// Component => whats not included
export const WhatsNotIncluded = (): JSX.Element => {
  const {
    info: { Include },
  } = useContext(TripContext);
  const { t } = useTranslation("trip");

  let array = [];
  try {
    array = JSON.parse(Include?.NoInclude);
  } catch (error) {
    console.log(error);
  }
  return (
    <Brick.Container>
      <Brick.Title>{t("noinclude")}</Brick.Title>
      <Aditional.List>
        {array?.map((item: string, index: number) => {
          return <Aditional.Item key={index}>{item}</Aditional.Item>;
        })}
      </Aditional.List>
    </Brick.Container>
  );
};
// Component => packing list
export const PackingList = (): JSX.Element => {
  const {
    info: {
      Include: { Recommendations },
    },
  } = useContext(TripContext);
  const { t } = useTranslation("trip");

  return (
    <Brick.Container>
      <Brick.Title>{t("list")}</Brick.Title>
      <Brick.Content>
        <ScrollSnap defaultSize={0.25} responsive={responsive}>
          {Recommendations?.map(
            ({ Title, Content,Icon }: IAdded, e: number): JSX.Element => {
              let array = [];
              try {
                array = JSON.parse(Content);
              } catch (e) {
                console.log(e);
              }
              
              return (
                <Brick.Card key={e}>
                  <Brick.Head>
                    <Brick.Icon active>
                    {Icons[Icon]}
                    </Brick.Icon>
                    <Brick.Text>{Title}</Brick.Text>
                  </Brick.Head>
                  <Brick.List>
                    {array?.map((item: string, e: number): JSX.Element => {
                      return <Brick.Item key={e}>{item}</Brick.Item>;
                    })}
                  </Brick.List>
                </Brick.Card>
              );
            }
          )}
        </ScrollSnap>
      </Brick.Content>
    </Brick.Container>
  );
};

// Responsive
const responsive: IResponsive[] = [
  {
    media: 1184,
    space: 16,
    firstMargin: 24,
    size: 0.25,
  },
  {
    media: 1024,
    space: 8,
    firstMargin: 24,
    size: 0.3,
  },
  {
    media: 767,
    space: 8,
    firstMargin: 24,
    size: 0.3,
  },
  {
    media: 500,
    space: 8,
    firstMargin: 24,
    size: 0.6,
  },
];

// Styles components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(12)};
  margin-top: ${({ theme: { toRem } }) => toRem(57)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
`;
export const Content = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;
export const Card = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(18)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-style: solid;
  border-radius: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(12)};
  padding-right: ${({ theme: { toRem } }) => toRem(12)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(14)};
  padding-top: ${({ theme: { toRem } }) => toRem(42)};
`;
export const Head = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Icon = styled.span<{ active?: boolean }>`
  display: flex;
   
  font-size: ${({ theme: { toRem } }) => toRem(30)};
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(17)};
  font-weight: 400;
  line-height: 120%;
`;
export const List = styled.ul`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(4)};
  list-style: circle;
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Item = styled.li`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(13)};
  font-weight: 300;
  line-height: 120%;
`;

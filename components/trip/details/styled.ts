// Styles components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(32)};
  margin-top: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Tags = styled.div`
  display: grid;
  align-items: center;
  grid-template-columns: repeat(auto-fill, minmax(173px, 1fr));
  column-gap: ${({ theme: { toRem } }) => toRem(24)};
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  ${({ theme: { media } }) => media.to(600)}{
    grid-template-columns:  repeat(auto-fill, minmax(140px, 1fr));
  }
`;
export const Tag = styled.div`
  display: flex;
  column-gap: ${({ theme: { toRem } }) => toRem(8)};
`;
export const TagIcon = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
`;
export const TagText = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
  font-weight: 300;
  line-height: 120%;
`;
export const Overview = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
`;
export const Description = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 300;
  line-height: ${({ theme: { toRem } }) => toRem(20)};
`;

 

export const Attractions = styled.div`
  display: grid;
  align-items: center;
  grid-template-columns: repeat(auto-fill, minmax(120px, 1fr));
  column-gap: ${({ theme: { toRem } }) => toRem(8)};
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
  ${({ theme: { media } }) => media.to(0, 824, true)} {
    grid-template-columns: repeat(auto-fill, minmax(170px, 1fr));
  }
`;
export const Attraction = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const CoverAttraction = styled.div`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(120)};
  border-radius: ${({ theme: { toRem } }) => toRem(16)};
  overflow: hidden;
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 300;
  line-height: 120%;
  text-align: center;
  white-space: nowrap;
`;

// React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
// React icons
import { BiTimeFive } from "react-icons/bi";
import { MdPeopleOutline } from "react-icons/md";
import { HiOutlineLocationMarker } from "react-icons/hi";
import { FaRegComments } from "react-icons/fa";
import { BsPeople } from "react-icons/bs";
import { TiLocationArrowOutline } from "react-icons/ti";
// Styles components
import * as Brick from "./styled";
import { useContext } from "react";
import { TripContext } from "@hooks/useContext";
import parse from "html-react-parser";
import { IPlace } from "@interfaces/trip";
import { useTranslation } from "next-i18next";
import *  as Info from "../info-additional/styled"

// Types props
type IProps = {};

// Component => details
export const Details = ( ): JSX.Element => {
  const { info:{Details,Intro:{highlights,introduction},Places}} = useContext(TripContext);
  const {t}= useTranslation("trip")
  let datahighlights = []

  try {
    datahighlights = JSON.parse(highlights)
  } catch (error) {
    
  }

  return (
    <Brick.Container>
      <Brick.Tags>
        <Brick.Tag>
          <Brick.TagIcon>
            <BiTimeFive />
          </Brick.TagIcon>
          <Brick.TagText>{Details?.duration?.Count} {Details?.duration?.Type}</Brick.TagText>
        </Brick.Tag>
        <Brick.Tag>
          <Brick.TagIcon>
            <MdPeopleOutline />
          </Brick.TagIcon>
          <Brick.TagText>{t("details.peoples")}: {Details?.max}</Brick.TagText>
        </Brick.Tag>
        <Brick.Tag>
          <Brick.TagIcon>
            <BsPeople />
          </Brick.TagIcon>
          <Brick.TagText>{t("details.age")}: {Details?.min}+</Brick.TagText>
        </Brick.Tag>
        <Brick.Tag>
          <Brick.TagIcon>
            <HiOutlineLocationMarker />
          </Brick.TagIcon>
          <Brick.TagText>{Details?.location}</Brick.TagText>
        </Brick.Tag>
        <Brick.Tag>
          <Brick.TagIcon>
            <TiLocationArrowOutline />
          </Brick.TagIcon>
          <Brick.TagText>
             {Places.map(({ name}:IPlace ,)=>(
               <>{name},</>
             ))}
          </Brick.TagText>
        </Brick.Tag>
        <Brick.Tag>
          <Brick.TagIcon>
            <FaRegComments />
          </Brick.TagIcon>
          <Brick.TagText>{Details?.languages}</Brick.TagText>
        </Brick.Tag>
      </Brick.Tags>
      <Brick.Overview>
        <Brick.Title>{t("overview")}</Brick.Title>
        <Brick.Description>
          {parse(introduction)}
        </Brick.Description>
        <Info.List>
          {datahighlights.map((item:string, index:number) =>{
            return(
              <Info.Item key={index}>{item}</Info.Item>

            )
          })}
        </Info.List>
      </Brick.Overview>
      <Brick.Overview>
        <Brick.Title>{t("attractions")}</Brick.Title>
        <Brick.Attractions>
          {Places.map(({image:{url},name}:IPlace ,index: number)=>{ 
            return(
            <Brick.Attraction key={index}>
              <Brick.CoverAttraction>
                <LazyLoadImage
                  effect="blur"
                  width="100%"
                  height="100%"
                  src={url}
                />
              </Brick.CoverAttraction>
              <Brick.Text>{name}</Brick.Text>
            </Brick.Attraction>
            )})}
          </Brick.Attractions>
      </Brick.Overview>
    </Brick.Container>
  );
};

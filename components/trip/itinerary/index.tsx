// React
import { Fragment, useContext, useState } from "react";
import { SRLWrapper } from "simple-react-lightbox";

// React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
// React html parse
import parse from "html-react-parser";
// Interfaces
import { IItinerary } from "@interfaces/trip";
// React icons
import {FaRegCalendarCheck } from "react-icons/fa";
// Styles components
import * as Brick from "./styled";
import { TripContext } from "@hooks/useContext";
import { useTranslation } from "next-i18next";

// Types props
type IProps = {};

// Component => itinerary
export const Itinerary = ( ): JSX.Element => {

  const { info:{Itinerary}} = useContext(TripContext);
  const {t}= useTranslation("trip")
  // States
  //const [active, setActive] = useState<number>(-1);
  // Methods
  /*const handleActive = (index: number): void => {
    setActive(index);
  };*/

  return (
    <Brick.Container>
      <Brick.Title>{t("itinerary")}</Brick.Title>
      <Brick.Content>
        {Itinerary.map((day: IItinerary, e: number): JSX.Element => {
          //const show: boolean = active === e;
          return (
            <Brick.Day key={e}>
              <Brick.Head  >
                <Brick.GroupTitle>
                  <Brick.Calendar>
                    <FaRegCalendarCheck />
                  </Brick.Calendar>
                  <Brick.TextGroup>
                    <Brick.DayTitle>{day?.day === "0" ? t("previews"): t("day")+" "+day?.day }</Brick.DayTitle>
                    <Brick.DaySubTitle>{day?.title}</Brick.DaySubTitle>
                  </Brick.TextGroup>
                </Brick.GroupTitle>
               
              </Brick.Head>
              
                <Fragment>
                  <SRLWrapper>
                    <Brick.Attractions>
                      {day?.gallery?.map((photo, e: number): JSX.Element => {
                        return (
                          <Brick.Attraction key={e}>
                            <a href={photo.url}  >
                            <Brick.CoverAttraction>
                              <LazyLoadImage
                                effect="blur"
                                width="100%"
                                height="100%"
                                alt={photo.name}
                                title={photo.name}
                                src={photo.url}
                              />
                            </Brick.CoverAttraction>
                            </a>
                            <Brick.Text>{photo.name}</Brick.Text>
                          </Brick.Attraction>
                        );
                      })}
                    </Brick.Attractions>
                    </SRLWrapper>
                  <Brick.Section>{parse(day.content)}</Brick.Section>
                </Fragment>
            
            </Brick.Day>
          );
        })}
      </Brick.Content>
    </Brick.Container>
  );
};

 
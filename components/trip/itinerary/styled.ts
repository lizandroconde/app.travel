// Styles components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  margin-top: ${({ theme: { toRem } }) => toRem(67)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(32)};
`;
export const Day = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Head = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(20)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GR23501};
  border-radius: ${({ theme: { toRem } }) => toRem(18)};
  padding-left: ${({ theme: { toRem } }) => toRem(26)};
  padding-right: ${({ theme: { toRem } }) => toRem(26)};
  padding-top: ${({ theme: { toRem } }) => toRem(18)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(20)};
  cursor: pointer;
`;
export const GroupTitle = styled.div`
  display: flex;
  align-items: flex-start;
  column-gap: ${({ theme: { toRem } }) => toRem(11)};
`;
export const Calendar = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
`;
export const TextGroup = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(4)};
`;
export const DayTitle = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(17)};
  font-weight: 700;
  line-height: 120%;
`;
export const DaySubTitle = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(20)};
  font-weight: 700;
  line-height: 120%;
`;
export const Down = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY81829c};
  font-size: ${({ theme: { toRem } }) => toRem(32)};
  margin-left: auto;
  cursor: pointer;
`;
export const Section = styled.div`
  width: 100%;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  text-align:  justify;
  font-family: sans-serif;
  line-height: 120%;
  padding-left: ${({ theme: { toRem } }) => toRem(10)};
  padding-right: ${({ theme: { toRem } }) => toRem(10)};
  p{
    font-family: sans-serif;
    margin-top: .5rem;
    margin-bottom: .5rem;
  }
  & p:not(:first-child) {
    margin-top: ${({ theme: { toRem } }) => toRem(10)};
   
  }
  & a {
    color: ${({ theme: { colors } }) => colors.orange.rgb.OF28023};
  }
  & strong,
  & b {
    font-weight: 700;
  }
  ${({ theme: { media } }) => media.to(0, 824, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(52)};
    padding-right: ${({ theme: { toRem } }) => toRem(52)};
  }
`;
export const Attractions = styled.div`
  display: grid;
  align-items: center;
  grid-template-columns: repeat(auto-fill, minmax(100px, 1fr));
  column-gap: ${({ theme: { toRem } }) => toRem(10)};
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
  padding-left: ${({ theme: { toRem } }) => toRem(10)};
  padding-right: ${({ theme: { toRem } }) => toRem(10)};
  ${({ theme: { media } }) => media.to(0, 824, true)} {
    grid-template-columns: repeat(auto-fill, minmax(120px, 1fr));
    column-gap: ${({ theme: { toRem } }) => toRem(40)};
    padding-left: ${({ theme: { toRem } }) => toRem(52)};
    padding-right: ${({ theme: { toRem } }) => toRem(52)};
  }
`;
export const Attraction = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const CoverAttraction = styled.div`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(120)};
  border-radius: ${({ theme: { toRem } }) => toRem(16)};
  overflow: hidden;
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  line-height: 120%;
  text-align: center;
  white-space: nowrap;
`;

// React
import {useContext, useState } from "react";
// React html parse
import parse from "html-react-parser";
// Interfaces
import { IFaq } from "@interfaces/trip";
// React icons
import { TiPlus } from "react-icons/ti";
import { FaSortDown } from "react-icons/fa";
// Styles components
import * as Brick from "./styled";
import { TripContext } from "@hooks/useContext";
import { useTranslation } from "next-i18next";
import { FAQPageJsonLd } from 'next-seo';

// Types props
type IProps = {};

// Component => faqs
export const Faqs = (props: IProps): JSX.Element => {
  // States
  const [active, setActive] = useState<number>(-1);
  const { info:{Faq} } = useContext(TripContext);
  const {t}= useTranslation("trip")
  // Methods
  const handleActive = (index: number): void => {
    if (active !== -1) {
      setActive(-1);
    } else {
      setActive(index);
    }
  };

  let newFaq = []

  for(let faq of Faq ){
    newFaq.push({questionName:faq.ask ,acceptedAnswerText:faq.response})
  }

  return (
    <Brick.Container>
      <Brick.Title>{t("faq")}</Brick.Title>
      <FAQPageJsonLd
      mainEntity={newFaq}
    />
      <Brick.Content>
        {Faq?.map((faq: IFaq, e: number): JSX.Element => {
          const show: boolean = active === e;
          return (
            <Brick.Faq key={e}>
              <Brick.Head  onClick={() => handleActive(e)}>
                <Brick.GroupTitle>
                  <Brick.Plus>
                    <TiPlus />
                  </Brick.Plus>
                  <Brick.FaqTitle  >
                    {faq.ask}
                  </Brick.FaqTitle>
                </Brick.GroupTitle>
                <Brick.More>
                  <FaSortDown />
                </Brick.More>
              </Brick.Head>
              {show && <Brick.Section>{parse(faq.response)}</Brick.Section>}
            </Brick.Faq>
          );
        })}
      </Brick.Content>
    </Brick.Container>
  );
};

 
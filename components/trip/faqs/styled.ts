// Styles components
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(43)};
  margin-top: ${({ theme: { toRem } }) => toRem(67)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 600;
  line-height: 120%;
`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(14)};
`;
export const Faq = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(31)};
`;
export const Head = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(20)};
  padding-top: ${({ theme: { toRem } }) => toRem(14)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(14)};
  border-bottom-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  border-bottom-width: ${({ theme: { toRem } }) => toRem(1)};
  border-bottom-style: solid;
  cursor: pointer;
`;
export const GroupTitle = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
`;
export const Plus = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
`;
export const FaqTitle = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(20)};
  font-weight: 700;
  line-height: 120%;
  ${Head}:hover & {
    text-decoration: underline;
  }
`;
export const More = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY81829c};
  font-size: ${({ theme: { toRem } }) => toRem(32)};
  margin-left: auto;
  cursor: pointer;
`;
export const Section = styled.div`
  width: 100%;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 300;
  line-height: 120%;
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  padding-right: ${({ theme: { toRem } }) => toRem(8)};
  & p:not(:first-child) {
    margin-top: ${({ theme: { toRem } }) => toRem(10)};
  }
  & a {
    color: ${({ theme: { colors } }) => colors.orange.rgb.OF28023};
  }
  & strong,
  & b {
    font-weight: 700;
  }
  ${({ theme: { media } }) => media.to(0, 824, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(14)};
    padding-right: ${({ theme: { toRem } }) => toRem(14)};
  }
`;

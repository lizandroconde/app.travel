// Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-top: ${({ theme: { toRem } }) => toRem(24)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Content = styled.div`
  width: 100%;
   
`;
export const Actions = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  column-gap: ${({ theme: { toRem } }) => toRem(24)};
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
  background: ${({ theme: { colors } }) => colors.blue.rgb.BL3b556a};
  border-radius: ${({ theme: { toRem } }) => toRem(20)};
  padding-top: ${({ theme: { toRem } }) => toRem(27)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(21)};
  padding-left: ${({ theme: { toRem } }) => toRem(40)};
  padding-right: ${({ theme: { toRem } }) => toRem(40)};
  margin-top: ${({ theme: { toRem } }) => toRem(55)};
`;
export const Action = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(11)};
  cursor: pointer;
`;
export const Icon = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(17)};
  font-weight: 400;
  line-height: 120%;
  ${Action}:hover & {
    text-decoration: underline;
  }
`;


export const Layout = styled.div`
   background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  display: flex;
  ${({ theme: { media } }) => media.to(1000)} {
    display: block;
  }
`

export const ContentLeft = styled.div`
  width: 65%;
  margin-right: ${({ theme: { toRem } }) => toRem(20)};
  //margin-top: ${({ theme: { toRem } }) => toRem(50)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(50)};
  ${({ theme: { media } }) => media.to(1250)} {
    width: 60%;
  }
  ${({ theme: { media } }) => media.to(1000)} {
    width: 100%;
    margin-right: ${({ theme: { toRem } }) => toRem(0)};
  }
`;

export const ContentRight = styled.div`
  width: 35%;
  //margin-top: ${({ theme: { toRem } }) => toRem(50)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(50)};
  ${({ theme: { media } }) => media.to(1250)} {
    width: 40%;
  }
  ${({ theme: { media } }) => media.to(1000)} {
    width: 100%;
  }
`;

export const StickyBook = styled.div`
  position: sticky;
  top: ${({ theme: { toRem } }) => toRem(3)};
  padding-top: ${({ theme: { toRem } }) => toRem(50)};
  padding-left: 2.5rem;
    padding-right: 2.6rem;
  
    ${({theme:{ media }})=>media.to(1000)}{
      padding-left: .5rem;
      padding-right: .5rem;
    }

  /*${({ theme: { media } }) => media.to(1000)} {
    position: fixed;
    left: 0;
    top: auto;
    bottom: 0;
    z-index: 999;
    width: 100%;
    height: fit-content;
    border-top-width: ${({ theme: { toRem } }) => toRem(1)};
    border-top-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYDDD};
    border-top-style: solid;
    background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-top: ${({ theme: { toRem } }) => toRem(5)};
    padding-bottom: ${({ theme: { toRem } }) => toRem(5)};
    padding-left: ${({ theme: { toRem } }) => toRem(15)};
    padding-right: ${({ theme: { toRem } }) => toRem(15)};
  }*/
`;

export const Head = styled.div`
  text-align: right;
  
    border-radius: 8px 8px 0 0;
    padding: 10px;
    border: 1px solid #e8e9e9;
  ${({ theme: { media } }) => media.to(1000)} {
    display: flex;
    flex-direction: column;
  }
`;

export const Price = styled.span`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(32)};
  font-weight: 700;
  margin-left: ${({ theme: { toRem } }) => toRem(6)};
  ${({ theme: { media } }) => media.to(1000)} {
    font-size: ${({ theme: { toRem } }) => toRem(28)};
    font-weight: 600;
  }
`;

export const From = styled.span`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(22)};
  font-weight: 500;
  margin-right: 10px;
  margin-left: ${({ theme: { toRem } }) => toRem(6)};
  ${({ theme: { media } }) => media.to(1000)} {
    font-size: ${({ theme: { toRem } }) => toRem(28)};
    font-weight: 600;
  }
`;

export const Badge = styled.span`
  background: rgb(163, 0, 0);
  color: white;
  display: block;
  height: max-content;

  border-radius: 25px;
  padding: 0px 10px;
  font-size: 0.8rem;
  margin-right: 10px;
`

export const Discount = styled.span`
    font-weight: 400;
    font-size: 1.54rem;
    text-decoration: line-through;
`

export const DicountLayout = styled.div`
  display: flex;
  justify-content:flex-end;
  align-items: center;
`
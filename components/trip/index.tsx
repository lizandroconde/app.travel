// React Scroll
import { Element } from "react-scroll";
// Components
import { Navigation } from "@components/trip/navigation";
import { Details } from "@components/trip/details";
import { Itinerary } from "@components/trip/itinerary";
import { InfoAdditional } from "@components/trip/info-additional";
import { Faqs } from "@components/trip/faqs";
import Script from 'next/script'
import {
  WhatsIncluded,
  WhatsNotIncluded,
  PackingList,
} from "@components/trip/additionals";
// Global
import { Center } from "@themes/global";
// React icons
import { FaComments, FaDownload, FaHeart } from "react-icons/fa";
// Styles components
import * as Brick from "./styled";
import { useTranslation } from "next-i18next";
import { useContext } from "react";
import { TripContext } from "@hooks/useContext";
import {
  ValidateDiscount,
  ValidateSeparatePrice,
} from "@hooks/validate/index.";

// Component => content
export const Content = (): JSX.Element => {
  const { t } = useTranslation("trip");
  const {
    info: { CodWetravel, Price },
  } = useContext(TripContext);

   

  return (
    <Brick.Container>
      <Center>
        <Brick.Layout>
          <Brick.ContentLeft>
            <Brick.Content>
              <Navigation />
              <Element name="Detail">
                <Details />
                <WhatsIncluded />
                <WhatsNotIncluded />
                <PackingList />
              </Element>
              <Element name="Itinerary">
                <Itinerary />
              </Element>
              <Element name="Additional_Info">
                <InfoAdditional />
              </Element>
              <Element name="FAQ">
                <Faqs />
              </Element>
              <Brick.Actions>
                <Brick.Action>
                  <Brick.Icon>
                    <FaComments />
                  </Brick.Icon>
                  <Brick.Text>{t("question")}</Brick.Text>
                </Brick.Action>
                <Brick.Action>
                  <Brick.Icon>
                    <FaDownload />
                  </Brick.Icon>
                  <Brick.Text>{t("download")}</Brick.Text>
                </Brick.Action>
                <Brick.Action>
                  <Brick.Icon>
                    <FaHeart />
                  </Brick.Icon>
                  <Brick.Text>{t("savelist")}</Brick.Text>
                </Brick.Action>
              </Brick.Actions>
            </Brick.Content>
          </Brick.ContentLeft>
          <Brick.ContentRight>
            <Brick.StickyBook>
              <Brick.Head>
                {Price.Discount !== 0 ? (
                  <>
                    <Brick.DicountLayout>
                      <Brick.Badge>- {Price.Discount} % </Brick.Badge>
                      <Brick.Discount>
                        {ValidateSeparatePrice({ price: Price.Amount })} USD
                      </Brick.Discount>
                    </Brick.DicountLayout>
                    <Brick.Price>
                      <Brick.From>{t("from")}</Brick.From>
                      {ValidateDiscount({
                        prices: Price.Amount,
                        discount: Price.Discount,
                      })}{" "}
                      USD
                    </Brick.Price>
                  </>
                ) : (
                  <Brick.Price>
                    <Brick.From>{t("from")}</Brick.From>
                    {ValidateSeparatePrice({ price: Price.Amount })} USD
                  </Brick.Price>
                )}
              </Brick.Head>
                    
               <iframe src={`https://www.wetravel.com/embed/calendar?uuid=${CodWetravel}&btnColor=f5a623&btnName=${t("booknow")}&title=${t("startdate")}&env=https://www.wetravel.com`} style={{width:"100%", minHeight:"500px",display:"block"}}  />
               
                 
            </Brick.StickyBook>
          </Brick.ContentRight>
        </Brick.Layout>
     
      </Center>
    </Brick.Container>
  );
};

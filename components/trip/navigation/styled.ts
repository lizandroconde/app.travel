// React Scroll
import { Link } from "react-scroll";
// Styles components
import styled from "styled-components";

export const Container = styled.div`
  position: sticky;
  top: 0;
  z-index: 1;
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
  overflow-x: auto;
  height: ${({ theme: { toRem } }) => toRem(44)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  border-radius: ${({ theme: { toRem } }) => toRem(40)};
  padding: ${({ theme: { toRem } }) => toRem(2)};
  -webkit-overflow-scrolling: touch;
  &::-webkit-scrollbar {
    display: none;
  }
`;
export const Item = styled.div``;
export const Routing = styled(Link)`
  display: block;
  height: ${({ theme: { toRem } }) => toRem(40)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  border-radius: ${({ theme: { toRem } }) => toRem(40)};
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-style: solid;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  font-weight: 400;
  line-height: 120%;
  text-align: center;
  white-space: nowrap;
  cursor: pointer;
  &.active {
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYf5f6f9};
    border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRY9aa7b5};
  }
`;

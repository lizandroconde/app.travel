// React
import { useTranslation } from "next-i18next";
import { useCallback, useRef } from "react";
// Styles components
import * as Brick from "./styled";

// Types props
type IMenu = {
  id: string;
  name: string;
};
type IProps = {};

// Component => navidation
export const Navigation = (props: IProps): JSX.Element => {
  // Ref
  const item = useRef<HTMLDivElement[]>([]);
  const { t } = useTranslation("trip");
  // Methods
  const scrollToCategory = (id: number): void => {
    item?.current[id]?.scrollIntoView({ inline: "center" });
  };

  const mycallback = useCallback((arg: HTMLDivElement, index: number) => {
    item.current[index] = arg;
  }, []);

  const menus: IMenu[] = [
    {
      id: "Detail",
      name: t("nav.detail"),
    },
    {
      id: "Itinerary",
      name: t("nav.itinerary"),
    },
    {
      id: "Additional_Info",
      name: t("nav.info"),
    },
    {
      id: "FAQ",
      name: t("nav.faq"),
    },
  ];

  return (
    <Brick.Container>
      {menus?.map((menu: IMenu, e: number): JSX.Element => {
        return (
          <Brick.Item key={e} ref={(arg: HTMLDivElement) => mycallback(arg, e)}>
            <Brick.Routing
              activeClass="active"
              to={menu?.id}
              spy={true}
              smooth={true}
              duration={500}
              offset={-50}
              onSetActive={() => scrollToCategory(e)}
            >
              {menu?.name}
            </Brick.Routing>
          </Brick.Item>
        );
      })}
    </Brick.Container>
  );
};

//# React
import { useContext, } from "react";

//# Interfaces
import { IResponsive } from "@interfaces/scroll-snap";
import { ICityHome } from "@interfaces/city";
//# Components
import { ScrollSnap } from "@components/scroll-snap";
import { City } from "@components/card/city";
//# Styles components
import * as C from "./styled";
//# hooks
import { HomeContext } from "@hooks/useContext"
//# Translate
import { useTranslation } from 'next-i18next';

interface Context {
    cities: ICityHome[]
}


//# Component => top trips
export const TopCities = (): JSX.Element => {
    const { cities }: Context = useContext(HomeContext)
    const {t}=useTranslation("home")

    return (
        <C.Container>
            <C.Title>{t("top_destination")}</C.Title>
            <C.Content>
                <ScrollSnap defaultSize={0.33} responsive={responsive}>
                    {cities.map((city: any, e: number): JSX.Element => {
                        return <City key={e} city={city} />;
                    })}
                </ScrollSnap>
            </C.Content>
        </C.Container>
    );
};

//# Responsive
const responsive: IResponsive[] = [
    {
        media: 1184,
        space: 16,
        firstMargin: 24,
        size: 0.25,
    },
    {
        media: 1024,
        space: 8,
        firstMargin: 24,
        size: 0.33333,
    },
    {
        media: 767,
        space: 8,
        firstMargin: 24,
        size: 0.66667,
    },
];

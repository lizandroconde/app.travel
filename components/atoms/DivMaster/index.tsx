import styled, { css } from 'styled-components';

interface MediaProps {
  lg?: string;
  md?: string;
  sm?: string;
}
interface MediaPropsInt {
  lg?: number;
  md?: number;
  sm?: number;
}

interface IDivMaster {
  children?: React.ReactNode;
  background?: string;
  padding?: string | MediaProps;
  margin?: string | MediaProps;
  justifyContent?: string | MediaProps;
  flexDirection?: string | MediaProps;
  alignItems?: string | MediaProps;
  alignContent?: string | MediaProps;
  maxWidth?: string | MediaProps;
  width?: string | MediaProps;
  Display?: string | MediaProps;
  boxShadow?: string | MediaProps;
  overflow?: string | MediaProps;
  border?: string | MediaProps;
  borderRadius?: number | MediaPropsInt;
  position?: string | MediaProps;
  textAlign?: string | MediaProps;
  mb?: number | MediaPropsInt;
  mt?: number | MediaPropsInt;
  mr?: number | MediaPropsInt;
  ml?: number | MediaPropsInt;
  mx?: number | MediaPropsInt;
  my?: number | MediaPropsInt;
  pb?: number | MediaPropsInt;
  pt?: number | MediaPropsInt;
  pr?: number | MediaPropsInt;
  pl?: number | MediaPropsInt;
  px?: number | MediaPropsInt;
  py?: number | MediaPropsInt;
}

const DivMaster = styled.div<IDivMaster>`
  display: ${({ Display }) => typeof Display === 'string' && Display};
  flex-direction: ${({ flexDirection }) =>
    typeof flexDirection === 'string' && flexDirection};
  justify-content: ${({ justifyContent }) =>
    typeof justifyContent === 'string' && justifyContent};
  align-items: ${({ alignItems }) =>
    typeof alignItems === 'string' && alignItems};
  align-content: ${({ alignContent }) =>
    typeof alignContent === 'string' && alignContent};
  text-align: ${({ textAlign }) => typeof textAlign === 'string' && textAlign};
  background: ${({ background }) => background};
  padding: ${({ padding }) => typeof padding === 'string' && padding};
  margin: ${({ margin }) => typeof margin === 'string' && margin};
  width: ${({ width }) => typeof width === 'string' && width};
  max-width: ${({ maxWidth }) => typeof maxWidth === 'string' && maxWidth};
  box-shadow: ${({ boxShadow }) => typeof boxShadow === 'string' && boxShadow};
  overflow: ${({ overflow }) => typeof overflow === 'string' && overflow};
  border-radius: ${({ theme: { spaces }, borderRadius }) =>
    typeof borderRadius === 'number' && spaces(borderRadius)};
  position: ${({ position }) => typeof position === 'string' && position};
  ${({ theme: { spaces }, mx }) =>
    typeof mx === 'number' &&
    css`
      margin-right: ${spaces(mx)};
      margin-left: ${spaces(mx)};
    `};
  ${({ theme: { spaces }, my }) =>
    typeof my === 'number' &&
    css`
      margin-top: ${spaces(my)};
      margin-bottom: ${spaces(my)};
    `};
  ${({ theme: { spaces }, px }) =>
    typeof px === 'number' &&
    css`
      padding-right: ${spaces(px)};
      padding-left: ${spaces(px)};
    `};
  ${({ theme: { spaces }, py }) =>
    typeof py === 'number' &&
    css`
      padding-top: ${spaces(py)};
      padding-bottom: ${spaces(py)};
    `};
  margin-bottom: ${({ theme: { spaces }, mb }) => typeof mb === 'number' && spaces(mb)};
  margin-right: ${({ theme: { spaces }, mr }) => typeof mr === 'number' && spaces(mr)};
  margin-top: ${({ theme: { spaces }, mt }) => typeof mt === 'number' && spaces(mt)};
  margin-left: ${({ theme: { spaces }, ml }) => typeof ml === 'number' && spaces(ml)};
  padding-bottom: ${({ theme: { spaces }, pb }) => typeof pb === 'number' && spaces(pb)};
  padding-right: ${({ theme: { spaces }, pr }) => typeof pr === 'number' && spaces(pr)};
  padding-top: ${({ theme: { spaces }, pt }) => typeof pt === 'number' && spaces(pt)};
  padding-left: ${({ theme: { spaces }, pl }) => typeof pl === 'number' && spaces(pl)};
  ${({ theme: { breakpoints } }) => breakpoints.sm} {
    display: ${({ Display }) =>
    typeof Display !== 'string' && Display?.sm && Display.sm};
    flex-direction: ${({ flexDirection }) =>
    typeof flexDirection !== 'string' &&
    flexDirection?.sm &&
    flexDirection?.sm};
    justify-content: ${({ justifyContent }) =>
    typeof justifyContent !== 'string' &&
    justifyContent?.sm &&
    justifyContent?.sm};
    align-items: ${({ alignItems }) =>
    typeof alignItems !== 'string' && alignItems?.sm && alignItems?.sm};
    align-content: ${({ alignContent }) =>
    typeof alignContent !== 'string' && alignContent?.sm && alignContent?.sm};
    text-align: ${({ textAlign }) =>
    typeof textAlign !== 'string' && textAlign?.sm && textAlign?.sm};
    padding: ${({ padding }) =>
    typeof padding !== 'string' && padding?.sm && padding?.sm};
    margin: ${({ margin }) =>
    typeof margin !== 'string' && margin?.sm && margin?.sm};
    width: ${({ width }) =>
    typeof width !== 'string' && width?.sm && width?.sm};
    max-width: ${({ maxWidth }) =>
    typeof maxWidth !== 'string' && maxWidth?.sm && maxWidth?.sm};
    box-shadow: ${({ boxShadow }) =>
    typeof boxShadow !== 'string' && boxShadow?.sm && boxShadow?.sm};
    overflow: ${({ overflow }) =>
    typeof overflow !== 'string' && overflow?.sm && overflow?.sm};
    position: ${({ position }) =>
    typeof position !== 'string' && position?.sm && position?.sm};
    border-radius: ${({ theme: { spaces }, borderRadius }) =>
    typeof borderRadius !== 'number' &&
    borderRadius?.sm &&
    spaces(borderRadius?.sm)};
    ${({ theme: { spaces }, mx }) =>
    typeof mx !== 'number' &&
    mx?.sm &&
    css`
        margin-right: ${spaces(mx?.sm)};
        margin-left: ${spaces(mx?.sm)};
      `};
    ${({ theme: { spaces }, my }) =>
    typeof my !== 'number' &&
    my?.sm &&
    css`
        margin-top: ${spaces(my?.sm)};
        margin-bottom: ${spaces(my?.sm)};
      `};
    ${({ theme: { spaces }, px }) =>
    typeof px !== 'number' &&
    px?.sm &&
    css`
        padding-right: ${spaces(px?.sm)};
        padding-left: ${spaces(px?.sm)};
      `};
    ${({ theme: { spaces }, py }) =>
    typeof py !== 'number' &&
    py?.sm &&
    css`
        padding-top: ${spaces(py?.sm)};
        padding-bottom: ${spaces(py?.sm)};
      `};
    margin-bottom: ${({ theme: { spaces }, mb }) =>
    typeof mb !== 'number' && mb?.sm && spaces(mb?.sm)};
    margin-right: ${({ theme: { spaces }, mr }) =>
    typeof mr !== 'number' && mr?.sm && spaces(mr?.sm)};
    margin-top: ${({ theme: { spaces }, mt }) =>
    typeof mt !== 'number' && mt?.sm && spaces(mt?.sm)};
    margin-left: ${({ theme: { spaces }, ml }) =>
    typeof ml !== 'number' && ml?.sm && spaces(ml?.sm)};
    padding-bottom: ${({ theme: { spaces }, pb }) =>
    typeof pb !== 'number' && pb?.sm && spaces(pb?.sm)};
    padding-right: ${({ theme: { spaces }, pr }) =>
    typeof pr !== 'number' && pr?.sm && spaces(pr?.sm)};
    padding-top: ${({ theme: { spaces }, pt }) =>
    typeof pt !== 'number' && pt?.sm && spaces(pt?.sm)};
    padding-left: ${({ theme: { spaces }, pl }) =>
    typeof pl !== 'number' && pl?.sm && spaces(pl?.sm)};
  }
  ${({ theme: { breakpoints } }) => breakpoints.md} {
    display: ${({ Display }) =>
    typeof Display !== 'string' && Display?.md && Display.md};
    flex-direction: ${({ flexDirection }) =>
    typeof flexDirection !== 'string' &&
    flexDirection?.md &&
    flexDirection?.md};
    justify-content: ${({ justifyContent }) =>
    typeof justifyContent !== 'string' &&
    justifyContent?.md &&
    justifyContent?.md};
    align-items: ${({ alignItems }) =>
    typeof alignItems !== 'string' && alignItems?.md && alignItems?.md};
    align-content: ${({ alignContent }) =>
    typeof alignContent !== 'string' && alignContent?.md && alignContent?.md};
    text-align: ${({ textAlign }) =>
    typeof textAlign !== 'string' && textAlign?.md && textAlign?.md};
    padding: ${({ padding }) =>
    typeof padding !== 'string' && padding?.md && padding?.md};
    margin: ${({ margin }) =>
    typeof margin !== 'string' && margin?.md && margin?.md};
    width: ${({ width }) =>
    typeof width !== 'string' && width?.md && width?.md};
    max-width: ${({ maxWidth }) =>
    typeof maxWidth !== 'string' && maxWidth?.md && maxWidth?.md};
    box-shadow: ${({ boxShadow }) =>
    typeof boxShadow !== 'string' && boxShadow?.md && boxShadow?.md};
    overflow: ${({ overflow }) =>
    typeof overflow !== 'string' && overflow?.md && overflow?.md};
    position: ${({ position }) =>
    typeof position !== 'string' && position?.md && position?.md};
    border-radius: ${({ theme: { spaces }, borderRadius }) =>
    typeof borderRadius !== 'number' &&
    borderRadius?.md &&
    spaces(borderRadius?.md)};
    ${({ theme: { spaces }, mx }) =>
    typeof mx !== 'number' &&
    mx?.md &&
    css`
        margin-right: ${spaces(mx?.md)};
        margin-left: ${spaces(mx?.md)};
      `};
    ${({ theme: { spaces }, my }) =>
    typeof my !== 'number' &&
    my?.md &&
    css`
        margin-top: ${spaces(my?.md)};
        margin-bottom: ${spaces(my?.md)};
      `};
    ${({ theme: { spaces }, px }) =>
    typeof px !== 'number' &&
    px?.md &&
    css`
        padding-right: ${spaces(px?.md)};
        padding-left: ${spaces(px?.md)};
      `};
    ${({ theme: { spaces }, py }) =>
    typeof py !== 'number' &&
    py?.md &&
    css`
        padding-top: ${spaces(py?.md)};
        padding-bottom: ${spaces(py?.md)};
      `};
    margin-bottom: ${({ theme: { spaces }, mb }) =>
    typeof mb !== 'number' && mb?.md && spaces(mb?.md)};
    margin-right: ${({ theme: { spaces }, mr }) =>
    typeof mr !== 'number' && mr?.md && spaces(mr?.md)};
    margin-top: ${({ theme: { spaces }, mt }) =>
    typeof mt !== 'number' && mt?.md && spaces(mt?.md)};
    margin-left: ${({ theme: { spaces }, ml }) =>
    typeof ml !== 'number' && ml?.md && spaces(ml?.md)};
    padding-bottom: ${({ theme: { spaces }, pb }) =>
    typeof pb !== 'number' && pb?.md && spaces(pb?.md)};
    padding-right: ${({ theme: { spaces }, pr }) =>
    typeof pr !== 'number' && pr?.md && spaces(pr?.md)};
    padding-top: ${({ theme: { spaces }, pt }) =>
    typeof pt !== 'number' && pt?.md && spaces(pt?.md)};
    padding-left: ${({ theme: { spaces }, pl }) =>
    typeof pl !== 'number' && pl?.md && spaces(pl?.md)};
  }
  ${({ theme: { breakpoints } }) => breakpoints.lg} {
    display: ${({ Display }) =>
    typeof Display !== 'string' && Display?.lg && Display.lg};
    flex-direction: ${({ flexDirection }) =>
    typeof flexDirection !== 'string' &&
    flexDirection?.lg &&
    flexDirection?.lg};
    justify-content: ${({ justifyContent }) =>
    typeof justifyContent !== 'string' &&
    justifyContent?.lg &&
    justifyContent?.lg};
    align-items: ${({ alignItems }) =>
    typeof alignItems !== 'string' && alignItems?.lg && alignItems?.lg};
    align-content: ${({ alignContent }) =>
    typeof alignContent !== 'string' && alignContent?.lg && alignContent?.lg};
    text-align: ${({ textAlign }) =>
    typeof textAlign !== 'string' && textAlign?.lg && textAlign?.lg};
    padding: ${({ padding }) =>
    typeof padding !== 'string' && padding?.lg && padding?.lg};
    margin: ${({ margin }) =>
    typeof margin !== 'string' && margin?.lg && margin?.lg};
    width: ${({ width }) =>
    typeof width !== 'string' && width?.lg && width?.lg};
    max-width: ${({ maxWidth }) =>
    typeof maxWidth !== 'string' && maxWidth?.lg && maxWidth?.lg};
    box-shadow: ${({ boxShadow }) =>
    typeof boxShadow !== 'string' && boxShadow?.lg && boxShadow?.lg};
    overflow: ${({ overflow }) =>
    typeof overflow !== 'string' && overflow?.lg && overflow?.lg};
    position: ${({ position }) =>
    typeof position !== 'string' && position?.lg && position?.lg};
    border-radius: ${({ theme: { spaces }, borderRadius }) =>
    typeof borderRadius !== 'number' &&
    borderRadius?.lg &&
    spaces(borderRadius?.lg)};
    ${({ theme: { spaces }, mx }) =>
    typeof mx !== 'number' &&
    mx?.lg &&
    css`
        margin-right: ${spaces(mx?.lg)};
        margin-left: ${spaces(mx?.lg)};
      `};
    ${({ theme: { spaces }, my }) =>
    typeof my !== 'number' &&
    my?.lg &&
    css`
        margin-top: ${spaces(my?.lg)};
        margin-bottom: ${spaces(my?.lg)};
      `};
    ${({ theme: { spaces }, px }) =>
    typeof px !== 'number' &&
    px?.lg &&
    css`
        padding-right: ${spaces(px?.lg)};
        padding-left: ${spaces(px?.lg)};
      `};
    ${({ theme: { spaces }, py }) =>
    typeof py !== 'number' &&
    py?.lg &&
    css`
        padding-top: ${spaces(py?.lg)};
        padding-bottom: ${spaces(py?.lg)};
      `};
    margin-bottom: ${({ theme: { spaces }, mb }) =>
    typeof mb !== 'number' && mb?.lg && spaces(mb?.lg)};
    margin-right: ${({ theme: { spaces }, mr }) =>
    typeof mr !== 'number' && mr?.lg && spaces(mr?.lg)};
    margin-top: ${({ theme: { spaces }, mt }) =>
    typeof mt !== 'number' && mt?.lg && spaces(mt?.lg)};
    margin-left: ${({ theme: { spaces }, ml }) =>
    typeof ml !== 'number' && ml?.lg && spaces(ml?.lg)};
    padding-bottom: ${({ theme: { spaces }, pb }) =>
    typeof pb !== 'number' && pb?.lg && spaces(pb?.lg)};
    padding-right: ${({ theme: { spaces }, pr }) =>
    typeof pr !== 'number' && pr?.lg && spaces(pr?.lg)};
    padding-top: ${({ theme: { spaces }, pt }) =>
    typeof pt !== 'number' && pt?.lg && spaces(pt?.lg)};
    padding-left: ${({ theme: { spaces }, pl }) =>
    typeof pl !== 'number' && pl?.lg && spaces(pl?.lg)};
  }
`;

export default DivMaster;

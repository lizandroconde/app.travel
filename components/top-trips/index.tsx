//# React
import { useContext, } from "react";
//# Apollo
//# Translate
import { useTranslation } from 'next-i18next';
//# Interfaces
import { IResponsive } from "@interfaces/scroll-snap";
import { ITrip } from "@interfaces/trip";
//# Components
import { ScrollSnap } from "@components/scroll-snap";
import { Trip } from "@components/card/trip";
//# Styles components
import * as C from "./styled";
import { HomeContext } from "@hooks/useContext";
import TripCity from "@components/organisms/Trips-City";

//# Component => top trips
export const TopTrips = (): JSX.Element => {
  //# States
  const { trips } = useContext(HomeContext)
  const { t } = useTranslation("home")

  return (
    <C.Container>
      <C.Title>{t("top_tours")}</C.Title>
      <C.Content>
         <TripCity trips={trips} />
      </C.Content>
    </C.Container>
  );
};

//# Responsive
const responsive: IResponsive[] = [
  {
    media: 1184,
    space: 16,
    firstMargin: 24,
    size: 0.25,
  },
  {
    media: 1024,
    space: 8,
    firstMargin: 24,
    size: 0.33333,
  },
  {
    media: 767,
    space: 8,
    firstMargin: 24,
    size: 0.66667,
  },
];

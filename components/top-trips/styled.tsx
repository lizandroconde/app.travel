import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Title = styled.h2`
  position: relative;
  margin-top: 3rem;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  line-height: 120%;
  font-weight: 800;
  text-align: center;
`;
export const Content = styled.div`
  width: 100%;
   
  padding-bottom: ${({ theme: { toRem } }) => toRem(30)};
`;

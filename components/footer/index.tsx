//# React icons
import { RiMastercardLine, RiVisaLine } from "react-icons/ri";
import { HiOutlinePhone,HiOutlineMail } from "react-icons/hi";
import {Router} from "@routes"
import {
  GrAmex,
  GrFacebook,
  GrInstagram,
  GrTwitter,
  GrYoutube,
} from "react-icons/gr";
import {
  FaCcDinersClub,
  FaPaypal,
  FaStripe,
  FaCcDiscover,
  FaTripadvisor,
} from "react-icons/fa";
//# Component
import { NewLetter } from "@components/footer/newletter";
//# Styles components
import * as C from "./styled";
import { useTranslation } from "next-i18next"
import { useRouter } from "next/router";

//# Types props
type IProps = {};

//# Component => footer
export const Footer = (props: IProps): JSX.Element => {

  const router = useRouter()

  const { t } = useTranslation("common")

  return (
    <C.Container>
      <NewLetter />
      <C.Accredited>
        <C.Title>{t("accredited")}</C.Title>
        <C.Accredits>
          <C.Company
            src="https://1.bp.blogspot.com/-9ZDiOG9uJl4/XtfndvPfzuI/AAAAAAAAgVk/VURX1LGtf9g6NNR4ZMOA-Z6J9SdjdVd0gCLcBGAsYHQ/s1600/CM4Q7WA5IFAK5DAEBZ7DT2ZIMQ.png"
            title="Ministerio de Cultura"
            alt="Ministerio de Cultura"
          />
          <C.Company
            src="https://pbs.twimg.com/profile_images/378800000029930734/fbff30515139b0abd1ef4445b55438d3_400x400.jpeg"
            title="Dircetur"
            alt="Dircetur"
          />
          <C.Company
            src="https://andeanlodges.com/wp-content/uploads/2020/07/footer_6_a.png"
            title="Caltur"
            alt="Caltur"
          />
        </C.Accredits>
      </C.Accredited>
      <C.Destination>
        <C.Center>
          <C.Group>
            <C.Logo
              src="https://images.conde.travel/logo.jpeg"
              title="Conde Travel"
              alt="Conde Travel"
            />
            <C.Menu>
              <C.Item>Sitemap</C.Item>
            </C.Menu>
          </C.Group>
          <C.Group>
            <C.Name>{t("footer.bestdestinations.title")}</C.Name>
            <C.Menu>
              <C.Item onClick={()=>{router.push(t("footer.bestdestinations.machupicchu.link"))}}>{t("footer.bestdestinations.machupicchu.name")}</C.Item>
              <C.Item onClick={()=>{router.push(t("footer.bestdestinations.incatrail.link"))}}>{t("footer.bestdestinations.incatrail.name")}</C.Item>
              <C.Item onClick={()=>{router.push(t("footer.bestdestinations.salkantay.link"))}} >{t("footer.bestdestinations.salkantay.name")}</C.Item>
            </C.Menu>
          </C.Group>
          <C.Group>
            <C.Name>{t("footer.company.title")}</C.Name>
            <C.Menu>
              <C.Item>{t("footer.company.about")}</C.Item>
              

            </C.Menu>
          </C.Group>
          <C.Group>
            <C.Name>{t("footer.issues.title")}</C.Name>
            <C.Menu>
              
              <C.Item onClick={()=>{router.replace(t("footer.issues.terms.link"))}}>{t("footer.issues.terms.name")}</C.Item>
              <C.Item onClick={()=>{router.replace(t("footer.issues.privacy.link"))}}>{t("footer.issues.privacy.name")}</C.Item>
            </C.Menu>
          </C.Group>
          <C.Group>
            <C.Name>{t("footer.contact.title")}</C.Name>
            <C.Menu>
              <C.Item> <HiOutlinePhone/> +51 984 603 305</C.Item>
              <C.Item> <HiOutlineMail/> info@condetraveladventures.con</C.Item>
            </C.Menu>
          </C.Group>
        </C.Center>
      </C.Destination>
      <C.Payment>
        <C.Title>{t("footer.payment")}</C.Title>
        <C.Cards>
          <C.Card>
            <RiMastercardLine />
          </C.Card>
          <C.Card>
            <GrAmex />
          </C.Card>
          <C.Card>
            <RiVisaLine />
          </C.Card>
          <C.Card>
            <FaCcDinersClub />
          </C.Card>
          <C.Card>
            <FaCcDiscover />
          </C.Card>
          <C.Card>
            <FaPaypal />
          </C.Card>
          <C.Card>
            <FaStripe />
          </C.Card>
        </C.Cards>
      </C.Payment>
      <C.Created>
        <C.Auto>
          <C.Author>
            <C.Direction>
              Av. Ayahuayco Mza. O Lote 5 Cusco,Peru 2022 Conde Travel
            </C.Direction>
            <C.Powered>{t("footer.power")} Nexeti Sof</C.Powered>
          </C.Author>
          <C.SocialMedia>
            {t("footer.follow")}
            <C.Social>
             <a href="https://www.facebook.com/CondeTravel" target="_blank" rel="noreferrer"> <GrFacebook /></a>
            </C.Social>
            <C.Social>
            <a href="https://www.instagram.com/condetravel/" target="_blank" rel="noreferrer"><GrInstagram /></a>
            </C.Social>
            <C.Social>
            <a href="https://twitter.com/condetravel" target="_blank" rel="noreferrer">  <GrTwitter /></a>
            </C.Social>
            <C.Social>
            <a href="https://www.youtube.com/c/CondeTravelCusco" target="_blank" rel="noreferrer"> <GrYoutube /></a>
            </C.Social>
            <C.Social>
            <a href="https://www.tripadvisor.com.pe/Attraction_Review-g294314-d6215736-Reviews-Conde_Travel-Cusco_Cusco_Region.html" target="_blank" rel="noreferrer"> <FaTripadvisor /></a>
            </C.Social>
          </C.SocialMedia>
        </C.Auto>
      </C.Created>
    </C.Container>
  );
};

//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;
export const Title = styled.h3`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
  line-height: 120%;
  font-weight: 600;
  text-align: center;
`;
export const Accredited = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-top: ${({ theme: { toRem } }) => toRem(100)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(60)};
  padding-left: ${({ theme: { toRem } }) => toRem(24)};
  padding-right: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Accredits = styled.div`
  width: ${({ theme: { toRem } }) => toRem(840)};
  max-width: 100%;
  margin: auto;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  column-gap: ${({ theme: { toRem } }) => toRem(40)};
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Company = styled.img`
  display: block;
  width: ${({ theme: { toRem } }) => toRem(160)};
  height: ${({ theme: { toRem } }) => toRem(80)};
  object-fit: contain;
`;
export const Destination = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYf5f6f9};
  padding-top: ${({ theme: { toRem } }) => toRem(45)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(45)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Center = styled.div`
  width: ${({ theme: { toRem } }) => toRem(1140)};
  max-width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  column-gap: ${({ theme: { toRem } }) => toRem(40)};
  row-gap: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Group = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
  margin: auto;
`;
export const Logo = styled.img`
  display: block;
  width: ${({ theme: { toRem } }) => toRem(180)};
  object-fit: cover;
  filter: grayscale(1);
`;
export const Name = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: 600;
`;
export const Menu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Item = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY81829c};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
  line-height: 120%;
  font-weight: 400;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;
export const Payment = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: ${({ theme: { toRem } }) => toRem(18)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  padding-top: ${({ theme: { toRem } }) => toRem(36)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(24)};
  padding-left: ${({ theme: { toRem } }) => toRem(24)};
  padding-right: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(24)};
  row-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Card = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({ theme: { toRem } }) => toRem(43)};
  height: ${({ theme: { toRem } }) => toRem(32)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  box-shadow: ${({ theme: { toRem, colors } }) =>
    `0 ${toRem(2)} ${toRem(4)} ${colors.black.rgba.BK000(0.1)}`};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Created = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme: { colors } }) => colors.blue.rgb.BL3b556a};
  padding-top: ${({ theme: { toRem } }) => toRem(36)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Auto = styled.div`
  width: ${({ theme: { toRem } }) => toRem(1140)};
  max-width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-between;
  column-gap: ${({ theme: { toRem } }) => toRem(36)};
  row-gap: ${({ theme: { toRem } }) => toRem(20)};
`;
export const Author = styled.div`
  width: ${({ theme: { toRem } }) => toRem(300)};
  max-width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Direction = styled.div`
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  font-weight: 400;
  text-align: center;
`;
export const Powered = styled.div`
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  font-weight: 400;
  text-align: left;
`;
export const SocialMedia = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(16)};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  font-weight: 400;
`;
export const Social = styled.span`
a{
  text-decoration:none;
  color: inherit;
}
  display: flex;
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(20)};
  cursor: pointer;
  transition: opacity 0.1s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    opacity: 0.6;
  }
`;

//# Styled components
import styled, { css } from "styled-components";

export const Container = styled.div`
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: ${({ theme: { toRem } }) => toRem(24)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRY9facba};
  padding-top: ${({ theme: { toRem } }) => toRem(40)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(24)};
  padding-left: ${({ theme: { toRem } }) => toRem(24)};
  padding-right: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Head = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
  align-items: center;
`;
export const Title = styled.h3`
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  line-height: 120%;
  font-weight: 600;
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRYe4e8ec};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  font-weight: 400;
`;
export const Url = styled(Text)`
  display: inline-block;
  color: ${({ theme: { colors } }) => colors.yellow.rgb.YWffe958};
  font-weight: 500;
  margin-left: ${({ theme: { toRem } }) => toRem(4)};
  margin-right: ${({ theme: { toRem } }) => toRem(4)};
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;
export const GroupForm = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(16)};
  row-gap: ${({ theme: { toRem } }) => toRem(20)};
`;
export const FormControl = styled.div`
  position: relative;
  width: ${({ theme: { toRem } }) => toRem(300)};
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
`;
export const Mail = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY81829c};
  font-size: ${({ theme: { toRem } }) => toRem(20)};
  padding-left: ${({ theme: { toRem } }) => toRem(20)};
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
`;
export const InputControl = styled.input`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(50)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-left: ${({ theme: { toRem } }) => toRem(6)};
  padding-right: ${({ theme: { toRem } }) => toRem(12)};
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
`;

 
export const Error = styled.span`
  position: absolute;
  top: 100%;
  left: 0;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: 120%;
  font-weight: 400;
`;
export const Submit = styled.button<{ isDisabled?: boolean }>`
  width: ${({ theme: { toRem } }) => toRem(150)};
  height: ${({ theme: { toRem } }) => toRem(50)};
  background: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  font-weight: 600;
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  padding-top: ${({ theme: { toRem } }) => toRem(10)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(10)};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  cursor: pointer;
  transition: opacity 0.1s linear;
  ${({isDisabled})=> isDisabled ? css` &:not(:disabled):hover {
    opacity: 0.85;
  }`: css`&:disabled {
    cursor: not-allowed;
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
    color: ${({ theme: { colors } }) => colors.gray.rgb.GRYF2F3F5};
  }`}
 
`;
export const Send = styled.span`
  position: absolute;
  right: ${({ theme: { toRem } }) => toRem(8)};
  top: 50%;
  transform: translateY(-50%);
  z-index: -1;
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({ theme: { toRem } }) => toRem(200)};
  height: ${({ theme: { toRem } }) => toRem(200)};
  border-radius: 50%;
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRY9aa7b5};
  font-size: ${({ theme: { toRem } }) => toRem(100)};
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRYa6b2bf};
`;

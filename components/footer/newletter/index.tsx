//# React
import { Fragment, useState } from "react";
//# React hooks form
import { useForm } from "react-hook-form";
//# React icons
import { HiOutlineMail, HiOutlineUser} from "react-icons/hi";
import { RiMailSendLine } from "react-icons/ri";
import { useTranslation } from "next-i18next";
//# Styles components
import * as C from "./styled";
import { NEW_NEWLETTER } from "@gql/mutation/mutation";

//# Types props
type ISchemaForm = {
  email: string;
  name: string;
};
type IProps = {};

//# Component => newletter
export const NewLetter = (props: IProps): JSX.Element => {
  const { t } = useTranslation("common");
  //# State
  const [loading, setLoading] = useState<boolean>(false);
  //# Form hooks
  const {
    handleSubmit,
    register,
    reset,
    formState: { errors, isDirty, isValid },
  } = useForm<ISchemaForm>({
    mode: "onChange",
    defaultValues: {
      email: "",
      name: "",
    },
  });
  //# Methods
  const onSubmit = async (data: ISchemaForm): Promise<void> => {
    setLoading(true);
    NEW_NEWLETTER
    reset({
      email: "",
      name: "",
    });

    setLoading(false);
  };

  return (
    <C.Container>
      <C.Head>
        <C.Title>{t("newsletter.title")}</C.Title>
        <C.Text>{t("newsletter.info")}</C.Text>
      </C.Head>
      <C.GroupForm>
        <C.FormControl>
          <C.Mail>
            <HiOutlineMail />
          </C.Mail>
          <C.InputControl
            {...register("email", {
              required: "Ingrese un correo correcto.",
              
            })}
            placeholder={t("newsletter.youremail")}
          />
          {errors && <C.Error>{errors.email?.message}</C.Error>}
        </C.FormControl>

        <C.FormControl
          style={{maxWidth: "200px"}}
        >
          <C.Mail>
            <HiOutlineUser />
          </C.Mail>
          <C.InputControl
            {...register("name", {
              required: "Ingrese Nombre",
              
            })}
            placeholder={t("newsletter.yourname")}
          />
          {errors && <C.Error>{errors.name?.message}</C.Error>}
        </C.FormControl>
         
        <C.Submit
          onClick={handleSubmit(onSubmit)}
          disabled={!isDirty || !isValid}
        >
          {loading ? (
            <Fragment>Subscribiendo...</Fragment>
          ) : (
            <Fragment>{t("newsletter.subscribe")}</Fragment>
          )}

         
        </C.Submit>
      </C.GroupForm>
      <C.Text>{t("newsletter.terms")}</C.Text>
      <C.Send>
        <RiMailSendLine />
      </C.Send>
    </C.Container>
  );
};

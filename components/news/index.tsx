//# Components
import { Newtime, INewtime } from "@components/news/newtime";
import { LastNewtime, ILastNewtime } from "@components/news/last-newtime";
//# Styles components
import * as C from "./styled";
import { Center } from "@themes/global";
import { useTranslation } from "next-i18next"

//# Types props
type IProps = {};

//# Component => news
export const News = (props: IProps): JSX.Element => {
  const { t } = useTranslation("common")
  return (
    <C.Container>
      <Center>
        <C.Head>
          <C.Title>{t("news.title")}</C.Title>
          <C.Text>{t("news.description")}</C.Text>
        </C.Head>
        <C.Content>
          <C.News>
            {newtimes.map(
              (newtime: INewtime, e: number): JSX.Element => (
                <Newtime key={e} newtime={newtime} />
              )
            )}
          </C.News>
          <C.Articles>
            {lastNewtimes.map(
              (newtime: ILastNewtime, e: number): JSX.Element => (
                <LastNewtime key={e} newtime={newtime} />
              )
            )}
          </C.Articles>
        </C.Content>
      </Center>
    </C.Container>
  );
};

const newtimes: INewtime[] = [
  {
    photo:
      "https://www.caminodelosincas.com/wp-content/uploads/2020/08/Paquete-a-Machu-Picchu-5-Dias-1080x675.jpg",
    tag: "GUIDES & TIP",
    title: "Explore the Most Beautiful Places in Cusco With This Epic Trip",
  },
  {
    photo:
      "https://www.salkantaytrekking.com/trekking-in-peru/cusco/salkantay-trek-4d/header-image.jpg",
    tag: "SEE & DO",
    title: "New Salkantay Route to Machu Picchu",
  },
];
const lastNewtimes: ILastNewtime[] = [
  {
    photo:
      "https://d1uz88p17r663j.cloudfront.net/resized/af2fdda6c48894161fc865e695c40c91_CEVICHE_DE_PESCADO_1200_600.jpg",
    tag: "FOOD & DRINK",
    title: "New Salkantay Route to Machu Picchu",
  },
  {
    photo:
      "https://www.todoenperu.net/wp-content/uploads/2019/03/cocalmayo.jpg",
    tag: "HEALTH & WELLNESS",
    title: "Cocalmayo, the best natural spas to visit in Cusco",
  },
  {
    photo: "https://www.imperiostravel.com/images/tours/huchuy-qosqo.jpg",
    tag: "GUIDES & TIPS",
    title: "Huchuy Qosqo - Machu Picchu 3D/2N",
  },
];

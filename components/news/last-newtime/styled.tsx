//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: grid;
  align-items: flex-start;
  grid-template-columns: 1.2fr 2fr;
  grid-column-gap: ${({ theme: { toRem } }) => toRem(12)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
`;
export const Photo = styled.div`
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(110)};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRY9facba};
  overflow: hidden;
  cursor: pointer;
`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(12)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Tag = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY9facba};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: 120%;
  font-weight: 500;
  text-transform: uppercase;
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  line-height: 120%;
  font-weight: 600;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

//# React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
//# Styles components
import * as C from "./styled";

//# Types props
export type ILastNewtime = {
  photo: string;
  tag: string;
  title: string;
};
type IProps = {
  newtime: ILastNewtime;
};

//# Component => last newtime
export const LastNewtime = ({ newtime }: IProps): JSX.Element => {
  //# Destroy
  const { photo, tag, title } = newtime;
  return (
    <C.Container>
      <C.Photo>
        <LazyLoadImage
          effect="black-and-white"
          width="100%"
          height="100%"
          src={photo}
          title={title}
          alt={tag}
        />
      </C.Photo>
      <C.Content>
        <C.Tag>{tag}</C.Tag>
        <C.Title>{title}</C.Title>
      </C.Content>
    </C.Container>
  );
};

//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(24)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(24)};
  padding-right: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Head = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(12)};
`;
export const Title = styled.h2`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  line-height: 120%;
  font-weight: 600;
  text-align: center;
`;
export const Text = styled.h2`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY9facba};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
  line-height: 120%;
  font-weight: 400;
  text-align: center;
`;
export const Content = styled.div`
  display: grid;
  align-items: flex-start;
  grid-template-columns: 60% 40%;
  grid-column-gap: ${({ theme: { toRem } }) => toRem(24)};
  grid-row-gap: ${({ theme: { toRem } }) => toRem(36)};
  ${({ theme: { media } }) => media.to(1184)} {
    grid-template-columns: 50% 50%;
  }
  ${({ theme: { media } }) => media.to(884)} {
    grid-template-columns: 1fr;
  }
`;
export const News = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${({ theme: { toRem } }) => toRem(24)};
  grid-row-gap: ${({ theme: { toRem } }) => toRem(24)};
  ${({ theme: { media } }) => media.to(1024)} {
    grid-template-columns: 1fr;
  }
`;
export const Articles = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(12)};
`;

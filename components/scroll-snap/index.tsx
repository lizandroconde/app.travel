//# React
import { ReactNode, useRef, useState, useEffect, useCallback } from "react";
//# Hooks
import { useMedia } from "@hooks/useMedia";
//# Interfaces
import { IResponsive, IMediaQuery } from "@interfaces/scroll-snap";
//# React icons
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
//# Styles components
import * as C from "./styled";

//# Types props
type IScrollSnapProps = {
  defaultSize?: number;
  responsive: IResponsive[];
  children: ReactNode[];
};

//# Component => scroll snap
export const ScrollSnap = ({
  children,
  defaultSize,
  responsive,
}: IScrollSnapProps): JSX.Element => {
  //# Refs
  const scrollRef = useRef<HTMLDivElement>(null);
  const slideRefs = useRef<HTMLLIElement[]>([]);
  const hideArrowThreshold: number = 5;
  //# States
  const [isLeft, setIsLeft] = useState<boolean>(false);
  const [isRight, setIsRight] = useState<boolean>(false);
  const [isScrolling, setIsScrolling] = useState<boolean>(false);
  const [step, setStep] = useState<number>(0);
  const [steps, setSteps] = useState<number>(0);
  //# Media
  const media: IMediaQuery = {
    space: 16,
    firstMargin: 0,
    size: defaultSize || 0.33333,
  };
  responsive.map((m: IResponsive) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const match: boolean = useMedia(`(max-width: ${m.media}px)`);
    if (match) {
      media.space = m.space;
      media.firstMargin = m.firstMargin;
      media.size = m.size;
    }
  });
  //# Methods
  const addNode = useCallback((node: HTMLLIElement, index: number) => {
    slideRefs.current[index] = node;
  }, []);
  const getSlideWidth = useCallback(
    (): number =>
      (scrollRef.current?.firstChild?.firstChild as HTMLUListElement)
        ?.clientWidth || 0,
    []
  );
  const onSliderScroll = (): void => {
    setTimeout(() => {
      setIsScrolling(false);
    }, 250);
    if (!isScrolling) {
      setIsScrolling(true);
    }
  };
  const isSliderScrollable = useCallback((): boolean => {
    if (!scrollRef.current) return false;
    const sliderWidth: number = scrollRef.current.clientWidth;
    const slideWidth: number = getSlideWidth() - 1;
    return slideRefs.current.length * slideWidth > sliderWidth;
  }, [getSlideWidth]);
  const onChangeScroll = (direction: "prev" | "next"): void => {
    const dir = direction === "prev" ? -1 : 1;
    if (scrollRef.current) {
      const slideWidth: number = getSlideWidth();
      const slidesToScroll: number = Math.floor(
        scrollRef.current.clientWidth / slideWidth
      );
      scrollRef.current.scrollBy({
        top: 0,
        behavior: "smooth",
        left: slidesToScroll * slideWidth * dir,
      });
      onStep();
    }
  };
  const onStep = useCallback((): void => {
    if (scrollRef.current) {
      const slideWidth: number = getSlideWidth();
      const slidesToScroll: number = Math.floor(
        scrollRef.current.clientWidth / slideWidth
      );
      const screen: number = slideWidth * slidesToScroll;
      const step: number =
        Math.floor(scrollRef.current.scrollLeft / screen) + 1;
      setStep(step);
      setSteps(children.length / slidesToScroll);
    }
  }, [children.length, getSlideWidth]);
  //# Prerender
  useEffect(() => {
    if (!isSliderScrollable()) return;
    if (!scrollRef.current) return;
    if (scrollRef.current.scrollLeft <= hideArrowThreshold) {
      setIsLeft(false);
      setIsRight(true);
    } else if (
      scrollRef.current.clientWidth + scrollRef.current.scrollLeft >=
      scrollRef.current.scrollWidth - hideArrowThreshold
    ) {
      setIsLeft(true);
      setIsRight(false);
    } else {
      setIsLeft(true);
      setIsRight(true);
    }
    if (scrollRef.current) {
      new ResizeObserver(onStep).observe(scrollRef.current);
    }
  }, [isScrolling, isSliderScrollable, onStep]);

  return (
    <C.Container>
      <C.GroupRow>
        <C.Steps>
          <C.Step>{step}</C.Step>
          <C.Space>/</C.Space>
          <C.Step>{steps}</C.Step>
        </C.Steps>
        <C.Row disabled={!isLeft} onClick={() => onChangeScroll("prev")}>
          <FiChevronLeft />
        </C.Row>
        <C.Row disabled={!isRight} onClick={() => onChangeScroll("next")}>
          <FiChevronRight />
        </C.Row>
      </C.GroupRow>
      <C.Content>
        <C.Scroll onScroll={onSliderScroll} ref={scrollRef}>
          <C.Grid>
            {children.map(
              (child: ReactNode, e: number): JSX.Element => (
                <C.Snap
                  key={e}
                  ref={(node: HTMLLIElement) => addNode(node, e)}
                  firstMargin={media.firstMargin}
                  space={media.space}
                  size={media.size}
                >
                  {child}
                </C.Snap>
              )
            )}
          </C.Grid>
        </C.Scroll>
      </C.Content>
    </C.Container>
  );
};

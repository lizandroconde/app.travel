//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
`;
export const GroupRow = styled.div`
  width: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  column-gap: ${({ theme: { toRem } }) => toRem(8)};
  margin-left: auto;
`;
export const Steps = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
  margin-right: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Step = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  text-align: center;
`;
export const Space = styled(Step)`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK555};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
`;
export const Row = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: ${({ theme: { toRem } }) => toRem(35)};
  height: ${({ theme: { toRem } }) => toRem(35)};
  border-radius: ${({ theme: { toRem } }) => toRem(20)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  box-shadow: ${({ theme: { toRem, colors } }) =>
    `0 ${toRem(1)} ${toRem(2)} 0 ${colors.black.rgba.BK000(0.25)}`};
  transition: background 0.1s linear, box-shadow 0.2s linear;
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(23)};
  cursor: pointer;
  &:hover:not(:disabled) {
    box-shadow: ${({ theme: { toRem, colors } }) =>
      `0 ${toRem(1)} ${toRem(2)} 0 ${colors.black.rgba.BK000(0.35)}`};
  }
  &:disabled {
    color: ${({ theme }) => theme.colors.gray.rgb.GRYCCC};
    box-shadow: none;
    cursor: not-allowed;
  }
`;
export const Content = styled.div`
  display: block;
  margin-left: calc(-1 * ${({ theme: { toRem } }) => toRem(0)});
  margin-right: calc(-1 * ${({ theme: { toRem } }) => toRem(0)});
  ${({ theme: { media } }) => media.to(1184)} {
    margin-left: calc(-1 * ${({ theme: { toRem } }) => toRem(24)});
    margin-right: calc(-1 * ${({ theme: { toRem } }) => toRem(24)});
  }
`;
export const Scroll = styled.div`
  display: block;
  overflow-x: scroll;
  scroll-behavior: smooth;
  scroll-snap-type: x mandatory;
  -webkit-overflow-scrolling: touch;
  padding: ${({ theme: { toRem } }) => toRem(4)};
  margin: calc(-1 * ${({ theme: { toRem } }) => toRem(4)});
  ::-webkit-scrollbar {
    display: none;
  }
`;
export const Grid = styled.ul`
  display: flex;
  list-style: none;
  &::after {
    display: block;
    content: "";
    width: ${({ theme: { toRem } }) => toRem(24)};
    flex-grow: 0;
    flex-shrink: 0;
  }
`;
type ISnap = {
  space: number;
  firstMargin: number;
  size: number;
};
export const Snap = styled.li<ISnap>`
  width: calc(
    ${({ firstMargin, space, size, theme: { toRem } }) =>
      `(100% + ${toRem(space)} - 2 * ${toRem(firstMargin)}) * ${size} - ${toRem(
        space
      )}`}
  );
  scroll-snap-align: start;
  scroll-margin-left: calc(
    ${({ firstMargin, theme: { toRem } }) =>
      `${toRem(firstMargin)} + ${toRem(4)}`}
  );
  scroll-margin-right: calc(
    ${({ firstMargin, theme: { toRem } }) =>
      `${toRem(firstMargin)} + ${toRem(4)}`}
  );
  &:not(:last-of-type) {
    margin-right: ${({ space, theme: { toRem } }) => toRem(space)};
  }
  &:first-of-type {
    margin-left: ${({ firstMargin, theme: { toRem } }) => toRem(firstMargin)};
  }
  flex-grow: 0;
  flex-shrink: 0;
`;

import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(24)};
  padding-top: ${({ theme: { toRem } }) => toRem(24)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(24)};
`;
export const Head = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(12)};
`;
export const Seller = styled.div`
  display: block;
  background: ${({ theme: { colors } }) => colors.orange.rgb.OF28023};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(13)};
  line-height: 120%;
  text-align: center;
  margin-right: auto;
  padding-top: ${({ theme: { toRem } }) => toRem(2)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(2)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  border-radius: ${({ theme: { toRem } }) => toRem(20)};
`;
export const Title = styled.h1`
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  font-weight: 700;
  line-height: 120%;
  text-align: left;
`;
export const Group = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: space-between;
  column-gap: ${({ theme: { toRem } }) => toRem(40)};
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
`;
export const SubTitle = styled.div`
  display: flex;
  align-items: baseline;
  flex-wrap: wrap;
  column-gap: ${({ theme: { toRem } }) => toRem(40)};
  row-gap: ${({ theme: { toRem } }) => toRem(8)};
`;
export const Ratings = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Star = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.orange.rgb.OGFF946D};
  font-size: ${({ theme: { toRem } }) => toRem(15)};
`;
export const Location = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const World = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
`;
export const TextRoute = styled.div`
  color: ${({ theme: { colors } }) => colors.gray.rgb.GRY151743};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
`;
export const Actions = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(10)};
`;
export const Action = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  column-gap: ${({ theme: { toRem } }) => toRem(6)};
`;
export const Icon = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(14)};
  line-height: 120%;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;
export const Gallery = styled.div`
  position: relative;
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(300)};
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-column-gap: ${({ theme: { toRem } }) => toRem(10)};
  grid-row-gap: ${({ theme: { toRem } }) => toRem(10)};
`;
type IGridArea = {
  [i: string]: string;
};
const gridArea1: IGridArea = {
  1: "1 / 1 / 3 / 3",
  2: "1 / 3 / 3 / 5",
  3: "1 / 5 / 2 / 6",
  4: "2 / 5 / 3 / 6",
  5: "1 / 6 / 3 / 8",
};
const gridArea2: IGridArea = {
  1: "1 / 1 / 3 / 4",
  2: "1 / 4 / 2 / 6",
  3: "2 / 4 / 3 / 6",
  4: "1 / 6 / 2 / 8",
  5: "2 / 6 / 3 / 8",
};
const gridArea3: IGridArea = {
  1: "1 / 1 / 2 / 5",
  2: "1 / 5 / 2 / 8",
  3: "2 / 1 / 3 / 3",
  4: "2 / 3 / 3 / 5",
  5: "2 / 5 / 3 / 8",
};
export const Picture = styled.div<{ position: number }>`
  position: relative;
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYE0E0E0};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  grid-area: ${({ position }) => gridArea1[position]};
  ${({ theme: { media } }) => media.to(824)} {
    grid-area: ${({ position }) => gridArea2[position]};
  }
  ${({ theme: { media } }) => media.to(640)} {
    grid-area: ${({ position }) => gridArea3[position]};
  }
`;
export const ViewMore = styled.button`
  position: absolute;
  right: ${({ theme: { toRem } }) => toRem(8)};
  bottom: ${({ theme: { toRem } }) => toRem(10)};
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
  white-space: nowrap;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  padding-top: ${({ theme: { toRem } }) => toRem(4)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(4)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  font-weight: 400;
  line-height: 120%;
  cursor: pointer;
`;
export const ViewIcon = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
`;

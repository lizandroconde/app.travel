// React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
// Interfaces
import { ITripPage, IGallery } from "@interfaces/trip";
import { SRLWrapper ,useLightbox} from "simple-react-lightbox";
// Hooks
import { useRating } from "@hooks/useRating";
// Reatc icons
import { GiWorld } from "react-icons/gi";
import { FaShare } from "react-icons/fa";
import { AiFillHeart } from "react-icons/ai";
import { CgMenuGridR } from "react-icons/cg";
// Styles
import * as Brick from "./styled";
import { useContext } from "react";
import { TripContext } from "@hooks/useContext";
import { useTranslation } from "next-i18next";
 

// Component => banner trip
export const BannerTrip = (): JSX.Element => {
  const { openLightbox, closeLightbox } = useLightbox()
  // Destroy
  const {t}= useTranslation("trip")
  const photos: number[] = [1,2, 3, 4, 5];
  const { info:{Title, Valoration, Gallery, Location }} = useContext(TripContext);
  photos.splice(0, Gallery.length);
   
  const [ratings] = useRating(Valoration.Ratio, Valoration.Cant);

  let  lightbox = []
for(let img of Gallery){
  lightbox.push({
     
    src: img.url,
    caption: img?.name,
  })
}

  return (
    <Brick.Container>
      <Brick.Head>
       {/*<Brick.Seller>Special offer</Brick.Seller>*/}
        <Brick.Title>{Title}</Brick.Title>
        <Brick.Group>
          <Brick.SubTitle>
            <Brick.Ratings>
              {ratings.map(
                (Star, e: number): JSX.Element => (
                  <Brick.Star key={e}>
                    <Star />
                  </Brick.Star>
                )
              )}
              ({Valoration.Cant} {t("review")})
            </Brick.Ratings>
            <Brick.Location>
              <Brick.World>
                <GiWorld />
              </Brick.World>
              <Brick.TextRoute>{Location}</Brick.TextRoute>
            </Brick.Location>
          </Brick.SubTitle>
          <Brick.Actions>
            <Brick.Action>
              <Brick.Icon>
                <FaShare />
              </Brick.Icon>
              <Brick.Text>{t("shared")}</Brick.Text>
            </Brick.Action>
            <Brick.Action>
              <Brick.Icon>
                <AiFillHeart />
              </Brick.Icon>
              <Brick.Text>{t("save")}</Brick.Text>
            </Brick.Action>
          </Brick.Actions>
        </Brick.Group>
      </Brick.Head>
      <SRLWrapper elements={lightbox}/>
      
      <Brick.Gallery>
      
        {Gallery?.map((photo: IGallery, e: number): JSX.Element => {
          const position: number = e + 1;
          
          return (
            <Brick.Picture key={e} position={position}>
                 
              <LazyLoadImage
                effect="blur"
                width="100%"
                height="100%"
                alt={photo.alt}
                title={photo?.name}
                src={photo?.url}
              />
               
            </Brick.Picture>
          );
        })}
          
        {Gallery?.length <= 5 &&
          photos.map(( e: number): JSX.Element => {
             
            return <Brick.Picture key={e} position={e} />;
          })}
        <Brick.ViewMore onClick={()=>openLightbox()}>
          <Brick.ViewIcon>
            <CgMenuGridR />
          </Brick.ViewIcon>
          {t("showgallery")}
        </Brick.ViewMore>
      </Brick.Gallery>
       
    </Brick.Container>
  );
};

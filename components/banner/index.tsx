//# React lazy load
import { LazyLoadImage } from "react-lazy-load-image-component";
import { SearchTravels } from "./search-travels";
//# Styles components
import * as C from "./styled";

//# Types proops
type IBannerProps = {
  title?: string;
  slogan?: string;
  img?: string;
};

//# Component => banner trip
export const Banner = ({ title, slogan, img }: IBannerProps) => {
  return (
    <C.Container>
      <C.Photo>
        <LazyLoadImage
          effect="black-and-white"
          width="100%"
          height="100%"
          src={img}
          title={title}
          alt={slogan}
        />
      </C.Photo>
      <C.Content>
        <C.Center>
          <C.Title>{title}</C.Title>
          <C.Slogan>{slogan}</C.Slogan>
        </C.Center>
      </C.Content>
      <C.Searching>
        <SearchTravels />
      </C.Searching>
    </C.Container>
  );
};

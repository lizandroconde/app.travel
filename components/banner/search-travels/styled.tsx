//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(9)};
  margin-top: ${({ theme: { toRem } }) => toRem(-100)};
  padding: ${({ theme: { toRem } }) => toRem(8)};
  box-shadow: ${({ theme: { toRem, colors } }) =>
    `0 0 ${toRem(8)} ${colors.black.rgba.BK000(0.2)}`};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    max-width: 83.33333%;
    margin-top: ${({ theme: { toRem } }) => toRem(-80)};
    padding: ${({ theme: { toRem } }) => toRem(12)};
    border-radius: ${({ theme: { toRem } }) => toRem(13)};
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
    padding-bottom: ${({ theme: { toRem } }) => toRem(24)};
  }
`;
export const Searching = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  gap: ${({ theme: { toRem } }) => toRem(8)};
  ${({ theme: { media } }) => media.to(814)} {
    flex-direction: column;
  }
`;
export const Search = styled.button`
  width: 100%;
  min-width: ${({ theme: { toRem } }) => toRem(250)};
  background: ${({ theme: { colors } }) => colors.green.rgb.GR72A842};
  border-radius: ${({ theme: { toRem } }) => toRem(8)};
  padding: ${({ theme: { toRem } }) => toRem(12)};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  line-height: ${({ theme: { toRem } }) => toRem(30)};
  cursor: pointer;
  transition: opacity 0.2s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    opacity: 0.92;
  }
`;
export const Filtering = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  column-gap: ${({ theme: { toRem } }) => toRem(24)};
  row-gap: ${({ theme: { toRem } }) => toRem(10)};
  padding-left: ${({ theme: { toRem } }) => toRem(12)};
  padding-right: ${({ theme: { toRem } }) => toRem(12)};
`;
export const FilterText = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: 600;
  ${({ theme: { media } }) => media.to(814)} {
    display: block;
    width: 100%;
  }
`;
export const CheckControl = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
`;
export const Check = styled.span<{ active?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ theme: { toRem } }) => toRem(22)};
  min-width: ${({ theme: { toRem } }) => toRem(22)};
  height: ${({ theme: { toRem } }) => toRem(22)};
  background: ${({ active, theme: { colors } }) =>
    active ? colors.black.rgb.BL000 : colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  border-width: ${({ theme: { toRem } }) => toRem(2)};
  border-color: ${({ active, theme: { colors } }) =>
    active ? colors.black.rgb.BL000 : colors.gray.rgb.GRYDDD};
  border-style: solid;
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  cursor: pointer;
`;
export const Title = styled.div`
  display: block;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: 400;
  cursor: pointer;
`;

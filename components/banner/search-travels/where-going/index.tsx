//# React
import { useState, FormEvent, useEffect } from "react";
//# React outsite
import useOnclickOutside, { Return } from "react-cool-onclickoutside";
//# React hook form
import { useController, UseControllerProps } from "react-hook-form";
//# Router 
import {Router} from "@routes"
//# Interfaces
import { IFormData } from "../index";
// React icons
import { MdLocationOn } from "react-icons/md";
//# Styles components
import * as C from "./styled";
import { SEARCH_HOME } from "@gql/global/querys";
import { useLazyQuery } from "@apollo/client";
import { useRouter } from "next/router";

//# Types props
type IWhereGoingProps = UseControllerProps<IFormData>;
var intervalo: any;
//# Component => where going
export const WhereGoing = (props: IWhereGoingProps): JSX.Element => {
  //# States
  const [active, setActive] = useState<boolean>(false);
  const [goings, setGoings] = useState<IData[]>([]);
  const [value, setValue] = useState<string>("");
  //# Hooks
  const { field } = useController(props);
  const ref: Return = useOnclickOutside(() => {
    setActive(false);
  });

  const router = useRouter();
  const locale = router?.query?.locale || "en";
   //# Use Lazy
   const [getResult, { data }] = useLazyQuery(SEARCH_HOME);

   const handleResult = async (query: any) => {
       getResult(query);
   };

   useEffect(() => {
       data && setGoings(data?.getSearchHome?.data);
   }, [data]);
  //# Methods
  const onHandleChange = (e: FormEvent<HTMLInputElement>): void => {
    const { value } = e.currentTarget;
    setValue(value);
    field.onChange(value);
    

    if (value.length > 2) {
        clearTimeout(intervalo);

        intervalo = setTimeout(() => {
          handleResult({
                variables: { slug: locale, text: value },
            });

            clearTimeout(intervalo); //Limpio el intervalo
        }, 1000);
    }
     
  };
  const onFocus = (): void => {
    setActive(true);
  };
  const onSelect = (value: string): void => {
    setValue(value);
    field.onChange(value);
    setActive(false);
  };

  return (
    <C.Container ref={ref}>
      <C.FormControl onClick={onFocus}>
        <C.Location>
          <MdLocationOn />
        </C.Location>
        <C.InputControl
          ref={field.ref}
          name={field.name}
          value={value}
          onChange={onHandleChange}
          placeholder="Where are you going?"
        />
      </C.FormControl>
      {active && (
        <C.AutoComplete>
          {goings?.length > 0 ? (
            goings?.map(
              (item: IData, e: number): JSX.Element => (
                <C.AutoCompleteItem
                  key={e}
                  onClick={()=>router.push(item.url)}
                >
                  <C.Text>{item.name}</C.Text>
                  <C.SubText>{item.type}</C.SubText>
                </C.AutoCompleteItem>
              )
            )
          ) : (
            <C.AutoCompleteItem>
              <C.Text>No se encontraron datos</C.Text>
            </C.AutoCompleteItem>
          )}
        </C.AutoComplete>
      )}
    </C.Container>
  );
};

type IData = {
  name: string;
  url: string;
  type: string;
}; 
 
//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(4)};
`;
export const FormControl = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  transition: background 0.2s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYF2F3F5};
  }
`;
export const Location = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
`;
export const InputControl = styled.input`
  background: transparent;
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: normal;
  &:focus::placeholder {
    color: transparent;
  }
`;
export const AutoComplete = styled.div`
  position: absolute;
  top: calc(100% + ${({ theme: { toRem } }) => toRem(8)});
  right: 0;
  width: 100%;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  z-index: 100;
  box-shadow: ${({ theme: { toRem, colors } }) => `
    0 ${toRem(16)} ${toRem(24)} rgb(3 54 63 / 16%), 0 ${toRem(8)} ${toRem(
    16
  )} rgb(3 54 63 / 4%),
    0 ${toRem(4)} ${toRem(8)} rgb(3 54 63 / 4%), 0 ${toRem(2)} ${toRem(
    4
  )} rgb(3 54 63 / 2%),
    0 ${toRem(1)} ${toRem(2)} rgb(3 54 63 / 4%), 0 ${toRem(-1)} ${toRem(
    2
  )} rgb(3 54 63 / 4%)
    `};
  border-radius: ${({ theme: { toRem } }) => toRem(13)};
  padding-top: ${({ theme: { toRem } }) => toRem(12)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  padding-right: ${({ theme: { toRem } }) => toRem(8)};
`;
export const AutoCompleteItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: ${({ theme: { toRem } }) => toRem(12)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(12)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  border-radius: ${({ theme: { toRem } }) => toRem(9)};
  cursor: pointer;
  transition: background 0.2s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  }
`;
export const Text = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: ${({ theme: { toRem } }) => toRem(24)};
`;
export const SubText = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(12)};
  line-height: ${({ theme: { toRem } }) => toRem(24)};
  text-align: right;
`;

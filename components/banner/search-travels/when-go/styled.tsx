//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(4)};
`;
export const FormControl = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme: { toRem } }) => toRem(4)};
  background: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-radius: ${({ theme: { toRem } }) => toRem(4)};
  cursor: pointer;
  transition: background 0.2s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYF2F3F5};
  }
`;
export const Calendar = styled.span`
  display: flex;
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
`;
export const SelectControl = styled.div<{ active?: boolean }>`
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(8)};
  color: ${({ active, theme: { colors } }) =>
    !active ? colors.gray.rgb.GRY151743 : colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: normal;
`;
export const AutoCalendar = styled.div`
  position: absolute;
  top: calc(100% + ${({ theme: { toRem } }) => toRem(8)});
  left: 0;
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme: { toRem } }) => toRem(16)};
  width: ${({ theme: { toRem } }) => toRem(300)};
  min-width: 100%;
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  z-index: 100;
  box-shadow: ${({ theme: { toRem, colors } }) => `
    0 ${toRem(16)} ${toRem(24)} rgb(3 54 63 / 16%), 0 ${toRem(8)} ${toRem(
    16
  )} rgb(3 54 63 / 4%),
    0 ${toRem(4)} ${toRem(8)} rgb(3 54 63 / 4%), 0 ${toRem(2)} ${toRem(
    4
  )} rgb(3 54 63 / 2%),
    0 ${toRem(1)} ${toRem(2)} rgb(3 54 63 / 4%), 0 ${toRem(-1)} ${toRem(
    2
  )} rgb(3 54 63 / 4%)
    `};
  border-radius: ${({ theme: { toRem } }) => toRem(13)};
  padding-top: ${({ theme: { toRem } }) => toRem(16)};
  padding-bottom: ${({ theme: { toRem } }) => toRem(16)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
`;
export const MonthSelector = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  column-gap: ${({ theme: { toRem } }) => toRem(16)};
`;
export const Year = styled.div`
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: 600;
  text-align: left;
`;
export const Arrows = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Arrow = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${({ theme: { toRem } }) => toRem(44)};
  height: ${({ theme: { toRem } }) => toRem(44)};
  background: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(24)};
  cursor: pointer;
  transition: color 0.2s cubic-bezier(0.075, 0.82, 0.165, 1);
  &:hover {
    color: ${({ theme: { colors } }) => colors.black.rgb.BLK4A5A5A};
  }
  &:disabled {
    color: ${({ theme: { colors } }) => colors.gray.rgb.GRYCCC};
  }
`;
export const Months = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  gap: ${({ theme: { toRem } }) => toRem(8)};
`;
export const Month = styled.button<{ active?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: calc(33.33% - ${({ theme: { toRem } }) => toRem(8)});
  height: ${({ theme: { toRem } }) => toRem(48)};
  background: ${({ active, theme: { colors } }) =>
    active ? colors.black.rgb.BL000 : colors.white.rgb.WFFF};
  border-radius: ${({ theme: { toRem } }) => toRem(9)};
  border-width: ${({ theme: { toRem } }) => toRem(1)};
  border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  border-style: solid;
  color: ${({ active, theme: { colors } }) =>
    active ? colors.white.rgb.WFFF : colors.black.rgb.BL000};
  font-size: ${({ theme: { toRem } }) => toRem(16)};
  line-height: 120%;
  font-weight: 400;
  letter-spacing: 0.2;
  cursor: pointer;
  transition: border 0.2s cubic-bezier(0.165, 0.84, 0.44, 1);
  &:hover {
    border-color: ${({ theme: { colors } }) => colors.black.rgb.BL000};
  }
  &:disabled {
    color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
    border-color: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
    cursor: not-allowed;
  }
`;

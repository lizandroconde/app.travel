//# React
import { useState } from "react";
//# Moment
import moment from "moment";
//# React outsite
import useOnclickOutside, { Return } from "react-cool-onclickoutside";
//# React hook form
import { useController, UseControllerProps } from "react-hook-form";
//# Interfaces
import { IFormData } from "../index";
// React icons
import { IoCalendarOutline } from "react-icons/io5";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
//# Styles components
import * as C from "./styled";

//# Types props
type IWhenGoProps = UseControllerProps<IFormData>;

//# Component => when go
export const WhenGo = (props: IWhenGoProps): JSX.Element => {
  //# Def
  const months: string[] = moment.monthsShort();
  const yearNow: number = moment().year();
  const monthNow: number = moment().month() + 1;
  //# States
  const [active, setActive] = useState<boolean>(false);
  const [value, setValue] = useState<string>("");
  const [newYear, setNewYear] = useState<number>(yearNow);
  const [year, setYear] = useState<number>(0);
  const [month, setMonth] = useState<string>("");
  //# Hooks
  const { field } = useController(props);
  const ref: Return = useOnclickOutside(() => {
    setActive(false);
  });
  //# Methods
  const onFocus = (): void => setActive(true);
  const onPrevYear = (): void => setNewYear((year) => year - 1);
  const onNextYear = (): void => setNewYear((year) => year + 1);
  const onSelect = (month: string, year: number): void => {
    const value: string = `${month} ${year}`;
    setYear(year);
    setMonth(month);
    setValue(value);
    field.onChange(value);
    setActive(false);
  };

  return (
    <C.Container ref={ref}>
      <C.FormControl onClick={onFocus}>
        <C.Calendar>
          <IoCalendarOutline />
        </C.Calendar>
        <C.SelectControl active={value.length > 0}>
          {value ? value : !active ? "When would you go?" : ""}
        </C.SelectControl>
      </C.FormControl>
      {active && (
        <C.AutoCalendar>
          <C.MonthSelector>
            <C.Year>{newYear}</C.Year>
            <C.Arrows>
              <C.Arrow disabled={newYear === yearNow} onClick={onPrevYear}>
                <BsChevronLeft />
              </C.Arrow>
              <C.Arrow onClick={onNextYear}>
                <BsChevronRight />
              </C.Arrow>
            </C.Arrows>
          </C.MonthSelector>
          <C.Months>
            {months.map((value: string, e: number): JSX.Element => {
              const monthNumber: number = e + 1;
              const active: boolean = newYear === year && value === month;
              const disabled: boolean =
                newYear === yearNow && monthNumber < monthNow ? true : false;

              return (
                <C.Month
                  key={e}
                  disabled={disabled}
                  active={active}
                  onClick={() => onSelect(value, newYear)}
                >
                  {value}
                </C.Month>
              );
            })}
          </C.Months>
        </C.AutoCalendar>
      )}
    </C.Container>
  );
};

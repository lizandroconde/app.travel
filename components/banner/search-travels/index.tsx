// React
import { useState } from "react";
//# React hook form
import { useForm, useController, UseControllerProps } from "react-hook-form";
//# Components
import { WhereGoing } from "./where-going";
import { WhenGo } from "./when-go";
// React icons
import { GoCheck } from "react-icons/go";
//# Styled components
import * as C from "./styled";

//# Types props
export type IFormData = {
  whereGoing: string;
  whenGo: string;
  groupTours: boolean;
  privateTours: boolean;
};
type ISearchTravelsProps = {};

//# Component => search travels
export const SearchTravels = ({}: ISearchTravelsProps): JSX.Element => {
  //# Hooks
  const { control, handleSubmit } = useForm<IFormData>({
    mode: "all",
    defaultValues: {
      whereGoing: "",
      whenGo: "",
      groupTours: false,
      privateTours: false,
    },
  });
  //# Methods
  const onSubmit = async (data: IFormData): Promise<void> => {
    console.log(data);
  };

  return (
    <C.Container>
      <C.Searching>
        <WhereGoing
          rules={{ required: true }}
          control={control}
          name="whereGoing"
        />
        <WhenGo rules={{ required: true }} control={control} name="whenGo" />
        <C.Search onClick={handleSubmit(onSubmit)}>Search</C.Search>
      </C.Searching>
      <C.Filtering>
        <C.FilterText>Filter by:</C.FilterText>
        <CheckControl title="Group tours" control={control} name="groupTours" />
        <CheckControl
          title="Private tours"
          control={control}
          name="privateTours"
        />
      </C.Filtering>
    </C.Container>
  );
};

//# Types props
type ICheckProps = {
  title?: string;
};
type ICheckControlProps = ICheckProps & UseControllerProps<IFormData>;
//# component => filter check
const CheckControl = (props: ICheckControlProps): JSX.Element => {
  //# Destroy
  const { title } = props;
  //# Hooks
  const { field } = useController(props);
  //# States
  const [active, setActive] = useState<boolean>(false);
  //# Methods
  const onCheck = (): void => {
    field.onChange(!active);
    setActive(!active);
  };

  return (
    <C.CheckControl onClick={onCheck}>
      <C.Check active={active}>{active && <GoCheck />}</C.Check>
      <C.Title>{title}</C.Title>
    </C.CheckControl>
  );
};

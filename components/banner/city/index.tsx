//# React lazy load
import { useTranslation } from "react-i18next";
import { LazyLoadImage } from "react-lazy-load-image-component";

//# Styles components
import * as C from "../styled";

//# Types proops
type IBannerProps = {
  title?: string;
  slogan?: string;
  img?: string;
};

//# Component => banner trip
export const Banner = ({ title, img }: IBannerProps) => {

  const {t} = useTranslation("common")
  return (
    <C.Container>
      <C.Photo>
        <LazyLoadImage
          effect="black-and-white"
          width="100%"
          height="100%"
          src={img}
          title={title}
          alt={title}
        />
      </C.Photo>
      <C.Content background>
        <C.Center>
          <C.Title> {t("titles.city")} {title}</C.Title>
        </C.Center>
      </C.Content>
    </C.Container>
  );
};

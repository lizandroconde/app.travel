//# Styled components
import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  z-index: 2;
  width: 100%;
  height: ${({ theme: { toRem } }) => toRem(288)};
  margin-bottom: ${({ theme: { toRem } }) => toRem(50)};
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    margin-bottom: ${({ theme: { toRem } }) => toRem(35)};
    height: ${({ theme: { toRem } }) => toRem(392)};
  }
`;
export const Photo = styled.div`
  position: absolute;
  z-index: 0;
  display: block;
  width: 100%;
  height: 100%;
`;
export const Content = styled.div<{background?:boolean}>`
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;
  background: ${({background,theme})=>background? "linear-gradient( rgba(0, 0, 0, .3), rgba(0, 0, 0, 0.3))" : theme.colors.black.rgba.BK000(0.2)} ;
`;
export const Center = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  row-gap: ${({ theme: { toRem } }) => toRem(20)};
  width: 100%;
  max-width: ${({ theme: { toRem } }) => toRem(1440)};
  height: 100%;
  margin-right: auto;
  margin-left: auto;
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(32)};
    padding-right: ${({ theme: { toRem } }) => toRem(32)};
    padding-bottom: ${({ theme: { toRem } }) => toRem(32)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(32)};
    padding-right: ${({ theme: { toRem } }) => toRem(32)};
    padding-bottom: ${({ theme: { toRem } }) => toRem(60)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(28)};
    padding-right: ${({ theme: { toRem } }) => toRem(28)};
  }
`;
export const Title = styled.h1`
  position: relative;
  width: 100%;
  min-height: ${({ theme: { toRem } }) => toRem(1)};
  display: block;
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(26)};
  line-height: 120%;
  font-weight: 700;
  text-shadow: ${({ theme: { toRem, colors } }) =>
    `0 ${toRem(1)} ${toRem(1)} ${colors.black.rgba.BK000(0.3)}`};
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    font-size: ${({ theme: { toRem } }) => toRem(48)};
    letter-spacing: ${({ theme: { toRem } }) => toRem(-2)};
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    text-align: center;
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
`;
export const Slogan = styled.p`
  position: relative;
  width: 100%;
  min-height: ${({ theme: { toRem } }) => toRem(1)};
  color: ${({ theme: { colors } }) => colors.white.rgb.WFFF};
  font-size: ${({ theme: { toRem } }) => toRem(18)};
  line-height: 120%;
  font-weight: normal;
  letter-spacing: ${({ theme: { toRem } }) => toRem(-0.2)};
  text-shadow: ${({ theme: { toRem, colors } }) =>
    `0 ${toRem(1)} ${toRem(1)} ${colors.black.rgba.BK000(0.3)}`};
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    text-align: center;
    padding-left: ${({ theme: { toRem } }) => toRem(8)};
    padding-right: ${({ theme: { toRem } }) => toRem(8)};
  }
`;
export const Searching = styled.div`
  position: relative;
  z-index: 3;
  display: flex;
  width: 100%;
  max-width: ${({ theme: { toRem } }) => toRem(1440)};
  margin-right: auto;
  margin-left: auto;
  padding-top: ${({ theme: { toRem } }) => toRem(30)};
  padding-left: ${({ theme: { toRem } }) => toRem(16)};
  padding-right: ${({ theme: { toRem } }) => toRem(16)};
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    justify-content: center;
    padding-left: ${({ theme: { toRem } }) => toRem(32)};
    padding-right: ${({ theme: { toRem } }) => toRem(32)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    padding-top: ${({ theme: { toRem } }) => toRem(8)};
    padding-left: ${({ theme: { toRem } }) => toRem(32)};
    padding-right: ${({ theme: { toRem } }) => toRem(32)};
  }
  ${({ theme: { media } }) => media.to(0, 568, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(28)};
    padding-right: ${({ theme: { toRem } }) => toRem(28)};
  }
`;

import styled from "styled-components";

export const Layout = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-gap: 1rem;
  margin-top: 1rem;
  margin-bottom: 3rem;
`;

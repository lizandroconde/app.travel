import { Trip } from "@components/card/trip";
import { ITrip } from "@interfaces/trip";
import * as Theme from "./styled";

interface CityTrips {
  trips: ITrip[];
}

const TripCity = ({ trips }: CityTrips) => {
  return (
    <Theme.Layout>
      {trips.map((trip: ITrip, index: number) => {
        return <Trip trip={trip} key={index} />;
      })}
    </Theme.Layout>
  );
};

export default TripCity;

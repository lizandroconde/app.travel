//# Media query of responsive
const media = (
  maxWidth: number,
  minWidth?: number,
  min?: boolean,
  all?: boolean
) => {
  if (min) {
    return `@media screen and (min-width: ${minWidth}px)`;
  } else if (all) {
    return `@media screen and (max-width: ${maxWidth}px) and (min-width: ${minWidth}px)`;
  } else {
    return `@media screen and (max-width: ${maxWidth}px)`;
  }
};

//# Export the options Media querys
export const MediaQuery = {
  to: (
    maxWidth: number = 0,
    minWidth: number = 0,
    min: boolean = false,
    all: boolean = false
  ) => {
    return media(maxWidth, minWidth, min, all);
  },
};

//# Export theme type => theme private
export type ThemeType = typeof ThemePrivate;
//# Import the all colors
import * as Colors from "./colors";
//# Media
import { MediaQuery } from "./media";
//# Import the all box shadows
export const ThemePrivate = {
  media: MediaQuery,
  colors: Colors,
  toRem: (px: number): string => {
    const REM: number = 1;
    const PX: number = 16;
    const rem: string = ((px * REM) / PX).toFixed(3);
    return `${Number(rem)}rem`;
  },
  spaces: (number:number): string => {
    const value = number * 8 + 'px';
    return value;
  },
  breakpoints: {
    sm: '@media (max-width: 600px)',
    md: '@media (min-width: 601px)',
    ll: '@media (min-width: 905px)',
    lg: '@media (min-width: 1240px)',
  },
};

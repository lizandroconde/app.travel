//# Color => black
export const black = {
  rgb: {
    BL000: "#000",
    BLk2C3E50: "#2C3E50",
    BLK555: "#555",
    BLK4A5A5A: "#4A5A5A",
  },
  rgba: {
    BK000: (deg: number) => `rgba(0,0,0,${deg})`,
  },
};
//# Color => white
export const white = {
  rgb: {
    WFFF: "#FFF",
  },
  rgba: {
    W255: (deg: number) => `rgb(255, 255, 255, ${deg})`,
  },
};
//# Color => orange
export const orange = {
  rgb: {
    OGFF946D: "#FF946D",
    OF28023: "#F28023",
  },
};
//# Color => green
export const green = {
  rgb: {
    GR72A842: "#72A842",
  },
};
//# Color => gray
export const gray = {
  rgb: {
    GRYEBEEF2: "#EBEEF2",
    GRYC7D0D9: "#C7D0D9",
    GRYDDD: "#DDD",
    GRYF2F3F5: "#F2F3F5",
    GRYCCC: "#CCC",
    GRY151743: "rgb(115,117,143)",
    GRYE0E0E0: "#E0E0E0",
    GRY9facba: "#9facba",
    GRYe4e8ec: "#e4e8ec",
    GRYf5f6f9: "#f5f6f9",
    GRY81829c: "#81829c",
    GRY9aa7b5: "#9aa7b5",
    GRYa6b2bf: "#a6b2bf",
    GR23501: "rgba(235, 235, 235, 1)",
  },
};
//# Color => blue
export const blue = {
  rgb: {
    BL286283: "#286283",
    BL4775250: "rgb(47,75,250)",
    BL35aafa: "#35aafa",
    BL3b556a: "#3b556a",
  },
};
//# Color => yellow
export const yellow = {
  rgb: {
    YWffe958: "#ffe958",
  },
};

//# Global Styled
import styled, { createGlobalStyle } from "styled-components";
export const GlobalStyle = createGlobalStyle<{}>`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: none;
    border: none;
    appearance: none;
    font-family: 'Roboto Slab', serif;
    object-fit: cover;
  }
  body{
    font-family: 'Roboto Slab', serif;
    color: ${({ theme: { colors } }) => colors.black.rgb.BLk2C3E50};
    font-size: ${({ theme: { toRem } }) => toRem(14)};
    line-height: 1.5;
    background: ${({ theme: { colors } }) => colors.gray.rgb.GRYEBEEF2};
  }
`;
export const Center = styled.div`
  ${({ theme: { media } }) => media.to(0, 1184, true)} {
    width: calc(
      ${({ theme: { toRem } }) => `${toRem(1136)} + 2 * ${toRem(24)}`}
    );
    padding-left: ${({ theme: { toRem } }) => toRem(24)};
    padding-right: ${({ theme: { toRem } }) => toRem(24)};
    margin-left: auto;
    margin-right: auto;
  }
  ${({ theme: { media } }) => media.to(0, 1024, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(24)};
    padding-right: ${({ theme: { toRem } }) => toRem(24)};
  }
  ${({ theme: { media } }) => media.to(0, 768, true)} {
    padding-left: ${({ theme: { toRem } }) => toRem(24)};
    padding-right: ${({ theme: { toRem } }) => toRem(24)};
  }
  ${({ theme: { media } }) => media.to(767)} {
    padding-left: ${({ theme: { toRem } }) => toRem(24)};
    padding-right: ${({ theme: { toRem } }) => toRem(24)};
  }
`;


export const SocialIcons = styled.div`
  position: fixed;
  position: fixed;
    cursor: pointer;
    overflow: hidden;
    z-index: 99;
    width: 65px;
    border-radius: 50%;
    height: 65px;
    bottom: 12px;
    left: 17px;
    background: #ddd7d7;
`
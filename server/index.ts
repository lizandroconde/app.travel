// Next
import next from "next";
import express from "express";
import { routes } from "./routes";
import { HTTPHandler } from "next-routes";
import { NextServer } from "next/dist/server/next";

// Asign the routes to nextJS
const port: number = parseInt(process.env.PORT!) || 8700;
const dev: boolean = process.env.NODE_ENV !== "production";
const app: NextServer = next({ dev:false });
const handler: HTTPHandler = routes.getRequestHandler(app);
 
const server = express();

// Init server with express
(async (): Promise<void> => {
  try {
    await app.prepare();
    server.use(express.static('public'));
    server.use(handler);
    server.listen(port, (err?: any): void => {
      
      if (err) throw err;
      console.log(`🚀 Server is running on: http://localhost:${port}`);
    })
      
      
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();

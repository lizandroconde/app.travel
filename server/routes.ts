// Next routes
import Routes from "next-routes";
// Routes defs
export const routes = new Routes();
export const Router = routes.Router;
export const Link = routes.Link;

// Routes from pages dinamic
routes
  .add("Home", "/", "home")
  .add("Homelang", "/:locale", "home")
  .add("Stylelang", "/:locale/e/:type", "style")
  .add("Attractionlang", "/:locale/a/:type", "attraction")
  .add("Citylang", "/:locale/:country/:city", "city")
  .add("Trip", "/:locale/:country/:city/:trip", "trip")
  .add("404", "/*", "_error");

/** @type {import('next').NextConfig} */
const dotenv = require("dotenv");

dotenv.config();

module.exports = {
  reactStrictMode: true,
  env: {
    ENPOINT: process.env.ENPOINT,
  },
  images: {
    domains: ["flagcdn.com", "www.conde.travel", "images.conde.travel"],
  },
  compress: true,
  async rewrites() {
    return [
      {
        source: "/",
        destination: "/home",
      },
      {
        source: "/:locale",
        destination: "/home",
      },
      {
        source: "/:locale/e/:type",
        destination: "/style",
      },
      {
        source: "/:locale/a/:type",
        destination: "/attraction",
      },
      {
        source: "/:locale/:country/:city",
        destination: "/city",
      },
      {
        source: "/:locale/:country/:city/:trip",
        destination: "/trip",
      },
      
    ];
  },
};


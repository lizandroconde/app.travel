// Next
import type { NextPage } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { initializeApollo } from "@apollo";
import { GET_TRIP_SLUG } from "@gql/trip/query";

import { ITripPage } from "@interfaces/trip";
// Components
import { BannerTrip } from "@components/banner/trip";

import { Content } from "@components/trip";
// Global
import { Center } from "@themes/global";
import Layout from "@components/layout";
import { HeaderContext, TripContext } from "@hooks/useContext";
import { ILangueages } from "@interfaces/langueages";
import { GET_HREFLA_TRIP } from "@gql/hrflag/query";
import Error from "next/error";

import { NextSeo,ProductJsonLd } from 'next-seo';
import { ValidateNormalDiscount } from "@hooks/validate/index.";
 

//# Types props
type ITripProps = {
  info: ITripPage;
  languages: ILangueages[];
  fallback: boolean;
};

interface IHreflang {
  hrefLang: string;
  href: string;
}

//# Page => Trip
const Trip: NextPage<ITripProps> = ({ info, languages,fallback }): JSX.Element => {
  if(fallback) return <Error statusCode={404}/>
  const { Seo,Gallery, Price ,Valoration} = info;
    
  const productgallery =[]

  for(let img of Gallery) {
    productgallery.push(img?.url)
  }

  const alternatives:IHreflang[] = [];
  for(let language of languages) {
    alternatives.push({
      hrefLang: language?.slug,
      href: language?.url
      
    })
  }
  
  return (
    <HeaderContext.Provider value={{ languages }}>
      <Layout>
          <NextSeo
          nofollow={true}
          languageAlternates={alternatives}
          title={Seo?.title + " - Conde Travel"}
          description={Seo?.description} 
          canonical={Seo?.canonical}
          openGraph={{
            url: Seo?.description,
            title: Seo?.title,
            description: Seo?.description,
            images: [
              { url:  Seo?.image, },
            ],
            site_name: 'Conde Travel',
          }}
          twitter={{
            handle: '@handle',
            site: '@site',
            cardType: 'summary_large_image',
          }}
        />
        
        <TripContext.Provider value={{ info }}>
           
            <Center>
              <BannerTrip />
            </Center>
            <Content />
           
        </TripContext.Provider>
        <ProductJsonLd
      productName={Seo?.title}
      images={productgallery}
      description={Seo?.description}
      brand="Conde Travel"
      color="orange"
      manufacturerName="Luis Conde"
      manufacturerLogo="https://images.conde.travel/logo.jpeg"
      releaseDate="2022-02-05T08:00:00+08:00"
      productionDate="2022-02-05T08:00:00+08:00"
      purchaseDate="2022-02-06T08:00:00+08:00"
      
      reviews={[
        {
          author: 'Jim',
          datePublished: '2022-01-06T03:37:40Z',
          reviewBody:
            'This is my favorite product yet! Thanks Nate for the example products and reviews.',
          name: 'So awesome!!!',
          reviewRating: {
            bestRating: '5',
            ratingValue: '5',
            worstRating: '1',
          },
          publisher: {
            type: 'Organization',
            name: 'TwoVit',
          },
        },
      ]}
      aggregateRating={{
        ratingValue: Valoration.Ratio,
        reviewCount: Valoration.Cant,
      }}
      offers={[
        {
          price: Price?.Discount > 0? ValidateNormalDiscount({discount: Price?.Discount,prices: Price?.Amount}):Price?.Amount ,
          priceCurrency: 'USD',
          priceValidUntil: '2022-11-05',
          itemCondition: 'https://schema.org/UsedCondition',
          availability: 'https://schema.org/InStock',
          url: Seo?.canonical,
          seller: {
            name: 'Ruth Mery Cusihuaman',
          },
        }
      ]}
       
    />
      </Layout>
    </HeaderContext.Provider>
  );
};

export const getServerSideProps = async ({
  query: { locale, country, city, trip },
}: any) => {
  const client = initializeApollo();
  const info:ITripPage= await(await client.query({
    query: GET_TRIP_SLUG,
    variables: {
      Slug: locale,
      Country: country,
      City: city,
      Trip: trip,
    },
  })).data?.getTripSlug?.data;

  if(info ===null){
    return{
      props: {
        fallback: true,
      }
    }
  }
 
  const languages = await (
    await client.query({
      query: GET_HREFLA_TRIP,
      variables: { Slug: locale, Country: country, City: city, Trip: trip },
    })
  ).data?.gethref?.data;

  return {
    props: {
      ...(await serverSideTranslations(locale, ["trip", "common"])),
      info,
      languages,
    },
  };
};

export default Trip;

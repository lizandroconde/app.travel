import Head from "next/head";

function Error({ statusCode }:{statusCode:number}) {
  return (
    <>
      <Head>
        <title>AH ! sorry</title>
        <meta name="robots" content="noindex, nofollow" />
        <link rel="stylesheet" href="/styles/error.css" />
      </Head>
      <div   className="background">
	<div className="ground"></div>
</div>
<div className="containers">
	<div className="left-section">
		<div className="inner-content">
			<h1 className="heading">  {statusCode}</h1>
			<p className="subheading">Looks like the page you were looking for is no longer here.</p>
      <p  style={{background:"white",display:"inline-block",padding:"10px",cursor:"pointer",borderRadius:"5px",margin:"auto"}}><a>Back to Home</a></p>
		</div>
	</div>
	 
</div>
 </>   
  );
}

Error.getInitialProps = ({ res, err }:any) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;

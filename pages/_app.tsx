//# Next App
import type { AppProps } from "next/app";
import Image from "next/image"
import{ Router} from "@routes"
//# Styled components
import { ThemeProvider } from "styled-components";
//# Global styled
import { GlobalStyle, SocialIcons } from "@themes/global";
import { ThemePrivate } from "@themes/index";
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress
//# Google fonts
import { GoogleFonts } from "next-google-fonts";
import { appWithTranslation } from "next-i18next";
//# Apollo
import {
  ApolloClient,
  NormalizedCacheObject,
  ApolloProvider,
} from "@apollo/client";
import { useApollo } from "@apollo";
//# Styles lazy load
import "react-lazy-load-image-component/src/effects/blur.css";
import SimpleReactLightbox from "simple-react-lightbox"; 
Router.events.on('routeChangeStart', () => NProgress.start()); Router.events.on('routeChangeComplete', () => NProgress.done()); Router.events.on('routeChangeError', () => NProgress.done());

//# Page => App
const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  //# Apollo client
  const apolloClient: ApolloClient<NormalizedCacheObject> =
    useApollo(pageProps);

  return (
    <ApolloProvider client={apolloClient}>
      <SimpleReactLightbox>
      <ThemeProvider theme={ThemePrivate}>
        <GlobalStyle />

        <GoogleFonts href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700;800;900&display=swap" />
        <Component {...pageProps} />
      </ThemeProvider>
      </SimpleReactLightbox>
      <SocialIcons >
          <a href="https://api.whatsapp.com/send?1=pt_BR&phone=51984603305&text=Hi!%20Conde%20Travel" target="_blank" rel="noreferrer">
            <Image src="https://images.conde.travel/assets/whatsapp.png" alt="whatsapp" width="100%" height="100%" />
          </a>
      </SocialIcons>
      <div style={{maxHeight:"0px",overflow: "hidden"}}><script src="https://cdn.wetravel.com/widgets/embed_calendar.js"  data-env="https://www.wetravel.com"  async  /></div>
      <script
            dangerouslySetInnerHTML={{
              __html: ` window.__lc = window.__lc || {};
              window.__lc.license = 13051974;
              ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))`,
            }}></script>
  
         

    </ApolloProvider>
  );
};
export default appWithTranslation(MyApp);

// Next
import type { NextPage } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";
// Apollo client
import { initializeApollo } from "@apollo";
import { GET_TRIPS_CITY } from "@gql/trip/query";
// Interfaces
import { ITrip } from "@interfaces/trip";
// Components
 
// Global
import { Center } from "@themes/global";
import Layout from "@components/layout";
import { HeaderContext } from "@hooks/useContext";
 
import { GET_CITY, GET_HREFLA_CITY } from "@gql/city/query";
import { Banner } from "@components/banner/city";
import TripCity from "@components/organisms/Trips-City";
import Error from "next/error";
import { ILangueages } from "@interfaces/langueages";

//# Types props
type ITripProps = {
  info: {
    Title: string;
    Name: string;
    Description: string;
    Image: {
      name: string;
      url: string;
      alt: string;
    };
  };
  activities: ITrip[];
  fallback: boolean;
  languages: ILangueages[];
};

//# Page => Trip
const Trip: NextPage<ITripProps> = ({ info, activities,fallback,languages }): JSX.Element => {
  if(fallback) return <Error statusCode={404}/>
  return (
    <HeaderContext.Provider value={{languages}}>
      <Layout>
        <Head>
          <title>{info?.Title} - Conde Travel</title>
          <meta name="description" content={info?.Description} />
        </Head>

        <Banner title={info?.Name} img={info?.Image?.url} />
        <main>
          <Center>
            <TripCity trips={activities} />
          </Center>
        </main>
      </Layout>
    </HeaderContext.Provider>
  );
};

export const getServerSideProps = async ({
  query: { locale, country, city },
}: any) => {
  const client = initializeApollo();

  const info = await (
    await client.query({
      query: GET_CITY,
      variables: { slug: locale, country, city },
    })
  ).data?.getSlugCity?.data;

  if(info ===null){
    return{
      props: {
        fallback: true,
      }
    }
  }

  const activities = await (
    await client.query({
      query: GET_TRIPS_CITY,
      variables: { slug: locale, country, city },
    })
  ).data?.getTripsCity?.data;

  const languages = await (
    await client.query({
      query: GET_HREFLA_CITY,
      variables: { slug: locale, country, city },
    })
  ).data?.getCityHreflag?.data;
  
  return {
    props: {
      ...(await serverSideTranslations(locale, ["trip", "common"])),
      info,
      activities,
      languages
    },
  };
};

export default Trip;

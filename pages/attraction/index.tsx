// Next
import type { NextPage } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";
// Apollo client
import { initializeApollo } from "@apollo";

// Interfaces
import { ITrip } from "@interfaces/trip";
 
// Global
import { Center } from "@themes/global";
import Layout from "@components/layout";
import { HeaderContext } from "@hooks/useContext";
 
 
import { Banner } from "@components/banner/city";
import TripCity from "@components/organisms/Trips-City";
import { GET_ALL_TRIP_ATTRACTION, GET_HREFLA_ATTRACTION, GET_ONE_ATTRACTION } from "@gql/attraction/query";
import Error from "next/error";
import { ILangueages } from "@interfaces/langueages";

//# Types props
type ITripProps = {
  info: {
    Title: string;
    Name: string;
    Description: string;
    Image: {
      name: string;
      url: string;
      alt: string;
    };
  };
  activities: ITrip[];
  fallback: boolean;
  languages: ILangueages[];
};

//# Page => Trip
const Trip: NextPage<ITripProps> = ({ info, activities,fallback ,languages}): JSX.Element => {
  if(fallback) return <Error statusCode={404}/>
  return (
    <HeaderContext.Provider value={{languages}}>
      <Layout>
        <Head>
          <title>{info?.Title}</title>
          <meta name="description" content={info?.Description} />
        </Head>

        <Banner title={info?.Name} img={info?.Image?.url} />
        <main>
          <Center>
            <TripCity trips={activities} />
          </Center>
        </main>
      </Layout>
    </HeaderContext.Provider>
  );
};

export const getServerSideProps = async ({
  query: { locale, type },
}: any) => {
  const client = initializeApollo();

  const info = await (
    await client.query({
      query: GET_ONE_ATTRACTION,
      variables: { slug: locale, type },
    })
  ).data?.getOneAttraction?.data;

  if(info ===null){
    return{
      props: {
        fallback: true,
      }
    }
  }

  const activities = await (
    await client.query({
      query: GET_ALL_TRIP_ATTRACTION,
      variables: { slug: locale, type},
    })
  ).data?.getTripsAttraction?.data;

  const languages = await (
    await client.query({
      query: GET_HREFLA_ATTRACTION,
      variables: { slug: locale, place: type},
    })
  ).data?.getPlaceHrefla?.data;

 
  return {
    props: {
      ...(await serverSideTranslations(locale, [ "common"])),
      info,
      activities,
      languages
    },
  };
};

export default Trip;

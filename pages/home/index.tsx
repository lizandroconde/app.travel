//# Next
import type { NextPage } from "next";
import Head from "next/head";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { HeaderContext } from "@hooks/useContext";
import { Banner } from "@components/banner";
import { TopTrips } from "@components/top-trips";
import { News } from "@components/news";

import { TopAttractions } from "@components/top-attractions";
//# Global
import { Center } from "@themes/global";
import { TopCities } from "@components/top-destinations";
import { TopStyles } from "@components/top-styles";
import { initializeApollo } from "@apollo";
//# Gql -
import { GET_CITIES_HOME } from "@gql/city/query";
import { GET_TRIPS_HOME } from "@gql/trip/query";
//# hooks
import { HomeContext } from "@hooks/useContext";
import Error from "next/error"
import { ICityHome } from "@interfaces/city";
import { ITrip } from "@interfaces/trip";
import { IStyle } from "@interfaces/travel-styles/index";
import { IAttractionsHome } from "@interfaces/attraction";
import { GET_STYLES_HOME } from "@gql/styles/query";
import { GET_ATTRACTIONS_HOME } from "@gql/attraction/query";
import { GET_HOME } from "@gql/global/querys";
import Layout from "@components/layout";
import { GET_HREFLA_HOME } from "@gql/hrflag/query";
import { ILangueages } from "@interfaces/langueages";
import { Highlighted } from "@components/molecules";

interface HomeProps {
  Slug: string;
  TitleGoogle: string;
  DecriptionGoogle: string;
  Title: string;
  Image: string;
  Slogan: string;
}

//# Types props
type IHomeProps = {
  cities: ICityHome[];
  trips: ITrip[];
  styles: IStyle[];
  atractions: IAttractionsHome[];
  home: HomeProps;
  languages: ILangueages[];
  fallback: boolean;
};

//# Page => home
const Home: NextPage<IHomeProps> = ({
  cities,
  trips,
  styles,
  atractions,
  home,
  languages,
  fallback
}): JSX.Element => {
  if(fallback) return <Error statusCode={404}/>
  const { Slug, Title, Image, DecriptionGoogle, TitleGoogle } = home;
  return (
    <HomeContext.Provider value={{ cities, trips, styles, atractions }}>
      <Head>
        <title>{TitleGoogle} - Conde Travel</title>
        <meta name="description" content={DecriptionGoogle} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HeaderContext.Provider value={{ languages }}>
        <Layout>
          <Banner title={Title} slogan={Slug} img={Image} />

          <Center>
            <Highlighted />
            <TopCities />
            <TopTrips />
          </Center>
          <TopAttractions />
          <TopStyles />

          {/*<News />*/}
        </Layout>
      </HeaderContext.Provider>
    </HomeContext.Provider>
  );
};

export const getServerSideProps = async ({ query }: any) => {
  const locale = query?.locale || "en";

  const client = initializeApollo();
  const home = await (
    await client.query({ query: GET_HOME, variables: { slug: locale } })
  ).data?.getLanguageHome?.data;
  if(home ===null){
    return{
      props: {
        fallback: true,
      }
    }
  }
  const cities = await (
    await client.query({ query: GET_CITIES_HOME, variables: { slug: locale } })
  ).data?.getCitieshome?.data;
  const trips = await (
    await client.query({ query: GET_TRIPS_HOME, variables: { slug: locale } })
  ).data?.getTripshome?.data;
  const styles = await (
    await client.query({ query: GET_STYLES_HOME, variables: { slug: locale } })
  ).data?.getStylesHome?.data;
  const atractions = await (
    await client.query({
      query: GET_ATTRACTIONS_HOME,
      variables: { slug: locale },
    })
  ).data?.getAttactionshome?.data;
  const languages = await (
    await client.query({ query: GET_HREFLA_HOME, variables: { slug: locale } })
  ).data?.gethrefhome?.data;

  return {
    props: {
      ...(await serverSideTranslations(locale, ["home", "common"])),
      cities,
      trips,
      styles,
      atractions,
      home,
      languages,
    },
  };
};

export default Home;

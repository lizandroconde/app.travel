//## Dependencys
import { ReactNode, ReactElement } from "react";
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
  DocumentInitialProps,
} from "next/document";
//# Server styled component
import { ServerStyleSheet } from "styled-components";
//# Types
type IProps = {
  lang: string;
};

//# Configuration from SSR of module styled components.
export default class MyDocument extends Document<IProps> {
  static async getInitialProps(
    ctx: DocumentContext
  ): Promise<DocumentInitialProps | any> {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;
    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp:
            (App) =>
            (props): ReactElement =>
              sheet.collectStyles((<App {...props} />) as ReactNode),
        });
      const initialProps: DocumentInitialProps = await Document.getInitialProps(
        ctx
      );
      return {
        ...initialProps,

        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
        lang: ctx?.query?.locale,
      };
    } finally {
      sheet.seal();
    }
  }
  render(): JSX.Element {
    const lang = this.props?.lang;
    return (
      <Html lang={lang || "en"}>
        <Head >
        <meta name="theme-color" content="#091821"/>
        </Head>
        <body>
          <Main />
          <NextScript   />
        </body>
      </Html>
    );
  }
}

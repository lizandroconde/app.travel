//# Interfaces => country
import { ICity } from "@interfaces/city";
export type ICountry = {
  Name: string;
  Url: string;
  Citys: ICity[];
};

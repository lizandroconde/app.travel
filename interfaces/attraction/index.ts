

export type IAttractionsHome = {
    Name: string;
    Url: string;
    Trips: number;
    Image: string;

};

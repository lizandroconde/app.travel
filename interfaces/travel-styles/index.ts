//# Interfaces => country
export type ITravelStyles = {
  Name: string;
  Url: string;
  Image: String;
};

export type IStyle = {
  Name: String;
  Url: String;
  Image: String;
  Trips: Number;
};

//# Interfaces => city
export type ICity = {
  Name: string;
  Url: string;
};


export type ICityHome = {
  Name: string;
  Url: string;
  Trips: number;
  Image: string;

};

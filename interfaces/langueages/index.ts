export interface ILangueages{
    cod: string;
    url: string;
    slug: string;
}
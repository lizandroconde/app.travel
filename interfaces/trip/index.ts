//# Interfaces => trip
export type ITrip = {
  _ID: string;
  Badge: IBadge;
  Image: string;
  Location: string;
  Name: string;
  Valoration: IValoration;
  Duration: IDuration;
  Price: number;
  Discount: number;
  Url: string;
};
export type IBadge = {
  Text: string;
  Color: string;
};
export type IValoration = {
  Cant: number;
  Ratio: number;
};
export type IDuration = {
  Type: string;
  Count: number;
};

interface IImagePlace {
  name: string;
  alt: string;
  url: string;
}

export interface IPlace {
  name: string;
  image: IImagePlace;
}

export interface IPrice {
  Amount: number;
  Discount: number;
}

// Trip page
export type ITripPage = {
  Details: IDetails;
  Faq: IFaq[];
  Gallery: IGallery[];
  Include: IErrorInclude;
  Intro: IIntro;
  Itinerary: IItinerary[];
  Location: string;
  Seo: ISeo;
  Title: string;
  Places: IPlace[];
  Aditional: string;
  Valoration: IValoration;
  CodWetravel: number;
  Price: IPrice
};
export type IDetails = {
  duration: IDuration;
  max: number;
  min: number;
  location: string;
  languages: string;
};
export type IFaq = {
  ask: string;
  response: string;
};
export type IGallery = {
  alt: string;
  name: string;
  url: string;
};
export type IErrorInclude = {
  Include: IInclude[];
  NoInclude: string;
  Recommendations: IInclude[];

};
export type IInclude = {
  Title: string;
  Content: string;
  Icon: string;
};
export type IIntro = {
  highlights: string;
  introduction: string;
};
export type IItinerary = {
  day: string;
  title: string;
  gallery: IGalleryItinerary[];
  content: string;
};
export type IGalleryItinerary = {
  name: string;
  url: string;
  alt: string;
};
export type ISeo = {
  canonical: string;
  description: string;
  image: string;
  title: string;
};

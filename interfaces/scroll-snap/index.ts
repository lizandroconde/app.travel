//# Interface => scroll snap
export type IResponsive = {
  media: number;
  space: number;
  firstMargin: number;
  size: number;
};
export type IMediaQuery = {
  space: number;
  firstMargin: number;
  size: number;
};

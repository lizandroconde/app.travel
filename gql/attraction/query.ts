//# Graphql => global
import { gql } from "@apollo/client";

export const GET_ATTRACTIONS_HOME = gql`
  query getAttactionshome($slug: String!) {
    getAttactionshome(Slug:$slug){   
    data{
        Name
        Image
        Url
        Trips
   }
   }
  }
`;


export const GET_ONE_ATTRACTION = gql`
  query getAttraction($slug: String, $type: String) {
    getOneAttraction(Slug:$slug, Type:$type){
      status
      data {
        Title
        Name
        Description
        Canonical
        Image {
          name
          url
          alt
        }
      }
    }
  }
`

export const GET_ALL_TRIP_ATTRACTION = gql`
  query getAllTrip($slug: String, $type: String){
    getTripsAttraction(Slug:$slug,Type:$type){
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`

export const GET_HREFLA_ATTRACTION = gql`
  query getPlaceHrefla($slug:String,$place:String){
    getPlaceHrefla(Slug:$slug,Place:$place){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`
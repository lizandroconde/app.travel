//# Graphql => global
import { gql } from "@apollo/client";

export const GET_CITIES_HOME = gql`
  query getCitieshomeD($slug: String!) {
    getCitieshome(Slug:$slug){   
    data{
     Name
     Url
     Trips
     Image
   }
   }
  }
`;


export const GET_CITY = gql`
  query getCity($slug:String,$country:String,$city:String){
    getSlugCity(Slug:$slug,Country:$country,City:$city){
      status
      data{
        Title
        Name
        Description
        Image{
          name
          url
          alt
        }
      }
    }
  }
`
export const GET_HREFLA_CITY = gql`
  query getCityHreflag($slug:String,$country:String,$city:String){
    getCityHreflag(Slug:$slug,Country:$country,City:$city){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`
//# Graphql => global
import { gql } from "@apollo/client";

export const GET_TRIPS_HOME = gql`
  query getTripshome($slug: String!) {
    getTripshome(Slug: $slug) {
      status
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;


export const GET_TRIPS_CITY = gql`
  query getTripsCity($slug: String,$country: String,$city: String) {
    getTripsCity(Slug: $slug,Country:$country,City:$city) {
      status
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;

export const GET_TRIP_SLUG = gql`
  query getTripSlug(
    $Slug: String!
    $Country: String!
    $City: String!
    $Trip: String!
  ) {
    getTripSlug(Slug: $Slug, Country: $Country, City: $City, Trip: $Trip) {
      data {
        Details {
          duration {
            Count
            Type
          }
          max
          min
          location
          languages
        }
        Seo {
          title
          description
          canonical
          image
        }
        Places{
          name
          image {
            name
            alt
            url
          }
        }
        Location
        Title
        Valoration {
          Cant
          Ratio
        }
        Gallery {
          name
          alt
          url
        }
        Intro {
          introduction
          highlights
        }
        Itinerary {
          day
          title
          content
          gallery {
            name
            url
          }
        }
        Include {
          Include {
            Icon
            Content
            Title
          }
          NoInclude
          Recommendations{
            Icon
            Title
            Content
          }
        }
        Faq {
          ask
          response
        }
        Aditional
        CodWetravel
        Price{
          Amount
          Discount
        }
      }
    }
  }
`;

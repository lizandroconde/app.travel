
//# Graphql => global
import { gql } from "@apollo/client";

export const GET_STYLES_HOME = gql`
  query getStylesHome($slug: String!) {
    getStylesHome(Slug:$slug){   
    data{
        Name
        Image
        Url
        Trips
   }
   }
  }
`;


export const GET_ONE_STYLE = gql`
  query getStyle($slug: String, $type: String) {
    getOneStyle(Slug:$slug, Type:$type){
      status
      data {
        Title
        Name
        Description
        Canonical
        Image {
          name
          url
          alt
        }
      }
    }
  }
`

export const GET_ALL_TRIP_STYLE = gql`
  query getAllTrip($slug: String, $type: String){
    getTripsStyle(Slug:$slug,Type:$type){
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`


export const GET_HREFLA_STYLE = gql`
  query getStyleHrefla($slug: String, $type: String){
    getStyleHrefla(Slug:$slug,Type:$type){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`
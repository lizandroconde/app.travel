import { gql } from "@apollo/client";

export const NEW_NEWLETTER = gql`
mutation newNewsletter($Email: String, $Name: String, $Language: String){
    newNewsletter(Email:$Email,Name:$Name,Language:$Language){
      message
    }
  }
`;


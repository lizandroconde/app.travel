
//# Graphql => global
import { gql } from "@apollo/client";

export const GET_HREFLA_HOME = gql`
  query getStylesHome($slug: String) {
    gethrefhome(Slug:$slug){
        status
        data{
          slug
          cod
          url
        }
      }
  }
`;


export const GET_HREFLA_TRIP = gql`
  query GETHREFLKA($Slug: String!
    $Country: String!
    $City: String!
    $Trip: String!) {
        gethref(Slug: $Slug, Country: $Country, City: $City, Trip: $Trip){
        status
        data{
          slug
          cod
          url
        }
      }
  }
`;


//# Graphql => global
import { gql } from "@apollo/client";
export const GET_DESTINATIONS = gql`
  query getDestinations($slug: String){
    getDestinations(Slug:$slug) {
      status
      data {
        Name
        Url
        Citys {
          Name
          Url
        }
      }
    }
  }
`;
export const GET_STYLES_SLUG = gql`
  query getStylesSlug($slug:String){
    getStylesSlug(Slug: $slug) {
      status
      data {
        Name
        Url
      }
    }
  }
`;

export const GET_ATTRACTTION_HEADER = gql`
  query getAllAtractionsHeader($slug:String){
    getAllAtractionsHeader(Slug: $slug) {
      status
      data{
        _ID
        Name
        Url
        Image
      }
    }
  }
`;

export const GET_HOME = gql`
query getLanguegeHole($slug: String) {
  getLanguageHome(Slug: $slug) {
    status
    data {
      TitleGoogle
      DescriptionGoogle
      Title
      Image
      Slogan
    }
  }
}
`

export const SEARCH_HOME = gql`
  query getSearchHome($slug:String,$text:String){
    getSearchHome(Slug: $slug, Text: $text){
      status
      data{
        name
        url
        type
      }
    }
  }
`
//# Graphql => global
import { gql } from "@apollo/client";
export const NEW_ENQUIRY = gql`
  mutation newInquiry(
    $name: String!
    $prefix: String!
    $email: String!
    $trip: String!
    $subject: String!
    $query: String!
  ) {
    newInquiry(
      Name: $name
      Prefix: $prefix
      Email: $email
      TourName: $trip
      Subject: $subject
      Consult: $query
    ) {
      status
      message
    }
  }
`;

//# Import original module declaarations
import "styled-components";
//# Import type from above file
import { ThemeType } from "./themes";
//# Extending the styled components module
declare module "styled-components" {
  //# Extends the global ** Default Theme ** with our ** theme type **
  export interface DefaultTheme extends ThemeType {}
}

//# React icons
import { BsStarFill, BsStarHalf, BsStar } from "react-icons/bs";
import { IconType } from "react-icons/lib";

//# Types props
type IRating = [quality: IconType[], countReviews: number];

//# Hook => useRating
export const useRating = (quantity: number, reviews: number = 1): IRating => {
  const rating = {
    count: quantity || 2.5,
    all: 0,
    half: 0,
  };
  const decimal: number = ((rating.count * 10) % 10) / 10;
  const review: number =
    decimal > 0.5 ? Math.round(quantity) : Math.floor(quantity);
  if (decimal < 0.6 && decimal > 0.0) {
    rating.half = 1;
  } else if (decimal > 0.5) {
    rating.all = review;
  }
  const missing: number = 5 - (review + rating.half);
  const quality: IconType[] = [];
  [...Array(review)].map(() => quality.push(BsStarFill));
  [...Array(rating.half)].map(() => quality.push(BsStarHalf));
  [...Array(missing)].map(() => quality.push(BsStar));

  return [quality, reviews];
};

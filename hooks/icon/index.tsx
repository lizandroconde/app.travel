 
import { IoIosRestaurant } from "react-icons/io";
import { HiOutlineTicket } from "react-icons/hi";
import { MdHotel,MdTrain,MdCleanHands} from "react-icons/md";
import { RiServiceFill } from "react-icons/ri";
import { AiFillCar } from "react-icons/ai";
import { GiMountains,GiClothes ,GiUmbrella,GiFirstAidKit} from "react-icons/gi";

interface IIcons {
    [index: string]: React.ReactNode
}

const  IconTrip:IIcons = {
    "trasport":<AiFillCar/> ,
    "food": <IoIosRestaurant/>,
    "ticket": <HiOutlineTicket/>,
    "hostel": <MdHotel/>,
    "service": <RiServiceFill/>,
    "train" : <MdTrain/>,
    "trekking": <GiMountains/>,
    "cloth": <GiClothes/>,
    "plastic": <GiUmbrella/>,
    "personal":<MdCleanHands/>,
    "kit": <GiFirstAidKit/>

}

export default IconTrip
import { createContext } from "react";
import { ICityHome } from "@interfaces/city";
import { ITrip, ITripPage } from "@interfaces/trip";
import { IStyle } from "@interfaces/travel-styles/index";
import { IAttractionsHome } from "@interfaces/attraction";

interface HomeInterface {
  cities: ICityHome[];
  trips: ITrip[];
  styles: IStyle[];
  atractions: IAttractionsHome[];
}

interface TripInterface {
  info: ITripPage;
}

interface hrefla {
  slug: string;
  cod: string;
  url: string;
}
interface HeaderInterface {
  languages?: hrefla[];
}

const InitialData = {
  Seo: {
    title: "Peru tour ",
    description: "Desc Peru tour",
    canonical: "",
    image:
      "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
  },
  Title: "Peru tour ",
  Gallery: [
    {
      name: "title text",
      alt: "alt text",
      url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
    },
    {
      name: "title 1",
      alt: "alt 1 ",
      url: "https://andeanrajuexpeditions.com/wp-content/uploads/2020/01/SALKANTAY-TREK-1024X640-1024x530.jpg",
    },
  ],
  Valoration: {
    Cant: 14,
    Ratio: 4.1,
  },
  Location: "Cusco, Peru",
  Details: {
    max: 15,
    min: 20,
    duration: {
      Type: "D",
      Count: 1,
    },
    location: "Cusco, Peru",
    places: "",
    languages: "Espanol, English",
  },
  Intro: {
    introduction: "xd",
    highlights: '["xd","xd"]',
  },
  Places: [
    {
      name: "Machu Picchu",
      image: {
        name: "title text",
        alt: "alt text",
        url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
      },
    },
  ],
  Include: {
    Include: [
      {
        Icon: "null",
        Title: "No se Aun",
        Content: '["xd","otra"]',
      },
    ],
    Recommendations: [
      {
        Icon: "null",
        Title: "test",
        Content: '["test","test"]',
      },
    ],
    NoInclude: '["xs","test"]',
  },
  Itinerary: [
    {
      gallery: [
        {
          name: "title text",
          alt: "alt text",
          url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
        },
      ],
      day: "1",
      title: "title",
      content: "this content is html conten all content is html ",
    },
    {
      gallery: [
        {
          name: "title text",
          alt: "alt text",
          url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
        },
      ],
      day: "3",
      title: "Test",
      content: "test",
    },
    {
      gallery: [
        {
          name: "title text",
          alt: "alt text",
          url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg",
        },
      ],
      day: "2",
      title: "xD",
      content:
        "<p>We will pick you up from your hotel around 4:30 am – 5:30 am and head south through the town of Urcos to the district of Checacupe (2 hours by bus) and then follow the road to Pitumarca where we will have breakfast.</p>\n<p>Then we continue along the road through hamlets until we reach the community of Hanchipata – Quesiuno (1 hour and 20 minutes by bus), the ideal starting point for the trek.</p>\n<p>We will see incredible mountains like Ausangate, beautiful blue lagoons and many other natural sites.<br>\nThe hike to Vinicunca (Rainbow Mountain) will take approximately 3 hours. At this peak we will savor the view for about an hour taking pictures of the beautiful scenery before descending the trail and returning to our bus.<br>\nWe will return to Pitumarca for lunch and then head back to Cusco.</p>",
    },
  ],
  Aditional: "",
  Faq: [
    {
      ask: "test ask ",
      response: "test repply",
    },
  ],
  CodWetravel: 123412,
  Price: {
    Amount: 20.2,
    Discount: 20,
  },
};

export const HomeContext = createContext<HomeInterface>({
  cities: [],
  trips: [],
  styles: [],
  atractions: [],
});
export const TripContext = createContext<TripInterface>({ info: InitialData });
export const HeaderContext = createContext<HeaderInterface>({ languages: [] });

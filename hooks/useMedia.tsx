//# React
import { useState, useEffect } from "react";
export const useMedia = (query: string): boolean => {
  //# States
  const [matches, setMatches] = useState<boolean>(false);
  //# Prerender
  useEffect(() => {
    //# Media query from window
    const media: MediaQueryList = window.matchMedia(query);
    if (media.matches !== matches) {
      setMatches(media.matches);
    }
    //# Event listenes from match media query
    const listener = (): void => {
      setMatches(media.matches);
    };
    //# Add event media query
    media.addEventListener("change", listener);
    //# Remove the event media query
    return (): void => media.removeEventListener("change", listener);
  }, [matches, query]);
  //# return the matches: true | false
  return matches;
};

interface ISeparatePrice {
    price: number
}

export const ValidateSeparatePrice = ({ price }: ISeparatePrice): string => {
    let newprice = price.toString().split(".");
    return `${newprice[0]},${typeof newprice[1] == "undefined" ? "00" : newprice[1]}`;
}


interface IValidateDiscount {
    prices: number,
    discount: number;
}

export const ValidateDiscount = ({ prices, discount }: IValidateDiscount) => {
    let price = Number((prices - (prices * discount) / 100).toFixed(0));
    return ValidateSeparatePrice({ price })
}


export const ValidateNormalDiscount = ({ prices, discount }: IValidateDiscount) => {
    let price = Number((prices - (prices * discount) / 100).toFixed(0));
    return price
}

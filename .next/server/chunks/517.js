"use strict";
exports.id = 517;
exports.ids = [517];
exports.modules = {

/***/ 8517:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "XK": () => (/* binding */ GET_ATTRACTIONS_HOME),
/* harmony export */   "q9": () => (/* binding */ GET_ONE_ATTRACTION),
/* harmony export */   "q1": () => (/* binding */ GET_ALL_TRIP_ATTRACTION),
/* harmony export */   "MQ": () => (/* binding */ GET_HREFLA_ATTRACTION)
/* harmony export */ });
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_ATTRACTIONS_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getAttactionshome($slug: String!) {
    getAttactionshome(Slug:$slug){   
    data{
        Name
        Image
        Url
        Trips
   }
   }
  }
`;
const GET_ONE_ATTRACTION = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getAttraction($slug: String, $type: String) {
    getOneAttraction(Slug:$slug, Type:$type){
      status
      data {
        Title
        Name
        Description
        Canonical
        Image {
          name
          url
          alt
        }
      }
    }
  }
`;
const GET_ALL_TRIP_ATTRACTION = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getAllTrip($slug: String, $type: String){
    getTripsAttraction(Slug:$slug,Type:$type){
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;
const GET_HREFLA_ATTRACTION = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getPlaceHrefla($slug:String,$place:String){
    getPlaceHrefla(Slug:$slug,Place:$place){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`;

/***/ })

};
;
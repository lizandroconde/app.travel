"use strict";
exports.id = 607;
exports.ids = [607];
exports.modules = {

/***/ 9607:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ew": () => (/* binding */ GET_TRIPS_HOME),
/* harmony export */   "Ge": () => (/* binding */ GET_TRIPS_CITY),
/* harmony export */   "Zg": () => (/* binding */ GET_TRIP_SLUG)
/* harmony export */ });
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_TRIPS_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getTripshome($slug: String!) {
    getTripshome(Slug: $slug) {
      status
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;
const GET_TRIPS_CITY = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getTripsCity($slug: String,$country: String,$city: String) {
    getTripsCity(Slug: $slug,Country:$country,City:$city) {
      status
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;
const GET_TRIP_SLUG = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getTripSlug(
    $Slug: String!
    $Country: String!
    $City: String!
    $Trip: String!
  ) {
    getTripSlug(Slug: $Slug, Country: $Country, City: $City, Trip: $Trip) {
      data {
        Details {
          duration {
            Count
            Type
          }
          max
          min
          location
          languages
        }
        Seo {
          title
          description
          canonical
          image
        }
        Places{
          name
          image {
            name
            alt
            url
          }
        }
        Location
        Title
        Valoration {
          Cant
          Ratio
        }
        Gallery {
          name
          alt
          url
        }
        Intro {
          introduction
          highlights
        }
        Itinerary {
          day
          title
          content
          gallery {
            name
            url
          }
        }
        Include {
          Include {
            Icon
            Content
            Title
          }
          NoInclude
          Recommendations{
            Icon
            Title
            Content
          }
        }
        Faq {
          ask
          response
        }
        Aditional
        CodWetravel
        Price{
          Amount
          Discount
        }
      }
    }
  }
`;

/***/ })

};
;
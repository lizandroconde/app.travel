"use strict";
exports.id = 844;
exports.ids = [844];
exports.modules = {

/***/ 39:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ layout)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: ./server/routes.ts
var routes = __webpack_require__(2914);
// EXTERNAL MODULE: ./hooks/useMedia.tsx
var useMedia = __webpack_require__(4403);
// EXTERNAL MODULE: external "@apollo/client"
var client_ = __webpack_require__(8074);
// EXTERNAL MODULE: ./gql/global/querys.ts
var querys = __webpack_require__(5550);
// EXTERNAL MODULE: external "react-cool-onclickoutside"
var external_react_cool_onclickoutside_ = __webpack_require__(62);
var external_react_cool_onclickoutside_default = /*#__PURE__*/__webpack_require__.n(external_react_cool_onclickoutside_);
// EXTERNAL MODULE: ./node_modules/react-icons/fi/index.esm.js
var index_esm = __webpack_require__(6893);
// EXTERNAL MODULE: ./node_modules/react-icons/ai/index.esm.js
var ai_index_esm = __webpack_require__(8193);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(9914);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/header/navbar/styled.tsx
//# Styled components

const Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-0"
})(["position:fixed;left:", ";top:0;z-index:1001;width:", ";height:100%;background:", ";overflow:hidden;transition:all 0.2s ease-in-out;"], ({
  active,
  theme: {
    toRem
  }
}) => active ? 0 : toRem(-260), ({
  theme: {
    toRem
  }
}) => toRem(260), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const Auth = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-1"
})(["width:100%;height:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";color:", ";background:", ";display:flex;align-items:center;justify-content:center;"], ({
  theme: {
    toRem
  }
}) => toRem(110), ({
  theme: {
    toRem
  }
}) => toRem(30), ({
  theme: {
    toRem
  }
}) => toRem(30), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842);
const LogIn = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-2"
})(["display:flex;justify-content:center;align-items:center;width:", ";height:", ";background:", ";border-width:", ";border-color:", ";border-style:solid;border-radius:50%;margin:auto;"], ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    toRem
  }
}) => toRem(2), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const User = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1evm13u-3"
})(["display:flex;font-size:", ";color:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-4"
})(["height:calc(100% - ", ");overflow-y:scroll;padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(110), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const List = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-5"
})(["width:100%;height:", ";font-weight:", ";text-align:center;cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(43), ({
  bold
}) => bold ? 700 : 400);
const Item = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1evm13u-6"
})(["display:block;padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";font-size:", ";color:", ";line-height:120%;"], ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50);
const Space = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-7"
})(["border-top-width:", ";border-top-color:", ";border-top-style:solid;margin-top:", ";margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYC7D0D9, ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const Drop = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1evm13u-8"
})(["display:flex;float:right;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(18));
const Backdrop = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-9"
})(["display:flex;align-items:center;position:absolute;width:100%;left:0;top:", ";padding:", ";height:", ";background:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(110), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const Back = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1evm13u-10"
})(["display:flex;color:", ";font-size:", ";margin-right:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(26), ({
  theme: {
    toRem
  }
}) => toRem(40));
const TabGroup = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-11"
})(["width:100%;background-color:", ";position:absolute;left:", ";top:", ";z-index:6;transition:all 0.2s ease-in-out;height:calc(100% - ", ");padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  active,
  theme: {
    toRem
  }
}) => active ? 0 : toRem(256), ({
  theme: {
    toRem
  }
}) => toRem(160), ({
  theme: {
    toRem
  }
}) => toRem(160), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const TabSubGroup = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-12"
})(["width:100%;background-color:", ";position:absolute;left:", ";top:0;z-index:6;transition:all 0.2s ease-in-out;overflow-x:hidden;height:100%;padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  active,
  theme: {
    toRem
  }
}) => active ? 0 : toRem(260), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const SubBackdrop = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1evm13u-13"
})(["display:flex;align-items:center;position:absolute;width:100%;left:0;top:", ";padding:", ";height:", ";background:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(-50), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(8475);
// EXTERNAL MODULE: ./hooks/useContext/index.tsx
var useContext = __webpack_require__(960);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/header/navbar/index.tsx
//# React
 //# Next routes

 //# Rooks

 //# Interfaces

//# Icons

 //# Styles components



 //# Types props



//# Component => navbar
const Navbar = ({
  destinations,
  travelStyles,
  active,
  setMenu
}) => {
  const {
    languages
  } = (0,external_react_.useContext)(useContext/* HeaderContext */.pI);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# Hooks

  const router = (0,router_.useRouter)();
  const ref = external_react_cool_onclickoutside_default()(() => {
    setMenu(false);
  }); //# States

  const {
    0: tab,
    1: setTab
  } = (0,external_react_.useState)({
    menu: -1,
    submenu: -1
  }); //# Methods

  const onRoute = path => {
    router.push(path);
  };

  const onBack = (e, tab) => {
    e.stopPropagation();
    setTab(tab);
  };

  const onTab = (e, tab) => {
    e.stopPropagation();
    setTab(tab);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Container, {
    ref: ref,
    active: active,
    children: [/*#__PURE__*/jsx_runtime_.jsx(Auth, {
      children: /*#__PURE__*/jsx_runtime_.jsx(LogIn, {
        children: /*#__PURE__*/jsx_runtime_.jsx(User, {
          children: /*#__PURE__*/jsx_runtime_.jsx(ai_index_esm/* AiOutlineUser */.nf1, {})
        })
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Group, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(List, {
        bold: true,
        onClick: e => onTab(e, {
          menu: 3,
          submenu: -1
        }),
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: [t("header.laguages"), /*#__PURE__*/jsx_runtime_.jsx(Drop, {
            children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronRight */.Tfp, {})
          }), tab.menu === 3 && tab.submenu === -1 && /*#__PURE__*/jsx_runtime_.jsx(Backdrop, {
            onClick: e => onBack(e, {
              menu: -1,
              submenu: -1
            }),
            children: /*#__PURE__*/jsx_runtime_.jsx(Back, {
              children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronLeft */.YFh, {})
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx(TabGroup, {
          active: tab.menu === 3,
          children: languages === null || languages === void 0 ? void 0 : languages.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx(List, {
            bold: true,
            onClick: () => onRoute(item.url),
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
              children: [" ", t(`header.${item === null || item === void 0 ? void 0 : item.slug}`)]
            })
          }, index))
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(Space, {}), /*#__PURE__*/jsx_runtime_.jsx(List, {
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: [" ", t("header.login")]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(List, {
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: [" ", t("header.signup")]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(Space, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(List, {
        bold: true,
        onClick: e => onTab(e, {
          menu: 2,
          submenu: -1
        }),
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: [t("header.travelstyles"), /*#__PURE__*/jsx_runtime_.jsx(Drop, {
            children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronRight */.Tfp, {})
          }), tab.menu === 2 && tab.submenu === -1 && /*#__PURE__*/jsx_runtime_.jsx(Backdrop, {
            onClick: e => onBack(e, {
              menu: -1,
              submenu: -1
            }),
            children: /*#__PURE__*/jsx_runtime_.jsx(Back, {
              children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronLeft */.YFh, {})
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx(TabGroup, {
          active: tab.menu === 2,
          children: travelStyles.map((travel, e) => {
            return /*#__PURE__*/jsx_runtime_.jsx(List, {
              bold: true,
              onClick: () => onRoute(travel.Url),
              children: /*#__PURE__*/jsx_runtime_.jsx(Item, {
                children: travel.Name
              })
            }, e);
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(List, {
        bold: true,
        onClick: e => onTab(e, {
          menu: 1,
          submenu: -1
        }),
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: [t("header.destinations"), /*#__PURE__*/jsx_runtime_.jsx(Drop, {
            children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronRight */.Tfp, {})
          }), tab.menu === 1 && tab.submenu === -1 && /*#__PURE__*/jsx_runtime_.jsx(Backdrop, {
            onClick: e => onBack(e, {
              menu: -1,
              submenu: -1
            }),
            children: /*#__PURE__*/jsx_runtime_.jsx(Back, {
              children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronLeft */.YFh, {})
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(TabGroup, {
            active: tab.menu === 1,
            children: destinations.map((d, index) => {
              return /*#__PURE__*/jsx_runtime_.jsx(List, {
                bold: true,
                onClick: e => onTab(e, {
                  menu: 1,
                  submenu: index
                }),
                children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
                  children: [d.Name, /*#__PURE__*/jsx_runtime_.jsx(Drop, {
                    children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronRight */.Tfp, {})
                  }), tab.submenu === index && /*#__PURE__*/jsx_runtime_.jsx(SubBackdrop, {
                    onClick: e => onBack(e, {
                      menu: 1,
                      submenu: -1
                    }),
                    children: /*#__PURE__*/jsx_runtime_.jsx(Back, {
                      children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronLeft */.YFh, {})
                    })
                  }), /*#__PURE__*/jsx_runtime_.jsx(TabSubGroup, {
                    active: tab.submenu === index,
                    children: d.Citys.map((city, e) => {
                      return /*#__PURE__*/jsx_runtime_.jsx(List, {
                        onClick: () => onRoute(city.Url),
                        children: /*#__PURE__*/jsx_runtime_.jsx(Item, {
                          children: city.Name
                        })
                      }, e);
                    })
                  })]
                })
              }, index);
            })
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(List, {
        bold: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Item, {
          children: ["   ", t("header.contact_us")]
        })
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/header/styled.tsx
//# Styled components

const styled_Container = /*#__PURE__*/external_styled_components_default().header.withConfig({
  componentId: "sc-1w62a7g-0"
})(["position:relative;z-index:4;background:", ";border-width:", ";border-color:", ";border-bottom-style:solid;border-top-style:solid;"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
const Auto = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-1"
})(["display:flex;width:100%;max-width:", ";height:", ";padding-left:", ";padding-right:", ";margin-left:auto;margin-right:auto;", "{padding-left:", ";padding-right:", ";}", "{padding-left:", ";padding-right:", ";}", "{padding-left:", ";padding-right:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(1440), ({
  theme: {
    toRem
  }
}) => toRem(56), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    media
  }
}) => media.to(0, 1024, true), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    media
  }
}) => media.to(0, 768, true), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    media
  }
}) => media.to(0, 568, true), ({
  theme: {
    toRem
  }
}) => toRem(36), ({
  theme: {
    toRem
  }
}) => toRem(36));
const Burger = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-2"
})(["position:relative;padding-top:", ";padding-bottom:", ";margin-right:", ";cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(16));
const King = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-3"
})(["border-radius:", ";width:", ";height:", ";margin-top:", ";background:", ";transition:background 0.15s ease-in-out;", ":hover &{background:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(100), ({
  theme: {
    toRem
  }
}) => toRem(25), ({
  theme: {
    toRem
  }
}) => toRem(2), ({
  theme: {
    toRem
  }
}) => toRem(5), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYC7D0D9, Burger, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50);
const Logo = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-4"
})(["display:flex;cursor:pointer;align-items:center;width:auto;background:", ";height:", ";padding-left:0;padding-right:", ";padding-top:", ";padding-bottom:", ";margin-right:auto;margin-top:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(54), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(1));
const Menu = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-1w62a7g-5"
})(["width:auto;height:100%;list-style:none;display:flex;align-items:center;"]);
const styled_Item = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-6"
})(["position:", ";width:auto;height:100%;float:left;align-items:center;padding-left:", ";padding-right:", ";border-width:", ";border-color:transparent;border-style:solid;margin-bottom:", ";&:hover{border-left-color:", ";border-right-color:", ";border-bottom-color:", ";cursor:pointer;}"], ({
  relative
}) => relative && "relative", ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(-1), ({
  border,
  theme: {
    colors
  }
}) => !border && colors.gray.rgb.GRYEBEEF2, ({
  border,
  theme: {
    colors
  }
}) => !border && colors.gray.rgb.GRYEBEEF2, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const Text = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1w62a7g-7"
})(["font-size:", ";line-height:", ";color:", ";", ":hover &{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(56), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, styled_Item, ({
  theme: {
    colors
  }
}) => colors.blue.rgb.BL286283);
const NavDeal = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-8"
})(["background:", ";margin-top:", ";border-width:", ";border-color:", ";border-style:solid;border-top-style:none;position:absolute;top:100%;right:0;z-index:5;"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
const ToDeal = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-1w62a7g-9"
})(["display:flex;flex-direction:column;list-style:none;height:100%;a{text-decoration:none;color:inherit;}"]);
const SectionDeal = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-10"
})(["width:100%;display:flex;justify-content:center;align-items:center;min-width:", ";white-space:nowrap;color:", ";font-size:", ";text-align:left;font-weight:400;line-height:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";transition:background 0.1s linear,color 0.1s linear;&:hover{background:", ";color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(150), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(17), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const Nav = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-11"
})(["width:100%;height:", ";background:", ";padding-top:", ";padding-bottom:", ";cursor:auto;overflow:hidden;margin-top:", ";border-width:", ";border-color:", ";border-style:solid;border-top-style:none;position:absolute;top:100%;right:0;z-index:5;box-shadow:0 16px 24px rgb(3 54 63 / 16%),0 8px 16px rgb(3 54 63 / 4%),0 4px 8px rgb(3 54 63 / 4%),0 2px 4px rgb(3 54 63 / 2%),0 1px 2px rgb(3 54 63 / 4%),0 -1px 2px rgb(3 54 63 / 4%);"], ({
  theme: {
    toRem
  }
}) => toRem(415), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(35), ({
  theme: {
    toRem
  }
}) => toRem(35), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
const Top = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-1w62a7g-12"
})(["display:flex;justify-content:flex-start;flex-direction:row;max-width:", ";height:100%;margin:0 auto;position:relative;list-style:none;"], ({
  theme: {
    toRem
  }
}) => toRem(1164));
const TopAttractions = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-1w62a7g-13"
})(["display:grid;grid-template-columns:repeat(auto-fill,minmax(250px,1fr));max-width:", ";margin:0 auto;list-style:none;"], ({
  theme: {
    toRem
  }
}) => toRem(1164));
const Paragrpah = /*#__PURE__*/external_styled_components_default().p.withConfig({
  componentId: "sc-1w62a7g-14"
})(["display:flex;align-items:center;"]);
const Section = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-15"
})(["width:", ";max-width:", ";float:left;padding-left:", ";padding-right:", ";border-width:", ";border-color:transparent;border-style:solid;margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(170), ({
  theme: {
    toRem
  }
}) => toRem(185), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(-1));
const SectionAtracction = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-16"
})(["margin-top:10px;float:left;padding-left:", ";padding-right:", ";border-width:", ";border-color:transparent;border-style:solid;margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(-1));
const Title = /*#__PURE__*/external_styled_components_default().h3.withConfig({
  componentId: "sc-1w62a7g-17"
})(["display:inline-block;white-space:nowrap;color:", ";font-size:", ";line-height:", ";font-weight:700;text-align:left;padding-left:", ";padding-right:", ";padding-top:", ";padding-bottom:", ";margin-bottom:", ";margin-right:auto;margin-left:auto;transition:color 0.1s linear;cursor:pointer;&:hover{color:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D);
const TitleImage = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1w62a7g-18"
})(["cursor:pointer;display:grid;grid-template-columns:100px 200px;grid-gap:1rem;;font-size:", ";font-weight:700;img{border-radius:10px;}&:hover{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D);
const SubTop = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-1w62a7g-19"
})(["width:100%;max-height:", ";display:flex;flex-direction:column;flex-wrap:wrap;margin-top:", ";list-style:none;a{text-decoration:none;color:inherit;}"], ({
  theme: {
    toRem
  }
}) => toRem(330), ({
  theme: {
    toRem
  }
}) => toRem(30));
const SubSection = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-20"
})(["text-decoration:none;display:block;width:100%;height:auto;color:", ";font-size:", ";text-align:left;font-weight:400;line-height:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";transition:background 0.1s linear,color 0.1s linear;cursor:pointer;a{text-decoration:none;color:inherit;}&:hover{color:", ";background:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(17), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D);
const AllSection = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-1w62a7g-21"
})(["width:100%;height:auto;color:", ";font-size:", ";line-height:", ";text-align:left;font-weight:700;padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";border-top-width:", ";border-top-color:", ";border-top-style:solid;transition:color 0.1s linear;cursor:pointer;&:hover{color:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(23), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2, ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D);
;// CONCATENATED MODULE: ./components/header/menu-item/index.tsx
//# React
 //# Styles components

 //# Types props



//# Component => menu item
const MenuItem = ({
  relative,
  title,
  children
}) => {
  //# Hooks
  const {
    0: show,
    1: setShow
  } = (0,external_react_.useState)(false); //# Methods

  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Item, {
    relative: relative,
    onMouseOver: onShow,
    onMouseLeave: onHide,
    children: [/*#__PURE__*/jsx_runtime_.jsx(Text, {
      children: title
    }), show && children]
  });
};
;// CONCATENATED MODULE: ./components/header/menu-nav/index.tsx
//# React router
 //# Interfaces

//# Styled components


 //# Types props



//# Component => menu nav destinations
const MenuNavDestinations = ({
  destinations
}) => {
  //# Hooks
  const router = (0,router_.useRouter)(); //# Methods

  const onRoute = path => {
    router.push(path);
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Nav, {
    children: /*#__PURE__*/jsx_runtime_.jsx(Top, {
      children: destinations.map((travel, e) => {
        const splitCity = travel.Citys.length > 6 ? travel.Citys.slice(0, 6) : travel.Citys;
        const seeAll = travel.Citys.length > 6 ? true : false;
        return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Section, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Title, {
            children: travel.Name
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(SubTop, {
            children: [splitCity.map((city, f) => {
              return /*#__PURE__*/jsx_runtime_.jsx(routes/* Link */.rU, {
                route: city.Url,
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: /*#__PURE__*/jsx_runtime_.jsx(SubSection, {
                    children: city.Name
                  })
                })
              }, f);
            }), seeAll && /*#__PURE__*/jsx_runtime_.jsx(AllSection, {
              onClick: () => onRoute(travel.Url),
              children: "See all"
            })]
          })]
        }, e);
      })
    })
  });
}; //# Types props

//# Component => menu nav travel styles
const MenuNavTravelStyles = ({
  travelStyles
}) => {
  //# Hooks
  const router = (0,router_.useRouter)(); //# Methods

  const onRoute = path => {
    router.push(path);
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Nav, {
    children: /*#__PURE__*/jsx_runtime_.jsx(TopAttractions, {
      children: travelStyles.map((travel, e) => {
        return /*#__PURE__*/jsx_runtime_.jsx(SectionAtracction, {
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(TitleImage, {
            onClick: () => onRoute(travel.Url),
            children: [/*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
              src: `${travel.Image}`,
              alt: travel.Name,
              width: "100px",
              height: "60px",
              layout: "fixed"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Paragrpah, {
              children: [" ", travel.Name]
            })]
          })
        }, e);
      })
    })
  });
};
;// CONCATENATED MODULE: ./components/header/menu-list/index.tsx
//# React
 //# Next

 //# Styles components

 //# Types props


const LanguageMenu = ({
  children
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx(NavDeal, {
    children: /*#__PURE__*/jsx_runtime_.jsx(ToDeal, {
      children: children
    })
  });
}; //# Component => nav list

const MenuList = ({
  children
}) => {
  //# Hooks
  const router = useRouter();
  return /*#__PURE__*/_jsx(C.NavDeal, {
    children: /*#__PURE__*/_jsx(C.ToDeal, {
      children: Children.map(children, child => {
        const item = child;

        const onClick = () => {//router.replace(item.props.value);
        };

        return /*#__PURE__*/cloneElement(item, {
          onClick
        });
      })
    })
  });
}; //# Types props

//# Component => list
const ListItem = ({
  onClick,
  children
}) => {
  return /*#__PURE__*/_jsx(C.SectionDeal, {
    onClick: onClick,
    children: children
  });
};
// EXTERNAL MODULE: external "react-modal"
var external_react_modal_ = __webpack_require__(9997);
var external_react_modal_default = /*#__PURE__*/__webpack_require__.n(external_react_modal_);
// EXTERNAL MODULE: ./node_modules/react-icons/io5/index.esm.js
var io5_index_esm = __webpack_require__(155);
// EXTERNAL MODULE: ./node_modules/react-icons/bs/index.esm.js
var bs_index_esm = __webpack_require__(3750);
// EXTERNAL MODULE: ./node_modules/react-icons/ti/index.esm.js
var ti_index_esm = __webpack_require__(9327);
// EXTERNAL MODULE: external "react-hook-form"
var external_react_hook_form_ = __webpack_require__(2662);
;// CONCATENATED MODULE: ./components/header/contact-us/check-booking/styled.tsx
//# Styled components

const check_booking_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-0"
})(["width:auto;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(10));
const styled_Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-1"
})(["display:flex;justify-content:space-between;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
const FormControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-2"
})(["width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const Label = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-3"
})(["color:", ";font-size:", ";font-weight:500;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const InputControl = /*#__PURE__*/external_styled_components_default().input.withConfig({
  componentId: "sc-1t221rf-4"
})(["width:100%;height:", ";padding-left:", ";padding-right:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;color:", ";font-size:", ";font-weight:500;line-height:120%;transition:border-color 0.3s,border 0.3s ease-in-out;&:focus{border-color:", ";}&::placeholder{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(45), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC);
const Error = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-5"
})(["width:auto;height:", ";color:", ";font-size:", ";font-weight:normal;line-height:120%;text-align:left;"], ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Footer = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-6"
})(["width:100%;display:flex;justify-content:space-between;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
const Message = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1t221rf-7"
})(["width:100%;margin-bottom:", ";margin-top:", ";color:", ";font-size:", ";font-weight:300;line-height:120%;"], ({
  theme: {
    toRem
  }
}) => toRem(30), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Back = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-1t221rf-8"
})(["width:", ";background:", ";color:", ";font-size:", ";font-weight:500;line-height:120%;cursor:pointer;border-width:", ";border-color:", ";border-style:solid;border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";box-shadow:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(200), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(17), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYDDD, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem,
    colors
  }
}) => `
   ${toRem(3)} ${toRem(3)} ${toRem(3)} 0 ${colors.black.rgba.BK000(0.1)}
  `);
const Send = /*#__PURE__*/external_styled_components_default()(styled_Back).withConfig({
  componentId: "sc-1t221rf-9"
})(["background:", ";color:", ";border-color:", ";float:right;&:disabled{opacity:0.45;cursor:not-allowed;}"], ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842);
;// CONCATENATED MODULE: ./components/header/contact-us/check-booking/index.tsx
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# React
 //# React Hooks form


 //# Styles components

 //# Types props



//# Component => check booking
const CheckBooking = ({
  onCancel
}) => {
  //# States
  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(false);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# Hooks

  const {
    register,
    formState: {
      errors,
      isDirty,
      isValid
    },
    handleSubmit
  } = (0,external_react_hook_form_.useForm)({
    mode: "all"
  }); //# Methods

  const onBooking = async data => {
    setLoading(true);
    setLoading(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(check_booking_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Group, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Label, {
          children: t("contact_us.consult.cod")
        }), /*#__PURE__*/jsx_runtime_.jsx(InputControl, _objectSpread({
          placeholder: "CT-XXXXXX"
        }, register("booking", {
          required: `${t("contact_us.consult_error.cod")}`
        }))), /*#__PURE__*/jsx_runtime_.jsx(Error, {
          children: errors.booking && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.booking.message
          })
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Label, {
          children: t("contact_us.consult.email")
        }), /*#__PURE__*/jsx_runtime_.jsx(InputControl, _objectSpread({
          placeholder: "micorreo@gmail.com"
        }, register("email", {
          required: `${t("contact_us.consult_error.email")}`
        }))), /*#__PURE__*/jsx_runtime_.jsx(Error, {
          children: errors.email && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.email.message
          })
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Message, {
      children: t("contact_us.consult_error.click")
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Footer, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Back, {
        onClick: onCancel,
        children: t("contact_us.back")
      }), /*#__PURE__*/jsx_runtime_.jsx(Send, {
        disabled: !isDirty || !isValid || loading,
        onClick: handleSubmit(onBooking),
        children: t("contact_us.next")
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./gql/global/mutations.ts
//# Graphql => global

const NEW_ENQUIRY = client_.gql`
  mutation newInquiry(
    $name: String!
    $prefix: String!
    $email: String!
    $trip: String!
    $subject: String!
    $query: String!
  ) {
    newInquiry(
      Name: $name
      Prefix: $prefix
      Email: $email
      TourName: $trip
      Subject: $subject
      Consult: $query
    ) {
      status
      message
    }
  }
`;
// EXTERNAL MODULE: ./node_modules/react-icons/hi/index.esm.js
var hi_index_esm = __webpack_require__(3854);
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/check-policy/styled.tsx
//# Styled components

const check_policy_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-tli4yo-0"
})(["width:auto;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-tli4yo-1"
})(["display:flex;align-items:center;justify-content:flex-start;column-gap:", ";width:100%;margin-bottom:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(30), ({
  theme: {
    toRem
  }
}) => toRem(10));
const Checked = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-tli4yo-2"
})(["display:flex;justify-content:center;align-items:center;width:", ";height:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;background:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  check,
  theme: {
    colors
  }
}) => check && colors.black.rgb.BLk2C3E50);
const Check = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-tli4yo-3"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-tli4yo-4"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;text-align:left;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const Linked = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-tli4yo-5"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;margin-left:", ";cursor:pointer;"], ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(4));
const styled_Error = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-tli4yo-6"
})(["width:auto;height:", ";color:", ";font-size:", ";font-weight:normal;line-height:120%;text-align:left;margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(5));
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/check-policy/index.tsx
//# React
 //# React Hooks form

//# Icons
 //# Styles components

 //# Types props



//# Component => policy
const CheckPolicy = ({
  value,
  setValue,
  error
}) => {
  //# Methods
  const onChecked = () => {
    setValue(!value);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(check_policy_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Content, {
      onClick: onChecked,
      children: [/*#__PURE__*/jsx_runtime_.jsx(Checked, {
        check: value,
        children: value && /*#__PURE__*/jsx_runtime_.jsx(Check, {
          children: /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineCheck */.Sul, {})
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Text, {
        children: ["Al env\xEDar este formulario est\xE1s aceptando nuestra", /*#__PURE__*/jsx_runtime_.jsx(Linked, {
          children: "Pol\xEDtica de privacidad"
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(styled_Error, {
      children: error && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
        children: error === null || error === void 0 ? void 0 : error.message
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/subject-trip/styled.tsx
//# Styled components

const subject_trip_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-0"
})(["width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(10));
const styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-1"
})(["position:relative;width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(1));
const Selected = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-2"
})(["display:flex;align-items:center;justify-content:flex-start;height:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;border-bottom-right-radius:", ";border-bottom-left-radius:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(45), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  active
}) => active && 0, ({
  active
}) => active && 0);
const subject_trip_styled_Text = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-pk3gwy-3"
})(["display:flex;flex:1;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;color:", ";font-size:", ";font-weight:normal;line-height:120%;align-items:center;padding-left:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(10));
const Down = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-pk3gwy-4"
})(["display:flex;width:", ";padding-right:", ";color:", ";font-size:", ";text-align:center;cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(25), ({
  theme: {
    toRem
  }
}) => toRem(5), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(10));
const subject_trip_styled_Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-5"
})(["position:absolute;top:100%;left:0;z-index:1050;width:auto;min-width:100%;display:block;overflow-y:auto;height:auto;max-height:", ";border-bottom-right-radius:", ";border-bottom-left-radius:", ";border-width:", ";border-color:", ";border-style:solid;margin-top:", ";background:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(240), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  theme: {
    toRem
  }
}) => toRem(-1), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const subject_trip_styled_Item = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-6"
})(["width:100%;color:", ";font-size:", ";font-weight:", ";line-height:120%;background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";cursor:pointer;transition:background 0.2s ease-out;&:hover{background:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  active
}) => active && 600, ({
  active,
  theme: {
    colors
  }
}) => active && colors.black.rgba.BK000(0.05), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(25), ({
  theme: {
    colors
  }
}) => colors.black.rgba.BK000(0.05));
const subject_trip_styled_Error = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-pk3gwy-7"
})(["width:auto;height:", ";color:", ";font-size:", ";font-weight:normal;line-height:120%;text-align:left;margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(5));
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/subject-trip/index.tsx
//# React
 //# React Hooks form

//# Rooks
 //# Icons

 //# Styles componets

 //# Types props



//# Component => subject trip
const SubjectTrip = ({
  value,
  setValue,
  error,
  children
}) => {
  //# Def
  const def = children.find(child => {
    const item = child;
    return item.props.children === value;
  }); //# Active

  const active = children.find(child => {
    const item = child;
    return item.props.selected && item;
  }); //# State

  const {
    0: show,
    1: setShow
  } = (0,external_react_.useState)(false);
  const {
    0: selected,
    1: setSelected
  } = (0,external_react_.useState)(); //# Methods

  const ref = external_react_cool_onclickoutside_default()(() => {
    setShow(false);
  });

  const onShow = () => setShow(!show);

  const onSelect = (value, text) => {
    setSelected({
      value,
      text
    });
    setValue(value);
    setShow(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(subject_trip_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Content, {
      ref: ref,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Selected, {
        active: show,
        onClick: onShow,
        children: [/*#__PURE__*/jsx_runtime_.jsx(subject_trip_styled_Text, {
          children: (selected === null || selected === void 0 ? void 0 : selected.text) || active.props.children || def
        }), /*#__PURE__*/jsx_runtime_.jsx(Down, {
          children: /*#__PURE__*/jsx_runtime_.jsx(bs_index_esm/* BsFillCaretDownFill */.F0C, {})
        })]
      }), show && /*#__PURE__*/jsx_runtime_.jsx(subject_trip_styled_Group, {
        children: external_react_.Children.map(children, child => {
          const item = child;
          const active = selected ? item.props.value === selected.value : item.props.selected ? true : false;

          const onClick = () => {
            onSelect(item.props.value, item.props.children);
          };

          return /*#__PURE__*/(0,external_react_.cloneElement)(item, {
            active,
            onClick
          });
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(subject_trip_styled_Error, {
      children: error && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
        children: error.message
      })
    })]
  });
}; //# Types props

//# Component => subject item
const SubjectItem = ({
  active,
  children,
  onClick
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx(subject_trip_styled_Item, {
    active: active,
    onClick: onClick,
    children: children
  });
};
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/styled.tsx
//# Styled components

const consult_trip_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-0"
})(["width:auto;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(10));
const consult_trip_styled_Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-1"
})(["display:flex;justify-content:space-between;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
const styled_FormControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-2"
})(["width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const styled_Label = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-3"
})(["color:", ";font-size:", ";font-weight:500;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_InputControl = /*#__PURE__*/external_styled_components_default().input.withConfig({
  componentId: "sc-6eo6wn-4"
})(["width:100%;height:", ";padding-left:", ";padding-right:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;color:", ";font-size:", ";font-weight:500;line-height:120%;transition:border-color 0.3s,border 0.3s ease-in-out;&:focus{border-color:", ";}&::placeholder{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(45), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC);
const TextControl = /*#__PURE__*/external_styled_components_default().textarea.withConfig({
  componentId: "sc-6eo6wn-5"
})(["width:100%;height:auto;padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;color:", ";font-size:", ";font-weight:500;line-height:", ";transition:border-color 0.3s,border 0.3s ease-in-out;&:focus{border-color:", ";}&::placeholder{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(19), ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC);
const consult_trip_styled_Error = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-6"
})(["width:auto;height:", ";color:", ";font-size:", ";font-weight:normal;line-height:120%;text-align:left;margin-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(5));
const styled_Footer = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-7"
})(["width:100%;display:flex;justify-content:space-between;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
const Response = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-8"
})(["display:flex;flex-direction:column;row-gap:", ";text-align:left;"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const Status = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-9"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Message = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-6eo6wn-10"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(14));
const consult_trip_styled_Back = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-6eo6wn-11"
})(["width:", ";background:", ";color:", ";font-size:", ";font-weight:500;line-height:120%;cursor:pointer;border-width:", ";border-color:", ";border-style:solid;border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";box-shadow:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(200), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(17), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYDDD, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem,
    colors
  }
}) => `
   ${toRem(3)} ${toRem(3)} ${toRem(3)} 0 ${colors.black.rgba.BK000(0.1)}
  `);
const styled_Send = /*#__PURE__*/external_styled_components_default()(consult_trip_styled_Back).withConfig({
  componentId: "sc-6eo6wn-12"
})(["background:", ";color:", ";border-color:", ";float:right;&:disabled{opacity:0.45;cursor:not-allowed;}"], ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842);
;// CONCATENATED MODULE: ./components/header/contact-us/consult-trip/index.tsx
function consult_trip_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function consult_trip_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { consult_trip_ownKeys(Object(source), true).forEach(function (key) { consult_trip_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { consult_trip_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function consult_trip_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# React
 //# Apollo client


 //# React Hooks form

 //# Components


 //# Styles components


 //# Types props



//# Component => consult trip
const ConsultTrip = ({
  onCancel
}) => {
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# States

  const {
    0: response,
    1: setResponse
  } = (0,external_react_.useState)();
  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(false); //# Hooks

  const {
    reset,
    control,
    register,
    formState: {
      errors,
      isDirty,
      isValid
    },
    handleSubmit
  } = (0,external_react_hook_form_.useForm)({
    mode: "all",
    defaultValues: {
      email: "",
      name: "",
      query: "",
      subject: "",
      trip: "",
      policy: false
    }
  }); //# Apollo mutations

  const [newEnquiry] = (0,client_.useMutation)(NEW_ENQUIRY); //# Methods

  const onBooking = async data => {
    setLoading(true);
    const {
      email,
      name,
      trip,
      subject,
      query
    } = data;
    const res = await newEnquiry({
      variables: {
        prefix: "es",
        email,
        name,
        trip,
        subject,
        query
      }
    });
    const status = res.data.newInquiry;
    setResponse(status);
    setLoading(false);
    setTimeout(() => {
      setResponse(undefined);
      reset({
        email: "",
        name: "",
        query: "",
        subject: "",
        trip: "",
        policy: false
      });
    }, 4000);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(consult_trip_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(consult_trip_styled_Group, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Label, {
          children: t("contact_us.consultour.name")
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_InputControl, consult_trip_objectSpread({}, register("name", {
          required: `${t("contact_us.consultour_error.name")}`
        }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
          children: errors.name && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.name.message
          })
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Label, {
          children: t("contact_us.consultour.enteremail")
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_InputControl, consult_trip_objectSpread({
          placeholder: t("contact_us.consultour.email")
        }, register("email", {
          required: `${t("contact_us.consultour_error.email")}`,
          pattern: {
            value: /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/,
            message: `${t("contact_us.consultour_error.email_valid")}`
          }
        }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
          children: errors.email && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.email.message
          })
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Label, {
        children: t("contact_us.consultour.trip")
      }), /*#__PURE__*/jsx_runtime_.jsx(styled_InputControl, consult_trip_objectSpread({}, register("trip", {
        required: `${t("contact_us.consultour_error.trip")}`
      }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
        children: errors.trip && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: errors.trip.message
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_react_hook_form_.Controller, {
      control: control,
      name: "subject",
      rules: {
        required: `${t("contact_us.consultour_error.subject")}`
      },
      render: ({
        field,
        formState
      }) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(SubjectTrip, {
        value: field.value,
        setValue: field.onChange,
        error: formState.errors.subject,
        children: [/*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: "",
          selected: true,
          children: t("contact_us.consultour.asunto.select")
        }, "0"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.question"),
          children: t("contact_us.consultour.asunto.question")
        }, "1"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.problems"),
          children: t("contact_us.consultour.asunto.problems")
        }, "2"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.qoute"),
          children: t("contact_us.consultour.asunto.qoute")
        }, "3")]
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Label, {
        children: ["   ", t("contact_us.consultour.consult")]
      }), /*#__PURE__*/jsx_runtime_.jsx(TextControl, consult_trip_objectSpread({
        rows: 5
      }, register("query", {
        required: `${t("contact_us.consultour_error.query")}`
      }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
        children: errors.query && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: errors.query.message
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_react_hook_form_.Controller, {
      control: control,
      name: "policy",
      rules: {
        required: `${t("contact_us.consultour_error.policy")}`
      },
      render: ({
        field,
        formState
      }) => /*#__PURE__*/jsx_runtime_.jsx(CheckPolicy, {
        value: field.value,
        setValue: field.onChange,
        error: formState.errors.policy
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Footer, {
      children: [response && /*#__PURE__*/(0,jsx_runtime_.jsxs)(Response, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Status, {
          children: response === null || response === void 0 ? void 0 : response.status
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_Message, {
          children: response === null || response === void 0 ? void 0 : response.message
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(consult_trip_styled_Back, {
        onClick: onCancel,
        children: [" ", t("contact_us.back")]
      }), /*#__PURE__*/jsx_runtime_.jsx(styled_Send, {
        disabled: !isDirty || !isValid || loading,
        onClick: handleSubmit(onBooking),
        children: loading ? /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: "Enviando..."
        }) : /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: "Enviar"
        })
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/header/contact-us/other-subject/index.tsx
function other_subject_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function other_subject_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { other_subject_ownKeys(Object(source), true).forEach(function (key) { other_subject_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { other_subject_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function other_subject_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# React
 //# Apollo client


 //# React Hooks form

 //# Components


 //# Styles components


 //# Types props



//# Component => other subject
const OtherSubject = ({
  onCancel
}) => {
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# States

  const {
    0: response,
    1: setResponse
  } = (0,external_react_.useState)();
  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(false); //# Hooks

  const {
    reset,
    control,
    register,
    formState: {
      errors,
      isDirty,
      isValid
    },
    handleSubmit
  } = (0,external_react_hook_form_.useForm)({
    mode: "all",
    defaultValues: {
      email: "",
      name: "",
      query: "",
      subject: "",
      policy: false
    }
  }); //# Apollo mutations

  const [newEnquiry] = (0,client_.useMutation)(NEW_ENQUIRY); //# Methods

  const onBooking = async data => {
    setLoading(true);
    const {
      email,
      name,
      subject,
      query
    } = data;
    const res = await newEnquiry({
      variables: {
        prefix: "es",
        email,
        name,
        trip: "",
        subject,
        query
      }
    });
    const status = res.data.newInquiry;
    setResponse(status);
    setLoading(false);
    setTimeout(() => {
      setResponse(undefined);
      reset({
        email: "",
        name: "",
        query: "",
        subject: "",
        policy: false
      });
    }, 3000);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(consult_trip_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(consult_trip_styled_Group, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Label, {
          children: t("contact_us.consultour.name")
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_InputControl, other_subject_objectSpread({}, register("name", {
          required: `${t("contact_us.consultour_error.name")}`
        }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
          children: errors.name && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.name.message
          })
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Label, {
          children: t("contact_us.consultour.enteremail")
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_InputControl, other_subject_objectSpread({
          placeholder: "micorreo@gmail.com"
        }, register("email", {
          required: `${t("contact_us.consultour_error.email")}`,
          pattern: {
            value: /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/,
            message: `${t("contact_us.consultour_error.email_valid")}`
          }
        }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
          children: errors.email && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: errors.email.message
          })
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_react_hook_form_.Controller, {
      control: control,
      name: "subject",
      rules: {
        required: `${t("contact_us.consultour_error.subject")}`
      },
      render: ({
        field,
        formState
      }) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(SubjectTrip, {
        value: field.value,
        setValue: field.onChange,
        error: formState.errors.subject,
        children: [/*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: "",
          selected: true,
          children: t("contact_us.consultour.asunto.select")
        }, "0"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.agency"),
          children: t("contact_us.consultour.asunto.agency")
        }, "1"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.web"),
          children: t("contact_us.consultour.asunto.web")
        }, "2"), /*#__PURE__*/jsx_runtime_.jsx(SubjectItem, {
          value: t("contact_us.consultour.asunto.other"),
          children: t("contact_us.consultour.asunto.other")
        }, "3")]
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Label, {
        children: ["   ", t("contact_us.consultour.consult")]
      }), /*#__PURE__*/jsx_runtime_.jsx(TextControl, other_subject_objectSpread({
        rows: 5
      }, register("query", {
        required: `${t("contact_us.consultour_error.query")}`
      }))), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Error, {
        children: errors.query && /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: errors.query.message
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_react_hook_form_.Controller, {
      control: control,
      name: "policy",
      rules: {
        required: `${t("contact_us.consultour_error.policy")}`
      },
      render: ({
        field,
        formState
      }) => /*#__PURE__*/jsx_runtime_.jsx(CheckPolicy, {
        value: field.value,
        setValue: field.onChange,
        error: formState.errors.policy
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Footer, {
      children: [response && /*#__PURE__*/(0,jsx_runtime_.jsxs)(Response, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Status, {
          children: response === null || response === void 0 ? void 0 : response.status
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_Message, {
          children: response === null || response === void 0 ? void 0 : response.message
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(consult_trip_styled_Back, {
        onClick: onCancel,
        children: t("contact_us.back")
      }), /*#__PURE__*/jsx_runtime_.jsx(styled_Send, {
        disabled: !isDirty || !isValid || loading,
        onClick: handleSubmit(onBooking),
        children: loading ? /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: "Enviando..."
        }) : /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: "Enviar"
        })
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/header/contact-us/styled.tsx
//# Styled component

const contact_us_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-0"
})(["background:", ";width:100%;height:100%;top:0;right:0;bottom:0;left:0;position:fixed;z-index:10104;"], ({
  theme
}) => theme.colors.white.rgba.W255(0.65));
const Center = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-1"
})(["max-width:100%;height:100%;position:fixed;top:50%;left:50%;bottom:auto;right:auto;transform:translate(-50%,-50%);z-index:10104;height:auto;", "{height:auto;}"], ({
  theme: {
    media
  }
}) => media.to(0, 768, true));
const contact_us_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-2"
})(["display:block;width:", ";max-width:100%;padding:", ";border-radius:", ";overflow:auto;background:", ";box-shadow:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(850), ({
  theme: {
    toRem
  }
}) => toRem(34), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem,
    colors
  }
}) => `
  0 ${toRem(11)} ${toRem(15)} ${toRem(-7)} ${colors.black.rgba.BK000(0.2)},
    0 ${toRem(24)} ${toRem(38)} ${toRem(3)} ${colors.black.rgba.BK000(0.14)},
    0 ${toRem(9)} ${toRem(46)} ${toRem(8)} ${colors.black.rgba.BK000(0.12)};
  `);
const Header = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-3"
})(["position:relative;"]);
const styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-7n3ui0-4"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;border-bottom-width:", ";border-bottom-color:", ";border-bottom-style:solid;padding-bottom:", ";margin-top:", ";margin-bottom:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK555, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYDDD, ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(-5), ({
  theme: {
    toRem
  }
}) => toRem(20));
const Close = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-7n3ui0-5"
})(["display:flex;position:absolute;cursor:pointer;top:", ";right:", ";color:", ";font-size:", ";line-height:120%;"], ({
  theme: {
    toRem
  }
}) => toRem(-10), ({
  theme: {
    toRem
  }
}) => toRem(-15), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK555, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Contact = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-6"
})(["width:100%;display:flex;justify-content:space-between;margin-left:", ";flex-wrap:wrap;", "{flex-wrap:nowrap;}"], ({
  theme: {
    toRem
  }
}) => toRem(-8), ({
  theme: {
    media
  }
}) => media.to(0, 768, true));
const Topic = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-7"
})(["display:flex;flex-direction:column;align-items:center;row-gap:", ";width:100%;padding:", ";margin-left:", ";margin-right:", ";border-width:", ";border-color:", ";border-style:solid;border-radius:", ";background:", ";cursor:pointer;transition:background 0.1s ease-in-out;&:hover{background:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(15), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYDDD, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  active,
  theme: {
    colors
  }
}) => active && colors.gray.rgb.GRYF2F3F5, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYF2F3F5);
const Icon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-7n3ui0-8"
})(["display:flex;color:", ";font-size:", ";line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK555, ({
  theme: {
    toRem
  }
}) => toRem(65));
const contact_us_styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-7n3ui0-9"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
;// CONCATENATED MODULE: ./components/header/contact-us/index.tsx
//# React
 //# Rooks

 //# React modal


external_react_modal_default().setAppElement("#__next"); //# Icons




 //# Components



 //# Styles components


 //# Types props



//# Component => contact us
const ContactUs = ({
  show,
  setShow
}) => {
  //# States
  const {
    0: index,
    1: setIndex
  } = (0,external_react_.useState)(-1);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# Data

  const consult = [{
    icon: /*#__PURE__*/jsx_runtime_.jsx(bs_index_esm/* BsBookmarkCheck */.OkQ, {}),
    text: t("contact_us.query"),
    children: /*#__PURE__*/jsx_runtime_.jsx(CheckBooking, {})
  }, {
    icon: /*#__PURE__*/jsx_runtime_.jsx(ti_index_esm/* TiThLargeOutline */.Y83, {}),
    text: t("contact_us.tourquery"),
    children: /*#__PURE__*/jsx_runtime_.jsx(ConsultTrip, {})
  }, {
    icon: /*#__PURE__*/jsx_runtime_.jsx(ai_index_esm/* AiOutlineComment */.RG4, {}),
    text: t("contact_us.others"),
    children: /*#__PURE__*/jsx_runtime_.jsx(OtherSubject, {})
  }]; //# Hooks

  const ref = external_react_cool_onclickoutside_default()(() => {
    setShow(false);
    onCancel();
    document.body.removeAttribute("style");
  }); //# methods

  const onClose = e => {
    e.stopPropagation();
    setShow(false);
    onCancel();
    document.body.removeAttribute("style");
  };

  const onIndex = i => setIndex(i);

  const onCancel = () => setIndex(-1);

  const children = index !== -1 && consult[index].children;
  const child = children;
  return /*#__PURE__*/jsx_runtime_.jsx((external_react_modal_default()), {
    onAfterOpen: () => {
      document.body.style.overflow = "hidden";
    },
    overlayElement: () => /*#__PURE__*/jsx_runtime_.jsx(contact_us_styled_Container, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Center, {
        ref: ref,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(contact_us_styled_Content, {
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Header, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Title, {
              children: t("contact_us.title")
            }), /*#__PURE__*/jsx_runtime_.jsx(Close, {
              onClick: onClose,
              children: /*#__PURE__*/jsx_runtime_.jsx(io5_index_esm/* IoCloseOutline */.IOM, {})
            })]
          }), index !== -1 ? /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
            children: /*#__PURE__*/(0,external_react_.cloneElement)(child, {
              onCancel
            })
          }) : /*#__PURE__*/jsx_runtime_.jsx(Contact, {
            children: consult.map(({
              icon,
              text
            }, e) => {
              const active = index === e;
              return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Topic, {
                active: active,
                onClick: () => onIndex(e),
                children: [/*#__PURE__*/jsx_runtime_.jsx(Icon, {
                  children: icon
                }), /*#__PURE__*/jsx_runtime_.jsx(contact_us_styled_Text, {
                  children: text
                })]
              }, e);
            })
          })]
        })
      })
    }),
    isOpen: show
  });
};
;// CONCATENATED MODULE: ./components/header/index.tsx
//# React
 //# Next



 //# Media

 //# Interfaces

//# Apollo client

 //# Components





 //# Translate

 //# Styles components





//# Component => header
const header_Header = ({}) => {
  var _router$query;

  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common");
  const {
    0: menu,
    1: setMenu
  } = (0,external_react_.useState)(false);
  const {
    0: contact,
    1: setContact
  } = (0,external_react_.useState)(false);
  const {
    0: destinations,
    1: setDestinations
  } = (0,external_react_.useState)([]);
  const {
    0: attracctions,
    1: setAttraction
  } = (0,external_react_.useState)([]);
  const match = (0,useMedia/* useMedia */.G)("(max-width: 1024px)");
  const {
    languages
  } = (0,external_react_.useContext)(useContext/* HeaderContext */.pI);

  const onContact = () => setContact(true);

  const onMenu = () => setMenu(true);

  const router = (0,router_.useRouter)();
  const locale = (_router$query = router.query) === null || _router$query === void 0 ? void 0 : _router$query.locale;
  const {
    data: getDesti
  } = (0,client_.useQuery)(querys/* GET_DESTINATIONS */.OI, {
    variables: {
      slug: locale || "en"
    }
  });
  const {
    data: getAttactions
  } = (0,client_.useQuery)(querys/* GET_ATTRACTTION_HEADER */.f5, {
    variables: {
      slug: locale || "en"
    }
  }); //# Prerender

  (0,external_react_.useEffect)(() => {
    const getDestinations = getDesti && getDesti.getDestinations.data;
    const getAtracctions = getAttactions && getAttactions.getAllAtractionsHeader.data;

    if (getDestinations && getAtracctions) {
      setDestinations(getDestinations);
      setAttraction(getAtracctions);
    } else {
      setDestinations([]);
      setAttraction([]);
    }
  }, [getDesti, getAttactions]);

  const changeLange = () => {
    router.push(`/${locale || "en"}`);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Container, {
    children: [match && /*#__PURE__*/jsx_runtime_.jsx(Navbar, {
      destinations: destinations,
      travelStyles: attracctions,
      active: menu,
      setMenu: setMenu
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Auto, {
      children: [match && /*#__PURE__*/(0,jsx_runtime_.jsxs)(Burger, {
        onClick: onMenu,
        children: [/*#__PURE__*/jsx_runtime_.jsx(King, {}), /*#__PURE__*/jsx_runtime_.jsx(King, {}), /*#__PURE__*/jsx_runtime_.jsx(King, {})]
      }), /*#__PURE__*/jsx_runtime_.jsx(Logo, {
        onClick: changeLange,
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          width: 140,
          height: 35,
          layout: "fixed",
          src: "/logo.jpeg",
          alt: "Conde Travel E.I.R.L"
        })
      }), !match && /*#__PURE__*/(0,jsx_runtime_.jsxs)(Menu, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(MenuItem, {
          title: t("header.travelstyles"),
          children: /*#__PURE__*/jsx_runtime_.jsx(MenuNavTravelStyles, {
            travelStyles: attracctions
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(MenuItem, {
          title: t("header.destinations"),
          children: /*#__PURE__*/jsx_runtime_.jsx(MenuNavDestinations, {
            destinations: destinations
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(MenuItem, {
          relative: true,
          title: t("header.laguages"),
          children: /*#__PURE__*/jsx_runtime_.jsx(LanguageMenu, {
            children: languages === null || languages === void 0 ? void 0 : languages.map(({
              cod,
              slug,
              url
            }, index) => {
              return /*#__PURE__*/jsx_runtime_.jsx(routes/* Link */.rU, {
                route: url,
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(SectionDeal, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
                      src: `https://flagcdn.com/32x24/${cod}.png`,
                      alt: slug,
                      width: 23,
                      height: 18,
                      layout: "fixed"
                    }), " \xA0", t(`header.${slug}`)]
                  })
                })
              }, index);
            })
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(styled_Item, {
          border: true,
          onClick: onContact,
          children: /*#__PURE__*/jsx_runtime_.jsx(Text, {
            children: t("header.contact_us")
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(ContactUs, {
        show: contact,
        setShow: setContact
      })]
    })]
  });
};
// EXTERNAL MODULE: ./node_modules/react-icons/ri/index.esm.js
var ri_index_esm = __webpack_require__(9352);
// EXTERNAL MODULE: ./node_modules/react-icons/gr/index.esm.js
var gr_index_esm = __webpack_require__(5155);
// EXTERNAL MODULE: ./node_modules/react-icons/fa/index.esm.js
var fa_index_esm = __webpack_require__(9583);
;// CONCATENATED MODULE: ./components/footer/newletter/styled.tsx
//# Styled components

const newletter_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-kukfh7-0"
})(["position:relative;z-index:1;display:flex;flex-direction:column;align-items:center;row-gap:", ";background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY9facba, ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-kukfh7-1"
})(["display:flex;flex-direction:column;row-gap:", ";align-items:center;"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const newletter_styled_Title = /*#__PURE__*/external_styled_components_default().h3.withConfig({
  componentId: "sc-kukfh7-2"
})(["color:", ";font-size:", ";line-height:120%;font-weight:600;"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(24));
const newletter_styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-kukfh7-3"
})(["color:", ";font-size:", ";line-height:120%;font-weight:400;"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Url = /*#__PURE__*/(/* unused pure expression or super */ null && (styled(newletter_styled_Text).withConfig({
  componentId: "sc-kukfh7-4"
})(["display:inline-block;color:", ";font-weight:500;margin-left:", ";margin-right:", ";cursor:pointer;&:hover{text-decoration:underline;}"], ({
  theme: {
    colors
  }
}) => colors.yellow.rgb.YWffe958, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(4))));
const GroupForm = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-kukfh7-5"
})(["display:flex;flex-wrap:wrap;align-items:center;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(20));
const newletter_styled_FormControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-kukfh7-6"
})(["position:relative;width:", ";display:flex;align-items:stretch;justify-content:flex-start;background:", ";border-radius:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(300), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(8));
const Mail = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-kukfh7-7"
})(["display:flex;align-items:center;justify-content:center;color:", ";font-size:", ";padding-left:", ";padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY81829c, ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const newletter_styled_InputControl = /*#__PURE__*/external_styled_components_default().input.withConfig({
  componentId: "sc-kukfh7-8"
})(["width:100%;height:", ";background:", ";padding-left:", ";padding-right:", ";padding-top:", ";padding-bottom:", ";border-radius:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(6), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(8));
const newletter_styled_Error = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-kukfh7-9"
})(["position:absolute;top:100%;left:0;color:", ";font-size:", ";line-height:120%;font-weight:400;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(12));
const Submit = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-kukfh7-10"
})(["width:", ";height:", ";background:", ";color:", ";font-size:", ";line-height:120%;font-weight:600;padding-left:", ";padding-right:", ";padding-top:", ";padding-bottom:", ";border-radius:", ";cursor:pointer;transition:opacity 0.1s linear;", ""], ({
  theme: {
    toRem
  }
}) => toRem(150), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  isDisabled
}) => isDisabled ? (0,external_styled_components_.css)([" &:not(:disabled):hover{opacity:0.85;}"]) : (0,external_styled_components_.css)(["&:disabled{cursor:not-allowed;background:", ";color:", ";}"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYF2F3F5));
const newletter_styled_Send = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-kukfh7-11"
})(["position:absolute;right:", ";top:50%;transform:translateY(-50%);z-index:-1;display:flex;align-items:center;justify-content:center;width:", ";height:", ";border-radius:50%;background:", ";font-size:", ";color:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(200), ({
  theme: {
    toRem
  }
}) => toRem(200), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY9aa7b5, ({
  theme: {
    toRem
  }
}) => toRem(100), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYa6b2bf);
;// CONCATENATED MODULE: ./gql/mutation/mutation.ts

const NEW_NEWLETTER = client_.gql`
mutation newNewsletter($Email: String, $Name: String, $Language: String){
    newNewsletter(Email:$Email,Name:$Name,Language:$Language){
      message
    }
  }
`;
;// CONCATENATED MODULE: ./components/footer/newletter/index.tsx
function newletter_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function newletter_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { newletter_ownKeys(Object(source), true).forEach(function (key) { newletter_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { newletter_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function newletter_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# React
 //# React hooks form

 //# React icons



 //# Styles components


 //# Types props



//# Component => newletter
const NewLetter = props => {
  var _errors$email, _errors$name;

  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common"); //# State

  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(false); //# Form hooks

  const {
    handleSubmit,
    register,
    reset,
    formState: {
      errors,
      isDirty,
      isValid
    }
  } = (0,external_react_hook_form_.useForm)({
    mode: "onChange",
    defaultValues: {
      email: "",
      name: ""
    }
  }); //# Methods

  const onSubmit = async data => {
    setLoading(true);
    NEW_NEWLETTER;
    reset({
      email: "",
      name: ""
    });
    setLoading(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(newletter_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Head, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Title, {
        children: t("newsletter.title")
      }), /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Text, {
        children: t("newsletter.info")
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(GroupForm, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(newletter_styled_FormControl, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Mail, {
          children: /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineMail */.Zuw, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_InputControl, newletter_objectSpread(newletter_objectSpread({}, register("email", {
          required: "Ingrese un correo correcto."
        })), {}, {
          placeholder: t("newsletter.youremail")
        })), errors && /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Error, {
          children: (_errors$email = errors.email) === null || _errors$email === void 0 ? void 0 : _errors$email.message
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(newletter_styled_FormControl, {
        style: {
          maxWidth: "200px"
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(Mail, {
          children: /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineUser */.HHH, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_InputControl, newletter_objectSpread(newletter_objectSpread({}, register("name", {
          required: "Ingrese Nombre"
        })), {}, {
          placeholder: t("newsletter.yourname")
        })), errors && /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Error, {
          children: (_errors$name = errors.name) === null || _errors$name === void 0 ? void 0 : _errors$name.message
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(Submit, {
        onClick: handleSubmit(onSubmit),
        disabled: !isDirty || !isValid,
        children: loading ? /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: "Subscribiendo..."
        }) : /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: t("newsletter.subscribe")
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Text, {
      children: t("newsletter.terms")
    }), /*#__PURE__*/jsx_runtime_.jsx(newletter_styled_Send, {
      children: /*#__PURE__*/jsx_runtime_.jsx(ri_index_esm/* RiMailSendLine */.qZi, {})
    })]
  });
};
;// CONCATENATED MODULE: ./components/footer/styled.tsx
//# Styled components

const footer_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-0"
})(["width:100%;display:flex;flex-direction:column;background:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF);
const footer_styled_Title = /*#__PURE__*/external_styled_components_default().h3.withConfig({
  componentId: "sc-2jwhm8-1"
})(["color:", ";font-size:", ";line-height:120%;font-weight:600;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(15));
const Accredited = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-2"
})(["display:flex;flex-direction:column;align-items:center;row-gap:", ";background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(100), ({
  theme: {
    toRem
  }
}) => toRem(60), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Accredits = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-3"
})(["width:", ";max-width:100%;margin:auto;display:flex;flex-wrap:wrap;align-items:center;justify-content:center;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(840), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(6));
const Company = /*#__PURE__*/external_styled_components_default().img.withConfig({
  componentId: "sc-2jwhm8-4"
})(["display:block;width:", ";height:", ";object-fit:contain;"], ({
  theme: {
    toRem
  }
}) => toRem(160), ({
  theme: {
    toRem
  }
}) => toRem(80));
const Destination = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-5"
})(["display:flex;align-items:center;justify-content:center;background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYf5f6f9, ({
  theme: {
    toRem
  }
}) => toRem(45), ({
  theme: {
    toRem
  }
}) => toRem(45), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Center = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-6"
})(["width:", ";max-width:100%;display:flex;flex-wrap:wrap;align-items:flex-start;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(1140), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(24));
const footer_styled_Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-7"
})(["display:flex;flex-direction:column;row-gap:", ";margin:auto;"], ({
  theme: {
    toRem
  }
}) => toRem(10));
const styled_Logo = /*#__PURE__*/external_styled_components_default().img.withConfig({
  componentId: "sc-2jwhm8-8"
})(["display:block;width:", ";object-fit:cover;filter:grayscale(1);"], ({
  theme: {
    toRem
  }
}) => toRem(180));
const Name = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-9"
})(["color:", ";font-size:", ";line-height:120%;font-weight:600;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Menu = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-10"
})(["display:flex;flex-direction:column;align-items:flex-start;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const footer_styled_Item = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-11"
})(["color:", ";font-size:", ";line-height:120%;font-weight:400;cursor:pointer;&:hover{text-decoration:underline;}"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY81829c, ({
  theme: {
    toRem
  }
}) => toRem(15));
const Payment = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-12"
})(["display:flex;flex-direction:column;align-items:center;row-gap:", ";background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(36), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Cards = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-13"
})(["display:flex;flex-wrap:wrap;align-items:center;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(6));
const Card = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-14"
})(["display:flex;align-items:center;justify-content:center;width:", ";height:", ";background:", ";border-radius:", ";box-shadow:", ";font-size:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(43), ({
  theme: {
    toRem
  }
}) => toRem(32), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem,
    colors
  }
}) => `0 ${toRem(2)} ${toRem(4)} ${colors.black.rgba.BK000(0.1)}`, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Created = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-15"
})(["display:flex;align-items:center;justify-content:center;background:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    colors
  }
}) => colors.blue.rgb.BL3b556a, ({
  theme: {
    toRem
  }
}) => toRem(36), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Auto = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-16"
})(["width:", ";max-width:100%;display:flex;flex-wrap:wrap;align-items:flex-start;justify-content:space-between;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(1140), ({
  theme: {
    toRem
  }
}) => toRem(36), ({
  theme: {
    toRem
  }
}) => toRem(20));
const Author = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-17"
})(["width:", ";max-width:100%;display:flex;flex-direction:column;align-items:center;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(300), ({
  theme: {
    toRem
  }
}) => toRem(10));
const Direction = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-18"
})(["color:", ";font-size:", ";line-height:120%;font-weight:400;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Powered = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-19"
})(["color:", ";font-size:", ";line-height:120%;font-weight:400;text-align:left;"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(14));
const SocialMedia = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2jwhm8-20"
})(["display:flex;flex-direction:row;align-items:center;column-gap:", ";color:", ";font-size:", ";line-height:120%;font-weight:400;"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Social = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-2jwhm8-21"
})(["a{text-decoration:none;color:inherit;}display:flex;color:", ";font-size:", ";cursor:pointer;transition:opacity 0.1s cubic-bezier(0.075,0.82,0.165,1);&:hover{opacity:0.6;}"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(20));
;// CONCATENATED MODULE: ./components/footer/index.tsx
//# React icons



 //# Component

 //# Styles components



 //# Types props



//# Component => footer
const footer_Footer = props => {
  const router = (0,router_.useRouter)();
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("common");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(NewLetter, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Accredited, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(footer_styled_Title, {
        children: t("accredited")
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Accredits, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Company, {
          src: "https://1.bp.blogspot.com/-9ZDiOG9uJl4/XtfndvPfzuI/AAAAAAAAgVk/VURX1LGtf9g6NNR4ZMOA-Z6J9SdjdVd0gCLcBGAsYHQ/s1600/CM4Q7WA5IFAK5DAEBZ7DT2ZIMQ.png",
          title: "Ministerio de Cultura",
          alt: "Ministerio de Cultura"
        }), /*#__PURE__*/jsx_runtime_.jsx(Company, {
          src: "https://pbs.twimg.com/profile_images/378800000029930734/fbff30515139b0abd1ef4445b55438d3_400x400.jpeg",
          title: "Dircetur",
          alt: "Dircetur"
        }), /*#__PURE__*/jsx_runtime_.jsx(Company, {
          src: "https://andeanlodges.com/wp-content/uploads/2020/07/footer_6_a.png",
          title: "Caltur",
          alt: "Caltur"
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Destination, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Center, {
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Group, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Logo, {
            src: "https://images.conde.travel/logo.jpeg",
            title: "Conde Travel",
            alt: "Conde Travel"
          }), /*#__PURE__*/jsx_runtime_.jsx(styled_Menu, {
            children: /*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              children: "Sitemap"
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Group, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Name, {
            children: t("footer.bestdestinations.title")
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Menu, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              onClick: () => {
                router.push(t("footer.bestdestinations.machupicchu.link"));
              },
              children: t("footer.bestdestinations.machupicchu.name")
            }), /*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              onClick: () => {
                router.push(t("footer.bestdestinations.incatrail.link"));
              },
              children: t("footer.bestdestinations.incatrail.name")
            }), /*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              onClick: () => {
                router.push(t("footer.bestdestinations.salkantay.link"));
              },
              children: t("footer.bestdestinations.salkantay.name")
            })]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Group, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Name, {
            children: t("footer.company.title")
          }), /*#__PURE__*/jsx_runtime_.jsx(styled_Menu, {
            children: /*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              children: t("footer.company.about")
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Group, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Name, {
            children: t("footer.issues.title")
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Menu, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              onClick: () => {
                router.replace(t("footer.issues.terms.link"));
              },
              children: t("footer.issues.terms.name")
            }), /*#__PURE__*/jsx_runtime_.jsx(footer_styled_Item, {
              onClick: () => {
                router.replace(t("footer.issues.privacy.link"));
              },
              children: t("footer.issues.privacy.name")
            })]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Group, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Name, {
            children: t("footer.contact.title")
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Menu, {
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Item, {
              children: [" ", /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlinePhone */.PES, {}), " +51 984 603 305"]
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(footer_styled_Item, {
              children: [" ", /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineMail */.Zuw, {}), " info@condetraveladventures.con"]
            })]
          })]
        })]
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Payment, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(footer_styled_Title, {
        children: t("footer.payment")
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Cards, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(ri_index_esm/* RiMastercardLine */.Qx0, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(gr_index_esm/* GrAmex */.ZOg, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(ri_index_esm/* RiVisaLine */.yT$, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaCcDinersClub */.xKW, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaCcDiscover */.kLK, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaPaypal */.kD0, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(Card, {
          children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaStripe */.g6c, {})
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Created, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Auto, {
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Author, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Direction, {
            children: "Av. Ayahuayco Mza. O Lote 5 Cusco,Peru 2022 Conde Travel"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Powered, {
            children: [t("footer.power"), " Nexeti Sof"]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(SocialMedia, {
          children: [t("footer.follow"), /*#__PURE__*/jsx_runtime_.jsx(Social, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
              href: "https://www.facebook.com/CondeTravel",
              target: "_blank",
              rel: "noreferrer",
              children: [" ", /*#__PURE__*/jsx_runtime_.jsx(gr_index_esm/* GrFacebook */.kKz, {})]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(Social, {
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "https://www.instagram.com/condetravel/",
              target: "_blank",
              rel: "noreferrer",
              children: /*#__PURE__*/jsx_runtime_.jsx(gr_index_esm/* GrInstagram */.Z8w, {})
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(Social, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
              href: "https://twitter.com/condetravel",
              target: "_blank",
              rel: "noreferrer",
              children: ["  ", /*#__PURE__*/jsx_runtime_.jsx(gr_index_esm/* GrTwitter */.Xai, {})]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(Social, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
              href: "https://www.youtube.com/c/CondeTravelCusco",
              target: "_blank",
              rel: "noreferrer",
              children: [" ", /*#__PURE__*/jsx_runtime_.jsx(gr_index_esm/* GrYoutube */.FIe, {})]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(Social, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
              href: "https://www.tripadvisor.com.pe/Attraction_Review-g294314-d6215736-Reviews-Conde_Travel-Cusco_Cusco_Region.html",
              target: "_blank",
              rel: "noreferrer",
              children: [" ", /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaTripadvisor */.LmS, {})]
            })
          })]
        })]
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/layout/index.tsx





const LayoutItem = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-vpm7qy-0"
})(["background:white;"]);

const Layout = ({
  children
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(LayoutItem, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(header_Header, {}), /*#__PURE__*/jsx_runtime_.jsx("main", {
      children: children
    }), /*#__PURE__*/jsx_runtime_.jsx(footer_Footer, {})]
  });
};

/* harmony default export */ const layout = (Layout);

/***/ }),

/***/ 5550:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OI": () => (/* binding */ GET_DESTINATIONS),
/* harmony export */   "f5": () => (/* binding */ GET_ATTRACTTION_HEADER),
/* harmony export */   "Yk": () => (/* binding */ GET_HOME),
/* harmony export */   "vN": () => (/* binding */ SEARCH_HOME)
/* harmony export */ });
/* unused harmony export GET_STYLES_SLUG */
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_DESTINATIONS = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getDestinations($slug: String){
    getDestinations(Slug:$slug) {
      status
      data {
        Name
        Url
        Citys {
          Name
          Url
        }
      }
    }
  }
`;
const GET_STYLES_SLUG = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getStylesSlug($slug:String){
    getStylesSlug(Slug: $slug) {
      status
      data {
        Name
        Url
      }
    }
  }
`;
const GET_ATTRACTTION_HEADER = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getAllAtractionsHeader($slug:String){
    getAllAtractionsHeader(Slug: $slug) {
      status
      data{
        _ID
        Name
        Url
        Image
      }
    }
  }
`;
const GET_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
query getLanguegeHole($slug: String) {
  getLanguageHome(Slug: $slug) {
    status
    data {
      TitleGoogle
      DescriptionGoogle
      Title
      Image
      Slogan
    }
  }
}
`;
const SEARCH_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getSearchHome($slug:String,$text:String){
    getSearchHome(Slug: $slug, Text: $text){
      status
      data{
        name
        url
        type
      }
    }
  }
`;

/***/ }),

/***/ 960:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Q8": () => (/* binding */ HomeContext),
/* harmony export */   "QS": () => (/* binding */ TripContext),
/* harmony export */   "pI": () => (/* binding */ HeaderContext)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const InitialData = {
  Seo: {
    title: "Peru tour ",
    description: "Desc Peru tour",
    canonical: "",
    image: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
  },
  Title: "Peru tour ",
  Gallery: [{
    name: "title text",
    alt: "alt text",
    url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
  }, {
    name: "title 1",
    alt: "alt 1 ",
    url: "https://andeanrajuexpeditions.com/wp-content/uploads/2020/01/SALKANTAY-TREK-1024X640-1024x530.jpg"
  }],
  Valoration: {
    Cant: 14,
    Ratio: 4.1
  },
  Location: "Cusco, Peru",
  Details: {
    max: 15,
    min: 20,
    duration: {
      Type: "D",
      Count: 1
    },
    location: "Cusco, Peru",
    places: "",
    languages: "Espanol, English"
  },
  Intro: {
    introduction: "xd",
    highlights: '["xd","xd"]'
  },
  Places: [{
    name: "Machu Picchu",
    image: {
      name: "title text",
      alt: "alt text",
      url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
    }
  }],
  Include: {
    Include: [{
      Icon: "null",
      Title: "No se Aun",
      Content: '["xd","otra"]'
    }],
    Recommendations: [{
      Icon: "null",
      Title: "test",
      Content: '["test","test"]'
    }],
    NoInclude: '["xs","test"]'
  },
  Itinerary: [{
    gallery: [{
      name: "title text",
      alt: "alt text",
      url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
    }],
    day: "1",
    title: "title",
    content: "this content is html conten all content is html "
  }, {
    gallery: [{
      name: "title text",
      alt: "alt text",
      url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
    }],
    day: "3",
    title: "Test",
    content: "test"
  }, {
    gallery: [{
      name: "title text",
      alt: "alt text",
      url: "https://www.conde.travel/wp-content/uploads/2019/10/cusco-machu-picchu-thumbail-600x356.jpg"
    }],
    day: "2",
    title: "xD",
    content: "<p>We will pick you up from your hotel around 4:30 am – 5:30 am and head south through the town of Urcos to the district of Checacupe (2 hours by bus) and then follow the road to Pitumarca where we will have breakfast.</p>\n<p>Then we continue along the road through hamlets until we reach the community of Hanchipata – Quesiuno (1 hour and 20 minutes by bus), the ideal starting point for the trek.</p>\n<p>We will see incredible mountains like Ausangate, beautiful blue lagoons and many other natural sites.<br>\nThe hike to Vinicunca (Rainbow Mountain) will take approximately 3 hours. At this peak we will savor the view for about an hour taking pictures of the beautiful scenery before descending the trail and returning to our bus.<br>\nWe will return to Pitumarca for lunch and then head back to Cusco.</p>"
  }],
  Aditional: "",
  Faq: [{
    ask: "test ask ",
    response: "test repply"
  }],
  CodWetravel: 123412,
  Price: {
    Amount: 20.2,
    Discount: 20
  }
};
const HomeContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_0__.createContext)({
  cities: [],
  trips: [],
  styles: [],
  atractions: []
});
const TripContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_0__.createContext)({
  info: InitialData
});
const HeaderContext = /*#__PURE__*/(0,react__WEBPACK_IMPORTED_MODULE_0__.createContext)({
  languages: []
});

/***/ }),

/***/ 4403:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "G": () => (/* binding */ useMedia)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
//# React

const useMedia = query => {
  //# States
  const {
    0: matches,
    1: setMatches
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false); //# Prerender

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    //# Media query from window
    const media = window.matchMedia(query);

    if (media.matches !== matches) {
      setMatches(media.matches);
    } //# Event listenes from match media query


    const listener = () => {
      setMatches(media.matches);
    }; //# Add event media query


    media.addEventListener("change", listener); //# Remove the event media query

    return () => media.removeEventListener("change", listener);
  }, [matches, query]); //# return the matches: true | false

  return matches;
};

/***/ }),

/***/ 1673:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ useRating)
/* harmony export */ });
/* harmony import */ var react_icons_bs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3750);
//# React icons

//# Hook => useRating
const useRating = (quantity, reviews = 1) => {
  const rating = {
    count: quantity || 2.5,
    all: 0,
    half: 0
  };
  const decimal = rating.count * 10 % 10 / 10;
  const review = decimal > 0.5 ? Math.round(quantity) : Math.floor(quantity);

  if (decimal < 0.6 && decimal > 0.0) {
    rating.half = 1;
  } else if (decimal > 0.5) {
    rating.all = review;
  }

  const missing = 5 - (review + rating.half);
  const quality = [];
  [...Array(review)].map(() => quality.push(react_icons_bs__WEBPACK_IMPORTED_MODULE_0__/* .BsStarFill */ .kRm));
  [...Array(rating.half)].map(() => quality.push(react_icons_bs__WEBPACK_IMPORTED_MODULE_0__/* .BsStarHalf */ .fXH));
  [...Array(missing)].map(() => quality.push(react_icons_bs__WEBPACK_IMPORTED_MODULE_0__/* .BsStar */ .RrZ));
  return [quality, reviews];
};

/***/ }),

/***/ 4122:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "K3": () => (/* binding */ ValidateSeparatePrice),
/* harmony export */   "LN": () => (/* binding */ ValidateDiscount),
/* harmony export */   "JP": () => (/* binding */ ValidateNormalDiscount)
/* harmony export */ });
const ValidateSeparatePrice = ({
  price
}) => {
  let newprice = price.toString().split(".");
  return `${newprice[0]},${typeof newprice[1] == "undefined" ? "00" : newprice[1]}`;
};
const ValidateDiscount = ({
  prices,
  discount
}) => {
  let price = Number((prices - prices * discount / 100).toFixed(0));
  return ValidateSeparatePrice({
    price
  });
};
const ValidateNormalDiscount = ({
  prices,
  discount
}) => {
  let price = Number((prices - prices * discount / 100).toFixed(0));
  return price;
};

/***/ })

};
;
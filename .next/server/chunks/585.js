"use strict";
exports.id = 585;
exports.ids = [585];
exports.modules = {

/***/ 1809:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "a": () => (/* binding */ ScrollSnap)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./hooks/useMedia.tsx
var useMedia = __webpack_require__(4403);
// EXTERNAL MODULE: ./node_modules/react-icons/fi/index.esm.js
var index_esm = __webpack_require__(6893);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(9914);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/scroll-snap/styled.tsx
//# Styled components

const Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-0"
})(["position:relative;width:100%;display:flex;flex-direction:column;"]);
const GroupRow = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-1"
})(["width:auto;display:flex;align-items:center;justify-content:space-between;column-gap:", ";margin-left:auto;"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const Steps = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-2"
})(["display:flex;align-items:center;justify-content:space-between;column-gap:", ";margin-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(10));
const Step = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-3"
})(["color:", ";font-size:", ";line-height:120%;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const Space = /*#__PURE__*/external_styled_components_default()(Step).withConfig({
  componentId: "sc-3jkyt5-4"
})(["color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK555, ({
  theme: {
    toRem
  }
}) => toRem(18));
const Row = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-3jkyt5-5"
})(["display:flex;justify-content:center;align-items:center;min-width:", ";height:", ";border-radius:", ";background:", ";box-shadow:", ";transition:background 0.1s linear,box-shadow 0.2s linear;color:", ";font-size:", ";cursor:pointer;&:hover:not(:disabled){box-shadow:", ";}&:disabled{color:", ";box-shadow:none;cursor:not-allowed;}"], ({
  theme: {
    toRem
  }
}) => toRem(35), ({
  theme: {
    toRem
  }
}) => toRem(35), ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem,
    colors
  }
}) => `0 ${toRem(1)} ${toRem(2)} 0 ${colors.black.rgba.BK000(0.25)}`, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(23), ({
  theme: {
    toRem,
    colors
  }
}) => `0 ${toRem(1)} ${toRem(2)} 0 ${colors.black.rgba.BK000(0.35)}`, ({
  theme
}) => theme.colors.gray.rgb.GRYCCC);
const Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-6"
})(["display:block;margin-left:calc(-1 * ", ");margin-right:calc(-1 * ", ");", "{margin-left:calc(-1 * ", ");margin-right:calc(-1 * ", ");}"], ({
  theme: {
    toRem
  }
}) => toRem(0), ({
  theme: {
    toRem
  }
}) => toRem(0), ({
  theme: {
    media
  }
}) => media.to(1184), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Scroll = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-3jkyt5-7"
})(["display:block;overflow-x:scroll;scroll-behavior:smooth;scroll-snap-type:x mandatory;-webkit-overflow-scrolling:touch;padding:", ";margin:calc(-1 * ", ");::-webkit-scrollbar{display:none;}"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(4));
const Grid = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-3jkyt5-8"
})(["display:flex;list-style:none;&::after{display:block;content:\"\";width:", ";flex-grow:0;flex-shrink:0;}"], ({
  theme: {
    toRem
  }
}) => toRem(24));
const Snap = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-3jkyt5-9"
})(["width:calc( ", " );scroll-snap-align:start;scroll-margin-left:calc( ", " );scroll-margin-right:calc( ", " );&:not(:last-of-type){margin-right:", ";}&:first-of-type{margin-left:", ";}flex-grow:0;flex-shrink:0;"], ({
  firstMargin,
  space,
  size,
  theme: {
    toRem
  }
}) => `(100% + ${toRem(space)} - 2 * ${toRem(firstMargin)}) * ${size} - ${toRem(space)}`, ({
  firstMargin,
  theme: {
    toRem
  }
}) => `${toRem(firstMargin)} + ${toRem(4)}`, ({
  firstMargin,
  theme: {
    toRem
  }
}) => `${toRem(firstMargin)} + ${toRem(4)}`, ({
  space,
  theme: {
    toRem
  }
}) => toRem(space), ({
  firstMargin,
  theme: {
    toRem
  }
}) => toRem(firstMargin));
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/scroll-snap/index.tsx
//# React
 //# Hooks

 //# Interfaces

//# React icons
 //# Styles components

 //# Types props



//# Component => scroll snap
const ScrollSnap = ({
  children,
  defaultSize,
  responsive
}) => {
  //# Refs
  const scrollRef = (0,external_react_.useRef)(null);
  const slideRefs = (0,external_react_.useRef)([]);
  const hideArrowThreshold = 5; //# States

  const {
    0: isLeft,
    1: setIsLeft
  } = (0,external_react_.useState)(false);
  const {
    0: isRight,
    1: setIsRight
  } = (0,external_react_.useState)(false);
  const {
    0: isScrolling,
    1: setIsScrolling
  } = (0,external_react_.useState)(false);
  const {
    0: step,
    1: setStep
  } = (0,external_react_.useState)(0);
  const {
    0: steps,
    1: setSteps
  } = (0,external_react_.useState)(0); //# Media

  const media = {
    space: 16,
    firstMargin: 0,
    size: defaultSize || 0.33333
  };
  responsive.map(m => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const match = (0,useMedia/* useMedia */.G)(`(max-width: ${m.media}px)`);

    if (match) {
      media.space = m.space;
      media.firstMargin = m.firstMargin;
      media.size = m.size;
    }
  }); //# Methods

  const addNode = (0,external_react_.useCallback)((node, index) => {
    slideRefs.current[index] = node;
  }, []);
  const getSlideWidth = (0,external_react_.useCallback)(() => {
    var _scrollRef$current, _scrollRef$current$fi, _scrollRef$current$fi2;

    return ((_scrollRef$current = scrollRef.current) === null || _scrollRef$current === void 0 ? void 0 : (_scrollRef$current$fi = _scrollRef$current.firstChild) === null || _scrollRef$current$fi === void 0 ? void 0 : (_scrollRef$current$fi2 = _scrollRef$current$fi.firstChild) === null || _scrollRef$current$fi2 === void 0 ? void 0 : _scrollRef$current$fi2.clientWidth) || 0;
  }, []);

  const onSliderScroll = () => {
    setTimeout(() => {
      setIsScrolling(false);
    }, 250);

    if (!isScrolling) {
      setIsScrolling(true);
    }
  };

  const isSliderScrollable = (0,external_react_.useCallback)(() => {
    if (!scrollRef.current) return false;
    const sliderWidth = scrollRef.current.clientWidth;
    const slideWidth = getSlideWidth() - 1;
    return slideRefs.current.length * slideWidth > sliderWidth;
  }, [getSlideWidth]);

  const onChangeScroll = direction => {
    const dir = direction === "prev" ? -1 : 1;

    if (scrollRef.current) {
      const slideWidth = getSlideWidth();
      const slidesToScroll = Math.floor(scrollRef.current.clientWidth / slideWidth);
      scrollRef.current.scrollBy({
        top: 0,
        behavior: "smooth",
        left: slidesToScroll * slideWidth * dir
      });
      onStep();
    }
  };

  const onStep = (0,external_react_.useCallback)(() => {
    if (scrollRef.current) {
      const slideWidth = getSlideWidth();
      const slidesToScroll = Math.floor(scrollRef.current.clientWidth / slideWidth);
      const screen = slideWidth * slidesToScroll;
      const step = Math.floor(scrollRef.current.scrollLeft / screen) + 1;
      setStep(step);
      setSteps(children.length / slidesToScroll);
    }
  }, [children.length, getSlideWidth]); //# Prerender

  (0,external_react_.useEffect)(() => {
    if (!isSliderScrollable()) return;
    if (!scrollRef.current) return;

    if (scrollRef.current.scrollLeft <= hideArrowThreshold) {
      setIsLeft(false);
      setIsRight(true);
    } else if (scrollRef.current.clientWidth + scrollRef.current.scrollLeft >= scrollRef.current.scrollWidth - hideArrowThreshold) {
      setIsLeft(true);
      setIsRight(false);
    } else {
      setIsLeft(true);
      setIsRight(true);
    }

    if (scrollRef.current) {
      new ResizeObserver(onStep).observe(scrollRef.current);
    }
  }, [isScrolling, isSliderScrollable, onStep]);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(GroupRow, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Steps, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Step, {
          children: step
        }), /*#__PURE__*/jsx_runtime_.jsx(Space, {
          children: "/"
        }), /*#__PURE__*/jsx_runtime_.jsx(Step, {
          children: steps
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(Row, {
        disabled: !isLeft,
        onClick: () => onChangeScroll("prev"),
        children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronLeft */.YFh, {})
      }), /*#__PURE__*/jsx_runtime_.jsx(Row, {
        disabled: !isRight,
        onClick: () => onChangeScroll("next"),
        children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* FiChevronRight */.Tfp, {})
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(Content, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Scroll, {
        onScroll: onSliderScroll,
        ref: scrollRef,
        children: /*#__PURE__*/jsx_runtime_.jsx(Grid, {
          children: children.map((child, e) => /*#__PURE__*/jsx_runtime_.jsx(Snap, {
            ref: node => addNode(node, e),
            firstMargin: media.firstMargin,
            space: media.space,
            size: media.size,
            children: child
          }, e))
        })
      })
    })]
  });
};

/***/ }),

/***/ 719:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "G": () => (/* binding */ GET_HREFLA_HOME),
/* harmony export */   "d": () => (/* binding */ GET_HREFLA_TRIP)
/* harmony export */ });
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_HREFLA_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getStylesHome($slug: String) {
    gethrefhome(Slug:$slug){
        status
        data{
          slug
          cod
          url
        }
      }
  }
`;
const GET_HREFLA_TRIP = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query GETHREFLKA($Slug: String!
    $Country: String!
    $City: String!
    $Trip: String!) {
        gethref(Slug: $Slug, Country: $Country, City: $City, Trip: $Trip){
        status
        data{
          slug
          cod
          url
        }
      }
  }
`;

/***/ })

};
;
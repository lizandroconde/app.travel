"use strict";
exports.id = 254;
exports.ids = [254];
exports.modules = {

/***/ 6635:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "in": () => (/* binding */ initializeApollo),
/* harmony export */   "Uk": () => (/* binding */ useApollo)
/* harmony export */ });
/* unused harmony export APOLLO_STATE_PROP_NAME */
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1071);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var deepmerge__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1141);
/* harmony import */ var deepmerge__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(deepmerge__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# React
 //# Apollo



 //# Http link connection

const ENPOINT = "http://localhost:8000";
const httpLink = new _apollo_client__WEBPACK_IMPORTED_MODULE_1__.HttpLink({
  uri: ENPOINT,
  fetch: (isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default())
}); //# Middleware from added authorization to the headers

const authMiddleware = new _apollo_client__WEBPACK_IMPORTED_MODULE_1__.ApolloLink((operation, forward) => {
  //# Add the authorization to the headers
  operation.setContext(({
    headers = {}
  }) => ({
    headers: _objectSpread({}, headers)
  }));
  return forward(operation);
}); //# Create apollo client

const APOLLO_STATE_PROP_NAME = "__APOLLO_STATE__";
let apolloClient;

const createApolloClient = () => {
  return new _apollo_client__WEBPACK_IMPORTED_MODULE_1__.ApolloClient({
    ssrMode: true,
    link: (0,_apollo_client__WEBPACK_IMPORTED_MODULE_1__.concat)(authMiddleware, httpLink),
    cache: new _apollo_client__WEBPACK_IMPORTED_MODULE_1__.InMemoryCache({
      addTypename: true,
      resultCaching: true
    }).restore({})
  });
}; //# Initial apollo in pages


const initializeApollo = (initialState = null) => {
  var _apolloClient2;

  const _apolloClient = (_apolloClient2 = apolloClient) !== null && _apolloClient2 !== void 0 ? _apolloClient2 : createApolloClient(); // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here


  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract(); // Merge the existing cache into data passed from getStaticProps/getServerSideProps


    const data = deepmerge__WEBPACK_IMPORTED_MODULE_3___default()(initialState, existingCache); // Restore the cache with the merged data

    _apolloClient.cache.restore(data);
  } // For SSG and SSR always create a new Apollo Client


  if (true) return _apolloClient; // Create the Apollo Client once in the client

  if (!apolloClient) {
    apolloClient = _apolloClient;
  }

  return _apolloClient;
}; //# Use apollo in _app

const useApollo = pageProps => {
  const state = pageProps[APOLLO_STATE_PROP_NAME];
  const store = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(() => initializeApollo(state), [state]);
  return store;
};

/***/ }),

/***/ 2914:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "F0": () => (/* binding */ Router),
/* harmony export */   "rU": () => (/* binding */ Link)
/* harmony export */ });
/* unused harmony export routes */
/* harmony import */ var next_routes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(247);
/* harmony import */ var next_routes__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_routes__WEBPACK_IMPORTED_MODULE_0__);
// Next routes
 // Routes defs

const routes = new (next_routes__WEBPACK_IMPORTED_MODULE_0___default())();
const Router = routes.Router;
const Link = routes.Link; // Routes from pages dinamic

routes.add("Home", "/", "home").add("Homelang", "/:locale", "home").add("Stylelang", "/:locale/e/:type", "style").add("Attractionlang", "/:locale/a/:type", "attraction").add("Citylang", "/:locale/:country/:city", "city").add("Trip", "/:locale/:country/:city/:trip", "trip").add("404", "/*", "_error");

/***/ }),

/***/ 5812:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ZL": () => (/* binding */ GlobalStyle),
/* harmony export */   "M5": () => (/* binding */ Center),
/* harmony export */   "HE": () => (/* binding */ SocialIcons)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9914);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
//# Global Styled

const GlobalStyle = /*#__PURE__*/(0,styled_components__WEBPACK_IMPORTED_MODULE_0__.createGlobalStyle)(["*{margin:0;padding:0;box-sizing:border-box;outline:none;border:none;appearance:none;font-family:'Roboto Slab',serif;object-fit:cover;}body{font-family:'Roboto Slab',serif;color:", ";font-size:", ";line-height:1.5;background:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
const Center = /*#__PURE__*/styled_components__WEBPACK_IMPORTED_MODULE_0___default().div.withConfig({
  componentId: "sc-uw5f5w-0"
})(["", "{width:calc( ", " );padding-left:", ";padding-right:", ";margin-left:auto;margin-right:auto;}", "{padding-left:", ";padding-right:", ";}", "{padding-left:", ";padding-right:", ";}", "{padding-left:", ";padding-right:", ";}"], ({
  theme: {
    media
  }
}) => media.to(0, 1184, true), ({
  theme: {
    toRem
  }
}) => `${toRem(1136)} + 2 * ${toRem(24)}`, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    media
  }
}) => media.to(0, 1024, true), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    media
  }
}) => media.to(0, 768, true), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    media
  }
}) => media.to(767), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const SocialIcons = /*#__PURE__*/styled_components__WEBPACK_IMPORTED_MODULE_0___default().div.withConfig({
  componentId: "sc-uw5f5w-1"
})(["position:fixed;position:fixed;cursor:pointer;overflow:hidden;z-index:99;width:65px;border-radius:50%;height:65px;bottom:12px;left:17px;background:#ddd7d7;"]);

/***/ })

};
;
"use strict";
exports.id = 107;
exports.ids = [107];
exports.modules = {

/***/ 6107:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SC": () => (/* binding */ GET_STYLES_HOME),
/* harmony export */   "_o": () => (/* binding */ GET_ONE_STYLE),
/* harmony export */   "WT": () => (/* binding */ GET_ALL_TRIP_STYLE),
/* harmony export */   "N6": () => (/* binding */ GET_HREFLA_STYLE)
/* harmony export */ });
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_STYLES_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getStylesHome($slug: String!) {
    getStylesHome(Slug:$slug){   
    data{
        Name
        Image
        Url
        Trips
   }
   }
  }
`;
const GET_ONE_STYLE = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getStyle($slug: String, $type: String) {
    getOneStyle(Slug:$slug, Type:$type){
      status
      data {
        Title
        Name
        Description
        Canonical
        Image {
          name
          url
          alt
        }
      }
    }
  }
`;
const GET_ALL_TRIP_STYLE = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getAllTrip($slug: String, $type: String){
    getTripsStyle(Slug:$slug,Type:$type){
      data {
        _ID
        Image
        Location
        Name
        Valoration {
          Cant
          Ratio
        }
        Duration {
          Type
          Count
        }
        Badge{
          Text
          Color
        }
        Price
        Discount
        Url
      }
    }
  }
`;
const GET_HREFLA_STYLE = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getStyleHrefla($slug: String, $type: String){
    getStyleHrefla(Slug:$slug,Type:$type){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`;

/***/ })

};
;
"use strict";
(() => {
var exports = {};
exports.id = 365;
exports.ids = [365];
exports.modules = {

/***/ 5595:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ trip),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "next-i18next/serverSideTranslations"
var serverSideTranslations_ = __webpack_require__(3295);
// EXTERNAL MODULE: ./apollo/index.ts
var apollo = __webpack_require__(6635);
// EXTERNAL MODULE: ./gql/trip/query.ts
var query = __webpack_require__(9607);
// EXTERNAL MODULE: external "react-lazy-load-image-component"
var external_react_lazy_load_image_component_ = __webpack_require__(9290);
// EXTERNAL MODULE: external "simple-react-lightbox"
var external_simple_react_lightbox_ = __webpack_require__(2175);
// EXTERNAL MODULE: ./hooks/useRating.tsx
var useRating = __webpack_require__(1673);
// EXTERNAL MODULE: ./node_modules/react-icons/gi/index.esm.js
var index_esm = __webpack_require__(2585);
// EXTERNAL MODULE: ./node_modules/react-icons/fa/index.esm.js
var fa_index_esm = __webpack_require__(9583);
// EXTERNAL MODULE: ./node_modules/react-icons/ai/index.esm.js
var ai_index_esm = __webpack_require__(8193);
// EXTERNAL MODULE: ./node_modules/react-icons/cg/index.esm.js
var cg_index_esm = __webpack_require__(471);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(9914);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/banner/trip/styled.tsx

const Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-0"
})(["display:flex;flex-direction:column;row-gap:", ";padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-1"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(12));
const Seller = /*#__PURE__*/(/* unused pure expression or super */ null && (styled.div.withConfig({
  componentId: "sc-17l3im5-2"
})(["display:block;background:", ";color:", ";font-size:", ";line-height:120%;text-align:center;margin-right:auto;padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";border-radius:", ";"], ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OF28023, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(2), ({
  theme: {
    toRem
  }
}) => toRem(2), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(20))));
const styled_Title = /*#__PURE__*/external_styled_components_default().h1.withConfig({
  componentId: "sc-17l3im5-3"
})(["color:", ";font-size:", ";font-weight:700;line-height:120%;text-align:left;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Group = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-4"
})(["display:flex;align-items:center;flex-wrap:wrap;justify-content:space-between;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(16));
const SubTitle = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-5"
})(["display:flex;align-items:baseline;flex-wrap:wrap;column-gap:", ";row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(8));
const Ratings = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-6"
})(["display:flex;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const styled_Star = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-17l3im5-7"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(15));
const styled_Location = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-8"
})(["display:flex;align-items:center;flex-direction:row;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const World = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-17l3im5-9"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY151743, ({
  theme: {
    toRem
  }
}) => toRem(14));
const TextRoute = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-10"
})(["color:", ";font-size:", ";line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY151743, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Actions = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-11"
})(["display:flex;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(10));
const Action = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-12"
})(["display:flex;align-items:center;flex-direction:row;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const Icon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-17l3im5-13"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(14));
const Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-14"
})(["color:", ";font-size:", ";line-height:120%;cursor:pointer;&:hover{text-decoration:underline;}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(14));
const styled_Gallery = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-15"
})(["position:relative;width:100%;height:", ";display:grid;grid-template-columns:repeat(7,1fr);grid-template-rows:repeat(2,1fr);grid-column-gap:", ";grid-row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(300), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10));
const gridArea1 = {
  1: "1 / 1 / 3 / 3",
  2: "1 / 3 / 3 / 5",
  3: "1 / 5 / 2 / 6",
  4: "2 / 5 / 3 / 6",
  5: "1 / 6 / 3 / 8"
};
const gridArea2 = {
  1: "1 / 1 / 3 / 4",
  2: "1 / 4 / 2 / 6",
  3: "2 / 4 / 3 / 6",
  4: "1 / 6 / 2 / 8",
  5: "2 / 6 / 3 / 8"
};
const gridArea3 = {
  1: "1 / 1 / 2 / 5",
  2: "1 / 5 / 2 / 8",
  3: "2 / 1 / 3 / 3",
  4: "2 / 3 / 3 / 5",
  5: "2 / 5 / 3 / 8"
};
const Picture = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17l3im5-16"
})(["position:relative;width:100%;height:100%;overflow:hidden;background:", ";border-radius:", ";grid-area:", ";", "{grid-area:", ";}", "{grid-area:", ";}"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYE0E0E0, ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  position
}) => gridArea1[position], ({
  theme: {
    media
  }
}) => media.to(824), ({
  position
}) => gridArea2[position], ({
  theme: {
    media
  }
}) => media.to(640), ({
  position
}) => gridArea3[position]);
const ViewMore = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-17l3im5-17"
})(["position:absolute;right:", ";bottom:", ";display:flex;align-items:center;column-gap:", ";white-space:nowrap;background:", ";border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";color:", ";font-size:", ";font-weight:400;line-height:120%;cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    toRem
  }
}) => toRem(16));
const ViewIcon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-17l3im5-18"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./hooks/useContext/index.tsx
var useContext = __webpack_require__(960);
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(8475);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/banner/trip/index.tsx
// React lazy load
 // Interfaces

 // Hooks

 // Reatc icons




 // Styles




 // Component => banner trip



const BannerTrip = () => {
  const {
    openLightbox,
    closeLightbox
  } = (0,external_simple_react_lightbox_.useLightbox)(); // Destroy

  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  const photos = [1, 2, 3, 4, 5];
  const {
    info: {
      Title,
      Valoration,
      Gallery,
      Location
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  photos.splice(0, Gallery.length);
  const [ratings] = (0,useRating/* useRating */.Z)(Valoration.Ratio, Valoration.Cant);
  let lightbox = [];

  for (let img of Gallery) {
    lightbox.push({
      src: img.url,
      caption: img === null || img === void 0 ? void 0 : img.name
    });
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Head, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Title, {
        children: Title
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Group, {
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(SubTitle, {
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Ratings, {
            children: [ratings.map((Star, e) => /*#__PURE__*/jsx_runtime_.jsx(styled_Star, {
              children: /*#__PURE__*/jsx_runtime_.jsx(Star, {})
            }, e)), "(", Valoration.Cant, " ", t("review"), ")"]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Location, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(World, {
              children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* GiWorld */.tQc, {})
            }), /*#__PURE__*/jsx_runtime_.jsx(TextRoute, {
              children: Location
            })]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Actions, {
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Action, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(Icon, {
              children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaShare */.Lrt, {})
            }), /*#__PURE__*/jsx_runtime_.jsx(Text, {
              children: t("shared")
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Action, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(Icon, {
              children: /*#__PURE__*/jsx_runtime_.jsx(ai_index_esm/* AiFillHeart */.M_L, {})
            }), /*#__PURE__*/jsx_runtime_.jsx(Text, {
              children: t("save")
            })]
          })]
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(external_simple_react_lightbox_.SRLWrapper, {
      elements: lightbox
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Gallery, {
      children: [Gallery === null || Gallery === void 0 ? void 0 : Gallery.map((photo, e) => {
        const position = e + 1;
        return /*#__PURE__*/jsx_runtime_.jsx(Picture, {
          position: position,
          children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
            effect: "blur",
            width: "100%",
            height: "100%",
            alt: photo.alt,
            title: photo === null || photo === void 0 ? void 0 : photo.name,
            src: photo === null || photo === void 0 ? void 0 : photo.url
          })
        }, e);
      }), (Gallery === null || Gallery === void 0 ? void 0 : Gallery.length) <= 5 && photos.map(e => {
        return /*#__PURE__*/jsx_runtime_.jsx(Picture, {
          position: e
        }, e);
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(ViewMore, {
        onClick: () => openLightbox(),
        children: [/*#__PURE__*/jsx_runtime_.jsx(ViewIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(cg_index_esm/* CgMenuGridR */.owA, {})
        }), t("showgallery")]
      })]
    })]
  });
};
;// CONCATENATED MODULE: external "react-scroll"
const external_react_scroll_namespaceObject = require("react-scroll");
;// CONCATENATED MODULE: ./components/trip/navigation/styled.ts
// React Scroll
 // Styles components


const styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2te7y7-0"
})(["position:sticky;top:0;z-index:1;display:flex;align-items:center;flex-wrap:nowrap;column-gap:", ";overflow-x:auto;height:", ";background:", ";border-radius:", ";padding:", ";-webkit-overflow-scrolling:touch;&::-webkit-scrollbar{display:none;}"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(44), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(2));
const Item = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-2te7y7-1"
})([""]);
const Routing = /*#__PURE__*/external_styled_components_default()(external_react_scroll_namespaceObject.Link).withConfig({
  componentId: "sc-2te7y7-2"
})(["display:block;height:", ";background:", ";border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";border-color:", ";border-width:", ";border-style:solid;color:", ";font-size:", ";font-weight:400;line-height:120%;text-align:center;white-space:nowrap;cursor:pointer;&.active{background:", ";border-color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYf5f6f9, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY9aa7b5);
;// CONCATENATED MODULE: ./components/trip/navigation/index.tsx
// React

 // Styles components

 // Types props


// Component => navidation
const Navigation = props => {
  // Ref
  const item = (0,external_react_.useRef)([]);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip"); // Methods

  const scrollToCategory = id => {
    var _item$current$id;

    item === null || item === void 0 ? void 0 : (_item$current$id = item.current[id]) === null || _item$current$id === void 0 ? void 0 : _item$current$id.scrollIntoView({
      inline: "center"
    });
  };

  const mycallback = (0,external_react_.useCallback)((arg, index) => {
    item.current[index] = arg;
  }, []);
  const menus = [{
    id: "Detail",
    name: t("nav.detail")
  }, {
    id: "Itinerary",
    name: t("nav.itinerary")
  }, {
    id: "Additional_Info",
    name: t("nav.info")
  }, {
    id: "FAQ",
    name: t("nav.faq")
  }];
  return /*#__PURE__*/jsx_runtime_.jsx(styled_Container, {
    children: menus === null || menus === void 0 ? void 0 : menus.map((menu, e) => {
      return /*#__PURE__*/jsx_runtime_.jsx(Item, {
        ref: arg => mycallback(arg, e),
        children: /*#__PURE__*/jsx_runtime_.jsx(Routing, {
          activeClass: "active",
          to: menu === null || menu === void 0 ? void 0 : menu.id,
          spy: true,
          smooth: true,
          duration: 500,
          offset: -50,
          onSetActive: () => scrollToCategory(e),
          children: menu === null || menu === void 0 ? void 0 : menu.name
        })
      }, e);
    })
  });
};
// EXTERNAL MODULE: ./node_modules/react-icons/bi/index.esm.js
var bi_index_esm = __webpack_require__(7516);
// EXTERNAL MODULE: ./node_modules/react-icons/md/index.esm.js
var md_index_esm = __webpack_require__(5434);
// EXTERNAL MODULE: ./node_modules/react-icons/hi/index.esm.js
var hi_index_esm = __webpack_require__(3854);
// EXTERNAL MODULE: ./node_modules/react-icons/bs/index.esm.js
var bs_index_esm = __webpack_require__(3750);
// EXTERNAL MODULE: ./node_modules/react-icons/ti/index.esm.js
var ti_index_esm = __webpack_require__(9327);
;// CONCATENATED MODULE: ./components/trip/details/styled.ts
// Styles components

const details_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-0"
})(["display:flex;flex-direction:column;row-gap:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(32), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Tags = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-1"
})(["display:grid;align-items:center;grid-template-columns:repeat(auto-fill,minmax(173px,1fr));column-gap:", ";row-gap:", ";", "{grid-template-columns:repeat(auto-fill,minmax(140px,1fr));}"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    media
  }
}) => media.to(600));
const Tag = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-2"
})(["display:flex;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const TagIcon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-gjlbil-3"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OGFF946D, ({
  theme: {
    toRem
  }
}) => toRem(24));
const TagText = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-4"
})(["color:", ";font-size:", ";font-weight:300;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(15));
const Overview = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-5"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-gjlbil-6"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Description = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-7"
})(["color:", ";font-size:", ";font-weight:300;line-height:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(20));
const Attractions = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-8"
})(["display:grid;align-items:center;grid-template-columns:repeat(auto-fill,minmax(120px,1fr));column-gap:", ";row-gap:", ";", "{grid-template-columns:repeat(auto-fill,minmax(170px,1fr));}"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    media
  }
}) => media.to(0, 824, true));
const Attraction = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-9"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const CoverAttraction = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-10"
})(["width:100%;height:", ";border-radius:", ";overflow:hidden;"], ({
  theme: {
    toRem
  }
}) => toRem(120), ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-gjlbil-11"
})(["color:", ";font-size:", ";font-weight:300;line-height:120%;text-align:center;white-space:nowrap;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
;// CONCATENATED MODULE: external "html-react-parser"
const external_html_react_parser_namespaceObject = require("html-react-parser");
var external_html_react_parser_default = /*#__PURE__*/__webpack_require__.n(external_html_react_parser_namespaceObject);
;// CONCATENATED MODULE: ./components/trip/info-additional/styled.ts
// Styles components

const info_additional_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-11sy1v7-0"
})(["display:flex;flex-direction:column;row-gap:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(17), ({
  theme: {
    toRem
  }
}) => toRem(67));
const info_additional_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-11sy1v7-1"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
const List = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-11sy1v7-2"
})(["display:flex;flex-direction:column;row-gap:", ";list-style:circle;margin-left:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Item = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-11sy1v7-3"
})(["color:", ";font-size:", ";font-weight:300;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
;// CONCATENATED MODULE: ./components/trip/details/index.tsx
// React lazy load
 // React icons






 // Styles components






 // Types props




// Component => details
const Details = () => {
  var _Details$duration, _Details$duration2;

  const {
    info: {
      Details,
      Intro: {
        highlights,
        introduction
      },
      Places
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  let datahighlights = [];

  try {
    datahighlights = JSON.parse(highlights);
  } catch (error) {}

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(details_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Tags, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(bi_index_esm/* BiTimeFive */.s3B, {})
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(TagText, {
          children: [Details === null || Details === void 0 ? void 0 : (_Details$duration = Details.duration) === null || _Details$duration === void 0 ? void 0 : _Details$duration.Count, " ", Details === null || Details === void 0 ? void 0 : (_Details$duration2 = Details.duration) === null || _Details$duration2 === void 0 ? void 0 : _Details$duration2.Type]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(md_index_esm/* MdPeopleOutline */.q6M, {})
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(TagText, {
          children: [t("details.peoples"), ": ", Details === null || Details === void 0 ? void 0 : Details.max]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(bs_index_esm/* BsPeople */.w7k, {})
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(TagText, {
          children: [t("details.age"), ": ", Details === null || Details === void 0 ? void 0 : Details.min, "+"]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineLocationMarker */.k9l, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(TagText, {
          children: Details === null || Details === void 0 ? void 0 : Details.location
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(ti_index_esm/* TiLocationArrowOutline */.aru, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(TagText, {
          children: Places.map(({
            name
          }) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [name, ","]
          }))
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Tag, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(TagIcon, {
          children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaRegComments */.RcG, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(TagText, {
          children: Details === null || Details === void 0 ? void 0 : Details.languages
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Overview, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Title, {
        children: t("overview")
      }), /*#__PURE__*/jsx_runtime_.jsx(Description, {
        children: external_html_react_parser_default()(introduction)
      }), /*#__PURE__*/jsx_runtime_.jsx(List, {
        children: datahighlights.map((item, index) => {
          return /*#__PURE__*/jsx_runtime_.jsx(styled_Item, {
            children: item
          }, index);
        })
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Overview, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Title, {
        children: t("attractions")
      }), /*#__PURE__*/jsx_runtime_.jsx(Attractions, {
        children: Places.map(({
          image: {
            url
          },
          name
        }, index) => {
          return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Attraction, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(CoverAttraction, {
              children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
                effect: "blur",
                width: "100%",
                height: "100%",
                src: url
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(styled_Text, {
              children: name
            })]
          }, index);
        })
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/trip/itinerary/styled.ts
// Styles components

const itinerary_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-0"
})(["display:flex;flex-direction:column;row-gap:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(67));
const itinerary_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-1fq0bai-1"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-2"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(32));
const Day = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-3"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24));
const styled_Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-4"
})(["display:flex;align-items:center;column-gap:", ";background:", ";border-radius:", ";padding-left:", ";padding-right:", ";padding-top:", ";padding-bottom:", ";cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GR23501, ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    toRem
  }
}) => toRem(26), ({
  theme: {
    toRem
  }
}) => toRem(26), ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    toRem
  }
}) => toRem(20));
const GroupTitle = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-5"
})(["display:flex;align-items:flex-start;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(11));
const Calendar = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1fq0bai-6"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(18));
const TextGroup = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-7"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const DayTitle = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-8"
})(["color:", ";font-size:", ";font-weight:700;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(17));
const DaySubTitle = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-1fq0bai-9"
})(["color:", ";font-size:", ";font-weight:700;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(20));
const Down = /*#__PURE__*/(/* unused pure expression or super */ null && (styled.span.withConfig({
  componentId: "sc-1fq0bai-10"
})(["display:flex;color:", ";font-size:", ";margin-left:auto;cursor:pointer;"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY81829c, ({
  theme: {
    toRem
  }
}) => toRem(32))));
const Section = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-11"
})(["width:100%;color:", ";font-size:", ";font-weight:400;text-align:justify;font-family:sans-serif;line-height:120%;padding-left:", ";padding-right:", ";p{font-family:sans-serif;margin-top:.5rem;margin-bottom:.5rem;}& p:not(:first-child){margin-top:", ";}& a{color:", ";}& strong,& b{font-weight:700;}", "{padding-left:", ";padding-right:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OF28023, ({
  theme: {
    media
  }
}) => media.to(0, 824, true), ({
  theme: {
    toRem
  }
}) => toRem(52), ({
  theme: {
    toRem
  }
}) => toRem(52));
const styled_Attractions = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-12"
})(["display:grid;align-items:center;grid-template-columns:repeat(auto-fill,minmax(100px,1fr));column-gap:", ";row-gap:", ";padding-left:", ";padding-right:", ";", "{grid-template-columns:repeat(auto-fill,minmax(120px,1fr));column-gap:", ";padding-left:", ";padding-right:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    media
  }
}) => media.to(0, 824, true), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(52), ({
  theme: {
    toRem
  }
}) => toRem(52));
const styled_Attraction = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-13"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const styled_CoverAttraction = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-14"
})(["width:100%;height:", ";border-radius:", ";overflow:hidden;"], ({
  theme: {
    toRem
  }
}) => toRem(120), ({
  theme: {
    toRem
  }
}) => toRem(16));
const itinerary_styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fq0bai-15"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;text-align:center;white-space:nowrap;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
;// CONCATENATED MODULE: ./components/trip/itinerary/index.tsx
// React

 // React lazy load

 // React html parse

 // Interfaces

// React icons
 // Styles components



 // Types props



// Component => itinerary
const Itinerary = () => {
  const {
    info: {
      Itinerary
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip"); // States
  //const [active, setActive] = useState<number>(-1);
  // Methods

  /*const handleActive = (index: number): void => {
    setActive(index);
  };*/

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(itinerary_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(itinerary_styled_Title, {
      children: t("itinerary")
    }), /*#__PURE__*/jsx_runtime_.jsx(Content, {
      children: Itinerary.map((day, e) => {
        var _day$gallery;

        //const show: boolean = active === e;
        return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Day, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Head, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(GroupTitle, {
              children: [/*#__PURE__*/jsx_runtime_.jsx(Calendar, {
                children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaRegCalendarCheck */.smD, {})
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(TextGroup, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(DayTitle, {
                  children: (day === null || day === void 0 ? void 0 : day.day) === "0" ? t("previews") : t("day") + " " + (day === null || day === void 0 ? void 0 : day.day)
                }), /*#__PURE__*/jsx_runtime_.jsx(DaySubTitle, {
                  children: day === null || day === void 0 ? void 0 : day.title
                })]
              })]
            })
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(external_simple_react_lightbox_.SRLWrapper, {
              children: /*#__PURE__*/jsx_runtime_.jsx(styled_Attractions, {
                children: day === null || day === void 0 ? void 0 : (_day$gallery = day.gallery) === null || _day$gallery === void 0 ? void 0 : _day$gallery.map((photo, e) => {
                  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Attraction, {
                    children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
                      href: photo.url,
                      children: /*#__PURE__*/jsx_runtime_.jsx(styled_CoverAttraction, {
                        children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
                          effect: "blur",
                          width: "100%",
                          height: "100%",
                          alt: photo.name,
                          title: photo.name,
                          src: photo.url
                        })
                      })
                    }), /*#__PURE__*/jsx_runtime_.jsx(itinerary_styled_Text, {
                      children: photo.name
                    })]
                  }, e);
                })
              })
            }), /*#__PURE__*/jsx_runtime_.jsx(Section, {
              children: external_html_react_parser_default()(day.content)
            })]
          })]
        }, e);
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/trip/info-additional/index.tsx
// Styles components



 // Types props



// Component => info additional
const InfoAdditional = props => {
  var _array;

  const {
    info: {
      Aditional
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  let array = [];

  try {
    array = JSON.parse(Aditional);
  } catch (error) {
    console.log(error);
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(info_additional_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(info_additional_styled_Title, {
      children: t("additional")
    }), /*#__PURE__*/jsx_runtime_.jsx(List, {
      children: (_array = array) === null || _array === void 0 ? void 0 : _array.map((item, index) => {
        return /*#__PURE__*/jsx_runtime_.jsx(styled_Item, {
          children: item
        }, index);
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/trip/faqs/styled.ts
// Styles components

const faqs_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-0"
})(["display:flex;flex-direction:column;row-gap:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(43), ({
  theme: {
    toRem
  }
}) => toRem(67));
const faqs_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-mut68x-1"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
const styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-2"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(14));
const styled_Faq = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-3"
})(["display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(31));
const faqs_styled_Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-4"
})(["display:flex;align-items:center;column-gap:", ";padding-top:", ";padding-bottom:", ";border-bottom-color:", ";border-bottom-width:", ";border-bottom-style:solid;cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(1));
const styled_GroupTitle = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-5"
})(["display:flex;align-items:center;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const Plus = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-mut68x-6"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(18));
const FaqTitle = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-7"
})(["color:", ";font-size:", ";font-weight:700;line-height:120%;", ":hover &{text-decoration:underline;}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(20), faqs_styled_Head);
const More = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-mut68x-8"
})(["display:flex;color:", ";font-size:", ";margin-left:auto;cursor:pointer;"], ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRY81829c, ({
  theme: {
    toRem
  }
}) => toRem(32));
const styled_Section = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mut68x-9"
})(["width:100%;color:", ";font-size:", ";font-weight:300;line-height:120%;padding-left:", ";padding-right:", ";& p:not(:first-child){margin-top:", ";}& a{color:", ";}& strong,& b{font-weight:700;}", "{padding-left:", ";padding-right:", ";}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    colors
  }
}) => colors.orange.rgb.OF28023, ({
  theme: {
    media
  }
}) => media.to(0, 824, true), ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(14));
;// CONCATENATED MODULE: external "next-seo"
const external_next_seo_namespaceObject = require("next-seo");
;// CONCATENATED MODULE: ./components/trip/faqs/index.tsx
// React
 // React html parse

 // Interfaces

// React icons

 // Styles components




 // Types props



// Component => faqs
const Faqs = props => {
  // States
  const {
    0: active,
    1: setActive
  } = (0,external_react_.useState)(-1);
  const {
    info: {
      Faq
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip"); // Methods

  const handleActive = index => {
    if (active !== -1) {
      setActive(-1);
    } else {
      setActive(index);
    }
  };

  let newFaq = [];

  for (let faq of Faq) {
    newFaq.push({
      questionName: faq.ask,
      acceptedAnswerText: faq.response
    });
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(faqs_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(faqs_styled_Title, {
      children: t("faq")
    }), /*#__PURE__*/jsx_runtime_.jsx(external_next_seo_namespaceObject.FAQPageJsonLd, {
      mainEntity: newFaq
    }), /*#__PURE__*/jsx_runtime_.jsx(styled_Content, {
      children: Faq === null || Faq === void 0 ? void 0 : Faq.map((faq, e) => {
        const show = active === e;
        return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Faq, {
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(faqs_styled_Head, {
            onClick: () => handleActive(e),
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_GroupTitle, {
              children: [/*#__PURE__*/jsx_runtime_.jsx(Plus, {
                children: /*#__PURE__*/jsx_runtime_.jsx(ti_index_esm/* TiPlus */.zth, {})
              }), /*#__PURE__*/jsx_runtime_.jsx(FaqTitle, {
                children: faq.ask
              })]
            }), /*#__PURE__*/jsx_runtime_.jsx(More, {
              children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaSortDown */.sF, {})
            })]
          }), show && /*#__PURE__*/jsx_runtime_.jsx(styled_Section, {
            children: external_html_react_parser_default()(faq.response)
          })]
        }, e);
      })
    })]
  });
};
// EXTERNAL MODULE: ./components/scroll-snap/index.tsx + 1 modules
var scroll_snap = __webpack_require__(1809);
;// CONCATENATED MODULE: ./components/trip/additionals/styled.ts
// Styles components

const additionals_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-m5j7jb-0"
})(["display:flex;flex-direction:column;row-gap:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(57));
const additionals_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-m5j7jb-1"
})(["color:", ";font-size:", ";font-weight:600;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24));
const additionals_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-m5j7jb-2"
})(["width:100%;display:flex;align-items:center;"]);
const Card = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-m5j7jb-3"
})(["display:flex;align-items:center;justify-content:center;flex-direction:column;row-gap:", ";border-color:", ";border-width:", ";border-style:solid;border-radius:", ";padding-left:", ";padding-right:", ";padding-bottom:", ";padding-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYe4e8ec, ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(14), ({
  theme: {
    toRem
  }
}) => toRem(42));
const additionals_styled_Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-m5j7jb-4"
})(["display:flex;align-items:center;justify-content:center;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(6));
const styled_Icon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-m5j7jb-5"
})(["display:flex;font-size:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
const additionals_styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-m5j7jb-6"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(17));
const styled_List = /*#__PURE__*/external_styled_components_default().ul.withConfig({
  componentId: "sc-m5j7jb-7"
})(["display:flex;flex-direction:column;row-gap:", ";list-style:circle;padding-left:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(16));
const additionals_styled_Item = /*#__PURE__*/external_styled_components_default().li.withConfig({
  componentId: "sc-m5j7jb-8"
})(["color:", ";font-size:", ";font-weight:300;line-height:120%;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(13));
// EXTERNAL MODULE: ./node_modules/react-icons/io/index.esm.js
var io_index_esm = __webpack_require__(1649);
// EXTERNAL MODULE: ./node_modules/react-icons/ri/index.esm.js
var ri_index_esm = __webpack_require__(9352);
;// CONCATENATED MODULE: ./hooks/icon/index.tsx







const IconTrip = {
  "trasport": /*#__PURE__*/jsx_runtime_.jsx(ai_index_esm/* AiFillCar */.q2x, {}),
  "food": /*#__PURE__*/jsx_runtime_.jsx(io_index_esm/* IoIosRestaurant */.y59, {}),
  "ticket": /*#__PURE__*/jsx_runtime_.jsx(hi_index_esm/* HiOutlineTicket */.Kaj, {}),
  "hostel": /*#__PURE__*/jsx_runtime_.jsx(md_index_esm/* MdHotel */.GTs, {}),
  "service": /*#__PURE__*/jsx_runtime_.jsx(ri_index_esm/* RiServiceFill */.TuE, {}),
  "train": /*#__PURE__*/jsx_runtime_.jsx(md_index_esm/* MdTrain */.Y75, {}),
  "trekking": /*#__PURE__*/jsx_runtime_.jsx(index_esm/* GiMountains */.E3P, {}),
  "cloth": /*#__PURE__*/jsx_runtime_.jsx(index_esm/* GiClothes */.DXo, {}),
  "plastic": /*#__PURE__*/jsx_runtime_.jsx(index_esm/* GiUmbrella */.cvu, {}),
  "personal": /*#__PURE__*/jsx_runtime_.jsx(md_index_esm/* MdCleanHands */.YMj, {}),
  "kit": /*#__PURE__*/jsx_runtime_.jsx(index_esm/* GiFirstAidKit */.ODP, {})
};
/* harmony default export */ const icon = (IconTrip);
;// CONCATENATED MODULE: ./components/trip/additionals/index.tsx
// Interfaces
// Components
 // React icons

// Styles components





 // Types props



// Component => whats included
const WhatsIncluded = () => {
  const {
    info: {
      Include: {
        Include
      }
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(additionals_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Title, {
      children: t("include")
    }), /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Content, {
      children: /*#__PURE__*/jsx_runtime_.jsx(scroll_snap/* ScrollSnap */.a, {
        defaultSize: 0.25,
        responsive: responsive,
        children: Include === null || Include === void 0 ? void 0 : Include.map(({
          Title,
          Icon,
          Content
        }, e) => {
          let array = [];

          try {
            array = JSON.parse(Content);
          } catch (e) {
            console.log(e);
          }

          return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Card, {
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(additionals_styled_Head, {
              children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Icon, {
                children: icon[Icon]
              }), /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Text, {
                children: Title
              })]
            }), /*#__PURE__*/jsx_runtime_.jsx(styled_List, {
              children: array.map((item, e) => {
                return /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Item, {
                  children: item
                }, e);
              })
            })]
          }, e);
        })
      })
    })]
  });
}; // Component => whats not included

const WhatsNotIncluded = () => {
  var _array;

  const {
    info: {
      Include
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  let array = [];

  try {
    array = JSON.parse(Include === null || Include === void 0 ? void 0 : Include.NoInclude);
  } catch (error) {
    console.log(error);
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(additionals_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Title, {
      children: t("noinclude")
    }), /*#__PURE__*/jsx_runtime_.jsx(List, {
      children: (_array = array) === null || _array === void 0 ? void 0 : _array.map((item, index) => {
        return /*#__PURE__*/jsx_runtime_.jsx(styled_Item, {
          children: item
        }, index);
      })
    })]
  });
}; // Component => packing list

const PackingList = () => {
  const {
    info: {
      Include: {
        Recommendations
      }
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(additionals_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Title, {
      children: t("list")
    }), /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Content, {
      children: /*#__PURE__*/jsx_runtime_.jsx(scroll_snap/* ScrollSnap */.a, {
        defaultSize: 0.25,
        responsive: responsive,
        children: Recommendations === null || Recommendations === void 0 ? void 0 : Recommendations.map(({
          Title,
          Content,
          Icon
        }, e) => {
          var _array2;

          let array = [];

          try {
            array = JSON.parse(Content);
          } catch (e) {
            console.log(e);
          }

          return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Card, {
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(additionals_styled_Head, {
              children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Icon, {
                active: true,
                children: icon[Icon]
              }), /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Text, {
                children: Title
              })]
            }), /*#__PURE__*/jsx_runtime_.jsx(styled_List, {
              children: (_array2 = array) === null || _array2 === void 0 ? void 0 : _array2.map((item, e) => {
                return /*#__PURE__*/jsx_runtime_.jsx(additionals_styled_Item, {
                  children: item
                }, e);
              })
            })]
          }, e);
        })
      })
    })]
  });
}; // Responsive

const responsive = [{
  media: 1184,
  space: 16,
  firstMargin: 24,
  size: 0.25
}, {
  media: 1024,
  space: 8,
  firstMargin: 24,
  size: 0.3
}, {
  media: 767,
  space: 8,
  firstMargin: 24,
  size: 0.3
}, {
  media: 500,
  space: 8,
  firstMargin: 24,
  size: 0.6
}];
// EXTERNAL MODULE: ./themes/global/index.ts
var global = __webpack_require__(5812);
;// CONCATENATED MODULE: ./components/trip/styled.ts
// Styled components

const trip_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-0"
})(["width:100%;background:", ";padding-top:", ";padding-bottom:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(24));
const trip_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-1"
})(["width:100%;"]);
const styled_Actions = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-2"
})(["display:flex;align-items:center;flex-wrap:wrap;column-gap:", ";row-gap:", ";background:", ";border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";margin-top:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    colors
  }
}) => colors.blue.rgb.BL3b556a, ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(27), ({
  theme: {
    toRem
  }
}) => toRem(21), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(40), ({
  theme: {
    toRem
  }
}) => toRem(55));
const styled_Action = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-3"
})(["display:flex;align-items:center;column-gap:", ";cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(11));
const trip_styled_Icon = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-12lepga-4"
})(["display:flex;color:", ";font-size:", ";"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(24));
const trip_styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-5"
})(["color:", ";font-size:", ";font-weight:400;line-height:120%;", ":hover &{text-decoration:underline;}"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(17), styled_Action);
const Layout = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-6"
})(["background:", ";display:flex;", "{display:block;}"], ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    media
  }
}) => media.to(1000));
const ContentLeft = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-7"
})(["width:65%;margin-right:", ";margin-bottom:", ";", "{width:60%;}", "{width:100%;margin-right:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(20), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    media
  }
}) => media.to(1250), ({
  theme: {
    media
  }
}) => media.to(1000), ({
  theme: {
    toRem
  }
}) => toRem(0));
const ContentRight = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-8"
})(["width:35%;margin-bottom:", ";", "{width:40%;}", "{width:100%;}"], ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    media
  }
}) => media.to(1250), ({
  theme: {
    media
  }
}) => media.to(1000));
const StickyBook = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-9"
})(["position:sticky;top:", ";padding-top:", ";padding-left:2.5rem;padding-right:2.6rem;", "{padding-left:.5rem;padding-right:.5rem;}"], ({
  theme: {
    toRem
  }
}) => toRem(3), ({
  theme: {
    toRem
  }
}) => toRem(50), ({
  theme: {
    media
  }
}) => media.to(1000));
const trip_styled_Head = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-10"
})(["text-align:right;border-radius:8px 8px 0 0;padding:10px;border:1px solid #e8e9e9;", "{display:flex;flex-direction:column;}"], ({
  theme: {
    media
  }
}) => media.to(1000));
const styled_Price = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-12lepga-11"
})(["color:", ";font-size:", ";font-weight:700;margin-left:", ";", "{font-size:", ";font-weight:600;}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(32), ({
  theme: {
    toRem
  }
}) => toRem(6), ({
  theme: {
    media
  }
}) => media.to(1000), ({
  theme: {
    toRem
  }
}) => toRem(28));
const From = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-12lepga-12"
})(["color:", ";font-size:", ";font-weight:500;margin-right:10px;margin-left:", ";", "{font-size:", ";font-weight:600;}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(22), ({
  theme: {
    toRem
  }
}) => toRem(6), ({
  theme: {
    media
  }
}) => media.to(1000), ({
  theme: {
    toRem
  }
}) => toRem(28));
const Badge = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-12lepga-13"
})(["background:rgb(163,0,0);color:white;display:block;height:max-content;border-radius:25px;padding:0px 10px;font-size:0.8rem;margin-right:10px;"]);
const Discount = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-12lepga-14"
})(["font-weight:400;font-size:1.54rem;text-decoration:line-through;"]);
const DicountLayout = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-12lepga-15"
})(["display:flex;justify-content:flex-end;align-items:center;"]);
// EXTERNAL MODULE: ./hooks/validate/index..ts
var index_ = __webpack_require__(4122);
;// CONCATENATED MODULE: ./components/trip/index.tsx
// React Scroll
 // Components






 // Global

 // React icons

 // Styles components





 // Component => content




const trip_Content = () => {
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("trip");
  const {
    info: {
      CodWetravel,
      Price
    }
  } = (0,external_react_.useContext)(useContext/* TripContext */.QS);
  return /*#__PURE__*/jsx_runtime_.jsx(trip_styled_Container, {
    children: /*#__PURE__*/jsx_runtime_.jsx(global/* Center */.M5, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Layout, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(ContentLeft, {
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(trip_styled_Content, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(Navigation, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_scroll_namespaceObject.Element, {
              name: "Detail",
              children: [/*#__PURE__*/jsx_runtime_.jsx(Details, {}), /*#__PURE__*/jsx_runtime_.jsx(WhatsIncluded, {}), /*#__PURE__*/jsx_runtime_.jsx(WhatsNotIncluded, {}), /*#__PURE__*/jsx_runtime_.jsx(PackingList, {})]
            }), /*#__PURE__*/jsx_runtime_.jsx(external_react_scroll_namespaceObject.Element, {
              name: "Itinerary",
              children: /*#__PURE__*/jsx_runtime_.jsx(Itinerary, {})
            }), /*#__PURE__*/jsx_runtime_.jsx(external_react_scroll_namespaceObject.Element, {
              name: "Additional_Info",
              children: /*#__PURE__*/jsx_runtime_.jsx(InfoAdditional, {})
            }), /*#__PURE__*/jsx_runtime_.jsx(external_react_scroll_namespaceObject.Element, {
              name: "FAQ",
              children: /*#__PURE__*/jsx_runtime_.jsx(Faqs, {})
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Actions, {
              children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Action, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(trip_styled_Icon, {
                  children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaComments */.OdJ, {})
                }), /*#__PURE__*/jsx_runtime_.jsx(trip_styled_Text, {
                  children: t("question")
                })]
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Action, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(trip_styled_Icon, {
                  children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaDownload */.aBF, {})
                }), /*#__PURE__*/jsx_runtime_.jsx(trip_styled_Text, {
                  children: t("download")
                })]
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Action, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(trip_styled_Icon, {
                  children: /*#__PURE__*/jsx_runtime_.jsx(fa_index_esm/* FaHeart */.$0H, {})
                }), /*#__PURE__*/jsx_runtime_.jsx(trip_styled_Text, {
                  children: t("savelist")
                })]
              })]
            })]
          })
        }), /*#__PURE__*/jsx_runtime_.jsx(ContentRight, {
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(StickyBook, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(trip_styled_Head, {
              children: Price.Discount !== 0 ? /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
                children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(DicountLayout, {
                  children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Badge, {
                    children: ["- ", Price.Discount, " % "]
                  }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Discount, {
                    children: [(0,index_/* ValidateSeparatePrice */.K3)({
                      price: Price.Amount
                    }), " USD"]
                  })]
                }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Price, {
                  children: [/*#__PURE__*/jsx_runtime_.jsx(From, {
                    children: t("from")
                  }), (0,index_/* ValidateDiscount */.LN)({
                    prices: Price.Amount,
                    discount: Price.Discount
                  }), " ", "USD"]
                })]
              }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Price, {
                children: [/*#__PURE__*/jsx_runtime_.jsx(From, {
                  children: t("from")
                }), (0,index_/* ValidateSeparatePrice */.K3)({
                  price: Price.Amount
                }), " USD"]
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("iframe", {
              src: `https://www.wetravel.com/embed/calendar?uuid=${CodWetravel}&btnColor=f5a623&btnName=${t("booknow")}&title=${t("startdate")}&env=https://www.wetravel.com`,
              style: {
                width: "100%",
                minHeight: "500px",
                display: "block"
              }
            })]
          })
        })]
      })
    })
  });
};
// EXTERNAL MODULE: ./components/layout/index.tsx + 24 modules
var layout = __webpack_require__(39);
// EXTERNAL MODULE: ./gql/hrflag/query.ts
var hrflag_query = __webpack_require__(719);
// EXTERNAL MODULE: external "next/error"
var error_ = __webpack_require__(8354);
var error_default = /*#__PURE__*/__webpack_require__.n(error_);
;// CONCATENATED MODULE: ./pages/trip/index.tsx
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Next



// Components

 // Global







 //# Types props




//# Page => Trip
const Trip = ({
  info,
  languages,
  fallback
}) => {
  if (fallback) return /*#__PURE__*/jsx_runtime_.jsx((error_default()), {
    statusCode: 404
  });
  const {
    Seo,
    Gallery,
    Price,
    Valoration
  } = info;
  const productgallery = [];

  for (let img of Gallery) {
    productgallery.push(img === null || img === void 0 ? void 0 : img.url);
  }

  const alternatives = [];

  for (let language of languages) {
    alternatives.push({
      hrefLang: language === null || language === void 0 ? void 0 : language.slug,
      href: language === null || language === void 0 ? void 0 : language.url
    });
  }

  return /*#__PURE__*/jsx_runtime_.jsx(useContext/* HeaderContext.Provider */.pI.Provider, {
    value: {
      languages
    },
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(layout/* default */.Z, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(external_next_seo_namespaceObject.NextSeo, {
        nofollow: true,
        languageAlternates: alternatives,
        title: (Seo === null || Seo === void 0 ? void 0 : Seo.title) + " - Conde Travel",
        description: Seo === null || Seo === void 0 ? void 0 : Seo.description,
        canonical: Seo === null || Seo === void 0 ? void 0 : Seo.canonical,
        openGraph: {
          url: Seo === null || Seo === void 0 ? void 0 : Seo.description,
          title: Seo === null || Seo === void 0 ? void 0 : Seo.title,
          description: Seo === null || Seo === void 0 ? void 0 : Seo.description,
          images: [{
            url: Seo === null || Seo === void 0 ? void 0 : Seo.image
          }],
          site_name: 'Conde Travel'
        },
        twitter: {
          handle: '@handle',
          site: '@site',
          cardType: 'summary_large_image'
        }
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(useContext/* TripContext.Provider */.QS.Provider, {
        value: {
          info
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(global/* Center */.M5, {
          children: /*#__PURE__*/jsx_runtime_.jsx(BannerTrip, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(trip_Content, {})]
      }), /*#__PURE__*/jsx_runtime_.jsx(external_next_seo_namespaceObject.ProductJsonLd, {
        productName: Seo === null || Seo === void 0 ? void 0 : Seo.title,
        images: productgallery,
        description: Seo === null || Seo === void 0 ? void 0 : Seo.description,
        brand: "Conde Travel",
        color: "orange",
        manufacturerName: "Luis Conde",
        manufacturerLogo: "https://images.conde.travel/logo.jpeg",
        releaseDate: "2022-02-05T08:00:00+08:00",
        productionDate: "2022-02-05T08:00:00+08:00",
        purchaseDate: "2022-02-06T08:00:00+08:00",
        reviews: [{
          author: 'Jim',
          datePublished: '2022-01-06T03:37:40Z',
          reviewBody: 'This is my favorite product yet! Thanks Nate for the example products and reviews.',
          name: 'So awesome!!!',
          reviewRating: {
            bestRating: '5',
            ratingValue: '5',
            worstRating: '1'
          },
          publisher: {
            type: 'Organization',
            name: 'TwoVit'
          }
        }],
        aggregateRating: {
          ratingValue: Valoration.Ratio,
          reviewCount: Valoration.Cant
        },
        offers: [{
          price: (Price === null || Price === void 0 ? void 0 : Price.Discount) > 0 ? (0,index_/* ValidateNormalDiscount */.JP)({
            discount: Price === null || Price === void 0 ? void 0 : Price.Discount,
            prices: Price === null || Price === void 0 ? void 0 : Price.Amount
          }) : Price === null || Price === void 0 ? void 0 : Price.Amount,
          priceCurrency: 'USD',
          priceValidUntil: '2022-11-05',
          itemCondition: 'https://schema.org/UsedCondition',
          availability: 'https://schema.org/InStock',
          url: Seo === null || Seo === void 0 ? void 0 : Seo.canonical,
          seller: {
            name: 'Ruth Mery Cusihuaman'
          }
        }]
      })]
    })
  });
};

const getServerSideProps = async ({
  query: {
    locale,
    country,
    city,
    trip
  }
}) => {
  var _await$client$query$d, _await$client$query$d2, _await$client$query$d3, _await$client$query$d4;

  const client = (0,apollo/* initializeApollo */.in)();
  const info = await ((_await$client$query$d = (await client.query({
    query: query/* GET_TRIP_SLUG */.Zg,
    variables: {
      Slug: locale,
      Country: country,
      City: city,
      Trip: trip
    }
  })).data) === null || _await$client$query$d === void 0 ? void 0 : (_await$client$query$d2 = _await$client$query$d.getTripSlug) === null || _await$client$query$d2 === void 0 ? void 0 : _await$client$query$d2.data);

  if (info === null) {
    return {
      props: {
        fallback: true
      }
    };
  }

  const languages = await ((_await$client$query$d3 = (await client.query({
    query: hrflag_query/* GET_HREFLA_TRIP */.d,
    variables: {
      Slug: locale,
      Country: country,
      City: city,
      Trip: trip
    }
  })).data) === null || _await$client$query$d3 === void 0 ? void 0 : (_await$client$query$d4 = _await$client$query$d3.gethref) === null || _await$client$query$d4 === void 0 ? void 0 : _await$client$query$d4.data);
  return {
    props: _objectSpread(_objectSpread({}, await (0,serverSideTranslations_.serverSideTranslations)(locale, ["trip", "common"])), {}, {
      info,
      languages
    })
  };
};
/* harmony default export */ const trip = (Trip);

/***/ }),

/***/ 8074:
/***/ ((module) => {

module.exports = require("@apollo/client");

/***/ }),

/***/ 1141:
/***/ ((module) => {

module.exports = require("deepmerge");

/***/ }),

/***/ 1071:
/***/ ((module) => {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 8475:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 3295:
/***/ ((module) => {

module.exports = require("next-i18next/serverSideTranslations");

/***/ }),

/***/ 247:
/***/ ((module) => {

module.exports = require("next-routes");

/***/ }),

/***/ 822:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 8354:
/***/ ((module) => {

module.exports = require("next/error");

/***/ }),

/***/ 6731:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 9297:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 62:
/***/ ((module) => {

module.exports = require("react-cool-onclickoutside");

/***/ }),

/***/ 2662:
/***/ ((module) => {

module.exports = require("react-hook-form");

/***/ }),

/***/ 9290:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 9997:
/***/ ((module) => {

module.exports = require("react-modal");

/***/ }),

/***/ 5282:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 2175:
/***/ ((module) => {

module.exports = require("simple-react-lightbox");

/***/ }),

/***/ 9914:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [675,108,434,722,254,844,607,585], () => (__webpack_exec__(5595)));
module.exports = __webpack_exports__;

})();
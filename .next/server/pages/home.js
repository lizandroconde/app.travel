"use strict";
(() => {
var exports = {};
exports.id = 229;
exports.ids = [229];
exports.modules = {

/***/ 3557:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "pd": () => (/* binding */ GET_CITIES_HOME),
/* harmony export */   "ru": () => (/* binding */ GET_CITY),
/* harmony export */   "FZ": () => (/* binding */ GET_HREFLA_CITY)
/* harmony export */ });
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8074);
/* harmony import */ var _apollo_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_apollo_client__WEBPACK_IMPORTED_MODULE_0__);
//# Graphql => global

const GET_CITIES_HOME = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getCitieshomeD($slug: String!) {
    getCitieshome(Slug:$slug){   
    data{
     Name
     Url
     Trips
     Image
   }
   }
  }
`;
const GET_CITY = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getCity($slug:String,$country:String,$city:String){
    getSlugCity(Slug:$slug,Country:$country,City:$city){
      status
      data{
        Title
        Name
        Description
        Image{
          name
          url
          alt
        }
      }
    }
  }
`;
const GET_HREFLA_CITY = _apollo_client__WEBPACK_IMPORTED_MODULE_0__.gql`
  query getCityHreflag($slug:String,$country:String,$city:String){
    getCityHreflag(Slug:$slug,Country:$country,City:$city){
      status
      message
      data{
        slug
        url
        cod
      }
    }
  }
`;

/***/ }),

/***/ 4389:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ home),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(701);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: external "next-i18next/serverSideTranslations"
var serverSideTranslations_ = __webpack_require__(3295);
// EXTERNAL MODULE: ./hooks/useContext/index.tsx
var useContext = __webpack_require__(960);
// EXTERNAL MODULE: external "react-lazy-load-image-component"
var external_react_lazy_load_image_component_ = __webpack_require__(9290);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "react-hook-form"
var external_react_hook_form_ = __webpack_require__(2662);
// EXTERNAL MODULE: external "react-cool-onclickoutside"
var external_react_cool_onclickoutside_ = __webpack_require__(62);
var external_react_cool_onclickoutside_default = /*#__PURE__*/__webpack_require__.n(external_react_cool_onclickoutside_);
// EXTERNAL MODULE: ./node_modules/react-icons/md/index.esm.js
var index_esm = __webpack_require__(5434);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(9914);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/banner/search-travels/where-going/styled.tsx
//# Styled components

const Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-0"
})(["position:relative;width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const FormControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-1"
})(["display:flex;align-items:center;column-gap:", ";background:", ";border-radius:", ";transition:background 0.2s cubic-bezier(0.075,0.82,0.165,1);&:hover{background:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYF2F3F5);
const Location = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1n7c4bk-2"
})(["display:flex;color:", ";font-size:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16));
const InputControl = /*#__PURE__*/external_styled_components_default().input.withConfig({
  componentId: "sc-1n7c4bk-3"
})(["background:transparent;padding-top:", ";padding-bottom:", ";padding-right:", ";padding-left:", ";color:", ";font-size:", ";line-height:120%;font-weight:normal;&:focus::placeholder{color:transparent;}"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
const AutoComplete = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-4"
})(["position:absolute;top:calc(100% + ", ");right:0;width:100%;background:", ";z-index:100;box-shadow:", ";border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem,
    colors
  }
}) => `
    0 ${toRem(16)} ${toRem(24)} rgb(3 54 63 / 16%), 0 ${toRem(8)} ${toRem(16)} rgb(3 54 63 / 4%),
    0 ${toRem(4)} ${toRem(8)} rgb(3 54 63 / 4%), 0 ${toRem(2)} ${toRem(4)} rgb(3 54 63 / 2%),
    0 ${toRem(1)} ${toRem(2)} rgb(3 54 63 / 4%), 0 ${toRem(-1)} ${toRem(2)} rgb(3 54 63 / 4%)
    `, ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8));
const AutoCompleteItem = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-5"
})(["display:flex;justify-content:space-between;align-items:center;padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";border-radius:", ";cursor:pointer;transition:background 0.2s cubic-bezier(0.075,0.82,0.165,1);&:hover{background:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(9), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
const Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-6"
})(["color:", ";font-size:", ";line-height:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(24));
const SubText = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1n7c4bk-7"
})(["color:", ";font-size:", ";line-height:", ";text-align:right;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(24));
// EXTERNAL MODULE: ./gql/global/querys.ts
var querys = __webpack_require__(5550);
// EXTERNAL MODULE: external "@apollo/client"
var client_ = __webpack_require__(8074);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/banner/search-travels/where-going/index.tsx
//# React
 //# React outsite

 //# React hook form

 //# Router 

// React icons
 //# Styles components




 //# Types props



var intervalo; //# Component => where going

const WhereGoing = props => {
  var _router$query;

  //# States
  const {
    0: active,
    1: setActive
  } = (0,external_react_.useState)(false);
  const {
    0: goings,
    1: setGoings
  } = (0,external_react_.useState)([]);
  const {
    0: value,
    1: setValue
  } = (0,external_react_.useState)(""); //# Hooks

  const {
    field
  } = (0,external_react_hook_form_.useController)(props);
  const ref = external_react_cool_onclickoutside_default()(() => {
    setActive(false);
  });
  const router = (0,router_.useRouter)();
  const locale = (router === null || router === void 0 ? void 0 : (_router$query = router.query) === null || _router$query === void 0 ? void 0 : _router$query.locale) || "en"; //# Use Lazy

  const [getResult, {
    data
  }] = (0,client_.useLazyQuery)(querys/* SEARCH_HOME */.vN);

  const handleResult = async query => {
    getResult(query);
  };

  (0,external_react_.useEffect)(() => {
    var _data$getSearchHome;

    data && setGoings(data === null || data === void 0 ? void 0 : (_data$getSearchHome = data.getSearchHome) === null || _data$getSearchHome === void 0 ? void 0 : _data$getSearchHome.data);
  }, [data]); //# Methods

  const onHandleChange = e => {
    const {
      value
    } = e.currentTarget;
    setValue(value);
    field.onChange(value);

    if (value.length > 2) {
      clearTimeout(intervalo);
      intervalo = setTimeout(() => {
        handleResult({
          variables: {
            slug: locale,
            text: value
          }
        });
        clearTimeout(intervalo); //Limpio el intervalo
      }, 1000);
    }
  };

  const onFocus = () => {
    setActive(true);
  };

  const onSelect = value => {
    setValue(value);
    field.onChange(value);
    setActive(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Container, {
    ref: ref,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(FormControl, {
      onClick: onFocus,
      children: [/*#__PURE__*/jsx_runtime_.jsx(Location, {
        children: /*#__PURE__*/jsx_runtime_.jsx(index_esm/* MdLocationOn */.$0r, {})
      }), /*#__PURE__*/jsx_runtime_.jsx(InputControl, {
        ref: field.ref,
        name: field.name,
        value: value,
        onChange: onHandleChange,
        placeholder: "Where are you going?"
      })]
    }), active && /*#__PURE__*/jsx_runtime_.jsx(AutoComplete, {
      children: (goings === null || goings === void 0 ? void 0 : goings.length) > 0 ? goings === null || goings === void 0 ? void 0 : goings.map((item, e) => /*#__PURE__*/(0,jsx_runtime_.jsxs)(AutoCompleteItem, {
        onClick: () => router.push(item.url),
        children: [/*#__PURE__*/jsx_runtime_.jsx(Text, {
          children: item.name
        }), /*#__PURE__*/jsx_runtime_.jsx(SubText, {
          children: item.type
        })]
      }, e)) : /*#__PURE__*/jsx_runtime_.jsx(AutoCompleteItem, {
        children: /*#__PURE__*/jsx_runtime_.jsx(Text, {
          children: "No se encontraron datos"
        })
      })
    })]
  });
};
;// CONCATENATED MODULE: external "moment"
const external_moment_namespaceObject = require("moment");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_namespaceObject);
// EXTERNAL MODULE: ./node_modules/react-icons/io5/index.esm.js
var io5_index_esm = __webpack_require__(155);
// EXTERNAL MODULE: ./node_modules/react-icons/bs/index.esm.js
var bs_index_esm = __webpack_require__(3750);
;// CONCATENATED MODULE: ./components/banner/search-travels/when-go/styled.tsx
//# Styled components

const styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-0"
})(["position:relative;width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const styled_FormControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-1"
})(["display:flex;align-items:center;column-gap:", ";background:", ";border-radius:", ";cursor:pointer;transition:background 0.2s cubic-bezier(0.075,0.82,0.165,1);&:hover{background:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYF2F3F5);
const Calendar = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-1mx2lav-2"
})(["display:flex;color:", ";font-size:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16));
const SelectControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-3"
})(["padding-top:", ";padding-bottom:", ";padding-right:", ";padding-left:", ";color:", ";font-size:", ";line-height:120%;font-weight:normal;"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  active,
  theme: {
    colors
  }
}) => !active ? colors.gray.rgb.GRY151743 : colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
const AutoCalendar = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-4"
})(["position:absolute;top:calc(100% + ", ");left:0;display:flex;flex-direction:column;row-gap:", ";width:", ";min-width:100%;background:", ";z-index:100;box-shadow:", ";border-radius:", ";padding-top:", ";padding-bottom:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(300), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem,
    colors
  }
}) => `
    0 ${toRem(16)} ${toRem(24)} rgb(3 54 63 / 16%), 0 ${toRem(8)} ${toRem(16)} rgb(3 54 63 / 4%),
    0 ${toRem(4)} ${toRem(8)} rgb(3 54 63 / 4%), 0 ${toRem(2)} ${toRem(4)} rgb(3 54 63 / 2%),
    0 ${toRem(1)} ${toRem(2)} rgb(3 54 63 / 4%), 0 ${toRem(-1)} ${toRem(2)} rgb(3 54 63 / 4%)
    `, ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16));
const MonthSelector = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-5"
})(["display:flex;align-items:center;justify-content:space-between;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const Year = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-6"
})(["color:", ";font-size:", ";line-height:120%;font-weight:600;text-align:left;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
const Arrows = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-7"
})(["display:flex;align-items:center;justify-content:space-between;"]);
const Arrow = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-1mx2lav-8"
})(["display:flex;justify-content:center;align-items:center;width:", ";height:", ";background:", ";color:", ";font-size:", ";cursor:pointer;transition:color 0.2s cubic-bezier(0.075,0.82,0.165,1);&:hover{color:", ";}&:disabled{color:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(44), ({
  theme: {
    toRem
  }
}) => toRem(44), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLK4A5A5A, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYCCC);
const Months = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1mx2lav-9"
})(["display:flex;flex-wrap:wrap;justify-content:space-between;gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(8));
const Month = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-1mx2lav-10"
})(["display:flex;justify-content:center;align-items:center;width:calc(33.33% - ", ");height:", ";background:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;color:", ";font-size:", ";line-height:120%;font-weight:400;letter-spacing:0.2;cursor:pointer;transition:border 0.2s cubic-bezier(0.165,0.84,0.44,1);&:hover{border-color:", ";}&:disabled{color:", ";border-color:", ";cursor:not-allowed;}"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(48), ({
  active,
  theme: {
    colors
  }
}) => active ? colors.black.rgb.BL000 : colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(9), ({
  theme: {
    toRem
  }
}) => toRem(1), ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2, ({
  active,
  theme: {
    colors
  }
}) => active ? colors.white.rgb.WFFF : colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2, ({
  theme: {
    colors
  }
}) => colors.gray.rgb.GRYEBEEF2);
;// CONCATENATED MODULE: ./components/banner/search-travels/when-go/index.tsx
//# React
 //# Moment

 //# React outsite

 //# React hook form

 //# Interfaces

// React icons

 //# Styles components

 //# Types props



//# Component => when go
const WhenGo = props => {
  //# Def
  const months = external_moment_default().monthsShort();
  const yearNow = external_moment_default()().year();
  const monthNow = external_moment_default()().month() + 1; //# States

  const {
    0: active,
    1: setActive
  } = (0,external_react_.useState)(false);
  const {
    0: value,
    1: setValue
  } = (0,external_react_.useState)("");
  const {
    0: newYear,
    1: setNewYear
  } = (0,external_react_.useState)(yearNow);
  const {
    0: year,
    1: setYear
  } = (0,external_react_.useState)(0);
  const {
    0: month,
    1: setMonth
  } = (0,external_react_.useState)(""); //# Hooks

  const {
    field
  } = (0,external_react_hook_form_.useController)(props);
  const ref = external_react_cool_onclickoutside_default()(() => {
    setActive(false);
  }); //# Methods

  const onFocus = () => setActive(true);

  const onPrevYear = () => setNewYear(year => year - 1);

  const onNextYear = () => setNewYear(year => year + 1);

  const onSelect = (month, year) => {
    const value = `${month} ${year}`;
    setYear(year);
    setMonth(month);
    setValue(value);
    field.onChange(value);
    setActive(false);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Container, {
    ref: ref,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_FormControl, {
      onClick: onFocus,
      children: [/*#__PURE__*/jsx_runtime_.jsx(Calendar, {
        children: /*#__PURE__*/jsx_runtime_.jsx(io5_index_esm/* IoCalendarOutline */.ORF, {})
      }), /*#__PURE__*/jsx_runtime_.jsx(SelectControl, {
        active: value.length > 0,
        children: value ? value : !active ? "When would you go?" : ""
      })]
    }), active && /*#__PURE__*/(0,jsx_runtime_.jsxs)(AutoCalendar, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(MonthSelector, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Year, {
          children: newYear
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Arrows, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Arrow, {
            disabled: newYear === yearNow,
            onClick: onPrevYear,
            children: /*#__PURE__*/jsx_runtime_.jsx(bs_index_esm/* BsChevronLeft */.pjk, {})
          }), /*#__PURE__*/jsx_runtime_.jsx(Arrow, {
            onClick: onNextYear,
            children: /*#__PURE__*/jsx_runtime_.jsx(bs_index_esm/* BsChevronRight */.fmn, {})
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(Months, {
        children: months.map((value, e) => {
          const monthNumber = e + 1;
          const active = newYear === year && value === month;
          const disabled = newYear === yearNow && monthNumber < monthNow ? true : false;
          return /*#__PURE__*/jsx_runtime_.jsx(Month, {
            disabled: disabled,
            active: active,
            onClick: () => onSelect(value, newYear),
            children: value
          }, e);
        })
      })]
    })]
  });
};
// EXTERNAL MODULE: ./node_modules/react-icons/go/index.esm.js
var go_index_esm = __webpack_require__(6653);
;// CONCATENATED MODULE: ./components/banner/search-travels/styled.tsx
//# Styled components

const search_travels_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-0"
})(["width:100%;display:flex;flex-direction:column;row-gap:", ";background:", ";border-radius:", ";margin-top:", ";padding:", ";box-shadow:", ";padding-top:", ";padding-bottom:", ";", "{max-width:83.33333%;margin-top:", ";padding:", ";border-radius:", ";padding-left:", ";padding-right:", ";}", "{padding-left:", ";padding-right:", ";padding-bottom:", ";}"], ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(9), ({
  theme: {
    toRem
  }
}) => toRem(-100), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem,
    colors
  }
}) => `0 0 ${toRem(8)} ${colors.black.rgba.BK000(0.2)}`, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    media
  }
}) => media.to(0, 1024, true), ({
  theme: {
    toRem
  }
}) => toRem(-80), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(13), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    media
  }
}) => media.to(0, 568, true), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(24));
const Searching = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-1"
})(["width:100%;display:flex;align-items:center;gap:", ";", "{flex-direction:column;}"], ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    media
  }
}) => media.to(814));
const Search = /*#__PURE__*/external_styled_components_default().button.withConfig({
  componentId: "sc-mncr3n-2"
})(["width:100%;min-width:", ";background:", ";border-radius:", ";padding:", ";color:", ";font-size:", ";line-height:", ";cursor:pointer;transition:opacity 0.2s cubic-bezier(0.075,0.82,0.165,1);&:hover{opacity:0.92;}"], ({
  theme: {
    toRem
  }
}) => toRem(250), ({
  theme: {
    colors
  }
}) => colors.green.rgb.GR72A842, ({
  theme: {
    toRem
  }
}) => toRem(8), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(18), ({
  theme: {
    toRem
  }
}) => toRem(30));
const Filtering = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-3"
})(["width:100%;display:flex;align-items:center;flex-wrap:wrap;column-gap:", ";row-gap:", ";padding-left:", ";padding-right:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(24), ({
  theme: {
    toRem
  }
}) => toRem(10), ({
  theme: {
    toRem
  }
}) => toRem(12), ({
  theme: {
    toRem
  }
}) => toRem(12));
const FilterText = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-4"
})(["color:", ";font-size:", ";line-height:120%;font-weight:600;", "{display:block;width:100%;}"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16), ({
  theme: {
    media
  }
}) => media.to(814));
const CheckControl = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-5"
})(["display:flex;align-items:center;flex-wrap:nowrap;column-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(4));
const Check = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-mncr3n-6"
})(["display:flex;justify-content:center;align-items:center;width:", ";min-width:", ";height:", ";background:", ";border-radius:", ";border-width:", ";border-color:", ";border-style:solid;color:", ";font-size:", ";cursor:pointer;"], ({
  theme: {
    toRem
  }
}) => toRem(22), ({
  theme: {
    toRem
  }
}) => toRem(22), ({
  theme: {
    toRem
  }
}) => toRem(22), ({
  active,
  theme: {
    colors
  }
}) => active ? colors.black.rgb.BL000 : colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(4), ({
  theme: {
    toRem
  }
}) => toRem(2), ({
  active,
  theme: {
    colors
  }
}) => active ? colors.black.rgb.BL000 : colors.gray.rgb.GRYDDD, ({
  theme: {
    colors
  }
}) => colors.white.rgb.WFFF, ({
  theme: {
    toRem
  }
}) => toRem(18));
const Title = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-mncr3n-7"
})(["display:block;color:", ";font-size:", ";line-height:120%;font-weight:400;cursor:pointer;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BL000, ({
  theme: {
    toRem
  }
}) => toRem(16));
;// CONCATENATED MODULE: ./components/banner/search-travels/index.tsx
// React
 //# React hook form

 //# Components


 // React icons

 //# Styled components

 //# Types props



//# Component => search travels
const SearchTravels = ({}) => {
  //# Hooks
  const {
    control,
    handleSubmit
  } = (0,external_react_hook_form_.useForm)({
    mode: "all",
    defaultValues: {
      whereGoing: "",
      whenGo: "",
      groupTours: false,
      privateTours: false
    }
  }); //# Methods

  const onSubmit = async data => {
    console.log(data);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(search_travels_styled_Container, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Searching, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(WhereGoing, {
        rules: {
          required: true
        },
        control: control,
        name: "whereGoing"
      }), /*#__PURE__*/jsx_runtime_.jsx(WhenGo, {
        rules: {
          required: true
        },
        control: control,
        name: "whenGo"
      }), /*#__PURE__*/jsx_runtime_.jsx(Search, {
        onClick: handleSubmit(onSubmit),
        children: "Search"
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Filtering, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(FilterText, {
        children: "Filter by:"
      }), /*#__PURE__*/jsx_runtime_.jsx(search_travels_CheckControl, {
        title: "Group tours",
        control: control,
        name: "groupTours"
      }), /*#__PURE__*/jsx_runtime_.jsx(search_travels_CheckControl, {
        title: "Private tours",
        control: control,
        name: "privateTours"
      })]
    })]
  });
}; //# Types props

//# component => filter check
const search_travels_CheckControl = props => {
  //# Destroy
  const {
    title
  } = props; //# Hooks

  const {
    field
  } = (0,external_react_hook_form_.useController)(props); //# States

  const {
    0: active,
    1: setActive
  } = (0,external_react_.useState)(false); //# Methods

  const onCheck = () => {
    field.onChange(!active);
    setActive(!active);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(CheckControl, {
    onClick: onCheck,
    children: [/*#__PURE__*/jsx_runtime_.jsx(Check, {
      active: active,
      children: active && /*#__PURE__*/jsx_runtime_.jsx(go_index_esm/* GoCheck */.vyQ, {})
    }), /*#__PURE__*/jsx_runtime_.jsx(Title, {
      children: title
    })]
  });
};
// EXTERNAL MODULE: ./components/banner/styled.tsx
var styled = __webpack_require__(4886);
;// CONCATENATED MODULE: ./components/banner/index.tsx
//# React lazy load

 //# Styles components

 //# Types proops



//# Component => banner trip
const Banner = ({
  title,
  slogan,
  img
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled/* Container */.W2, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(styled/* Photo */.Pz, {
      children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
        effect: "black-and-white",
        width: "100%",
        height: "100%",
        src: img,
        title: title,
        alt: slogan
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(styled/* Content */.VY, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled/* Center */.M5, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(styled/* Title */.Dx, {
          children: title
        }), /*#__PURE__*/jsx_runtime_.jsx(styled/* Slogan */.ne, {
          children: slogan
        })]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(styled/* Searching */.aA, {
      children: /*#__PURE__*/jsx_runtime_.jsx(SearchTravels, {})
    })]
  });
};
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(8475);
;// CONCATENATED MODULE: ./components/top-trips/styled.tsx

const top_trips_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-s5w71p-0"
})(["width:100%;display:flex;flex-direction:column;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-s5w71p-1"
})(["position:relative;margin-top:3rem;color:", ";font-size:", ";line-height:120%;font-weight:800;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(24));
const Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-s5w71p-2"
})(["width:100%;padding-bottom:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(30));
// EXTERNAL MODULE: ./components/organisms/Trips-City/index.tsx + 3 modules
var Trips_City = __webpack_require__(8719);
;// CONCATENATED MODULE: ./components/top-trips/index.tsx
//# React
 //# Apollo
//# Translate

 //# Interfaces

//# Styles components


 //# Component => top trips



const TopTrips = () => {
  //# States
  const {
    trips
  } = (0,external_react_.useContext)(useContext/* HomeContext */.Q8);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("home");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(top_trips_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Title, {
      children: t("top_tours")
    }), /*#__PURE__*/jsx_runtime_.jsx(Content, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Trips_City/* default */.Z, {
        trips: trips
      })
    })]
  });
}; //# Responsive

const responsive = [{
  media: 1184,
  space: 16,
  firstMargin: 24,
  size: 0.25
}, {
  media: 1024,
  space: 8,
  firstMargin: 24,
  size: 0.33333
}, {
  media: 767,
  space: 8,
  firstMargin: 24,
  size: 0.66667
}];
// EXTERNAL MODULE: ./components/scroll-snap/index.tsx + 1 modules
var scroll_snap = __webpack_require__(1809);
;// CONCATENATED MODULE: ./components/card/attraction/styled.tsx

const Box = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17fc0nw-0"
})(["height:120px;cursor:pointer;overflow:hidden;position:relative;display:grid;grid-template-columns:1fr 2fr;"]);
const styled_Image = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17fc0nw-1"
})([".lazy-load-image-background img{object-fit:cover;border-radius:14px;}"]);
const attraction_styled_Title = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-17fc0nw-2"
})(["top:0;display:flex;flex-direction:column;align-items:flex-start;height:100%;padding:15px;z-index:3;"]);
const styled_Name = /*#__PURE__*/external_styled_components_default().h3.withConfig({
  componentId: "sc-17fc0nw-3"
})(["font-size:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(23));
const Count = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-17fc0nw-4"
})(["font-weight:600;"]);
;// CONCATENATED MODULE: ./components/card/attraction/index.tsx






const Attraction = ({
  attraction
}) => {
  const {
    Image,
    Name,
    Trips,
    Url
  } = attraction;
  const router = (0,router_.useRouter)();

  const onRoute = e => {
    e.preventDefault();
    router.push(Url);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Box, {
    onClick: onRoute,
    children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Image, {
      children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
        effect: "blur",
        width: "100%",
        height: "100px",
        src: Image
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(attraction_styled_Title, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(styled_Name, {
        children: Name
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Count, {
        children: [Trips, " Tours"]
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/top-attractions/styled.tsx

const top_attractions_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fxnaif-0"
})(["width:100%;display:flex;background:white;flex-direction:column;paddin-top:2rem;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const top_attractions_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-1fxnaif-1"
})(["position:relative;margin-top:3rem;color:", ";font-size:", ";line-height:120%;font-weight:800;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(24));
const styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-1fxnaif-2"
})(["width:100%;display:flex;align-items:center;justify-content:flex-start;"]);
// EXTERNAL MODULE: ./themes/global/index.ts
var global = __webpack_require__(5812);
;// CONCATENATED MODULE: ./components/top-attractions/index.tsx
//# React
 //# Apollo

 //# Interfaces

//# Components

 //# Styles components



 //# Component => top trips



const TopAttractions = () => {
  const {
    atractions
  } = (0,external_react_.useContext)(useContext/* HomeContext */.Q8);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("home");
  return /*#__PURE__*/jsx_runtime_.jsx(top_attractions_styled_Container, {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(global/* Center */.M5, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(top_attractions_styled_Title, {
        children: t("top_attractions")
      }), /*#__PURE__*/jsx_runtime_.jsx(styled_Content, {
        children: /*#__PURE__*/jsx_runtime_.jsx(scroll_snap/* ScrollSnap */.a, {
          defaultSize: 0.33,
          responsive: top_attractions_responsive,
          children: atractions.map((attraction, e) => {
            return /*#__PURE__*/jsx_runtime_.jsx(Attraction, {
              attraction: attraction
            }, e);
          })
        })
      })]
    })
  });
}; //# Responsive

const top_attractions_responsive = [{
  media: 1184,
  space: 16,
  firstMargin: 24,
  size: 0.25
}, {
  media: 1024,
  space: 8,
  firstMargin: 24,
  size: 0.33333
}, {
  media: 767,
  space: 8,
  firstMargin: 24,
  size: 0.66667
}];
;// CONCATENATED MODULE: ./components/card/city/styled.tsx

const styled_Box = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-xalq4a-0"
})(["border-radius:14px;box-shadow:rgb(152 167 204 / 12%) 0px 3px 3px -2px,rgb(152 167 204 / 14%) 0px 3px 4px 0px,rgb(152 167 204 / 20%) 0px 1px 8px 0px;display:block;height:230px;cursor:pointer;overflow:hidden;position:relative;"]);
const city_styled_Image = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-xalq4a-1"
})(["position:absolute;z-index:1;width:100%;height:230px;.lazy-load-image-background{width:100%;height:100%;img{object-fit:cover;}}"]);
const city_styled_Title = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-xalq4a-2"
})(["top:0;color:white;background-image:linear-gradient(rgba(32,27,27,0.062),rgba(0,0,0,0.478));display:flex;justify-content:space-between;align-items:flex-end;height:100%;padding:15px;position:relative;z-index:1;"]);
const city_styled_Name = /*#__PURE__*/external_styled_components_default().h3.withConfig({
  componentId: "sc-xalq4a-3"
})(["font-size:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(23));
const styled_Count = /*#__PURE__*/external_styled_components_default().span.withConfig({
  componentId: "sc-xalq4a-4"
})(["font-weight:600;"]);
;// CONCATENATED MODULE: ./components/card/city/index.tsx





const City = ({
  city
}) => {
  const {
    Image,
    Name,
    Trips,
    Url
  } = city;
  const router = (0,router_.useRouter)();

  const onRoute = () => {
    router.push(Url);
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Box, {
    onClick: onRoute,
    children: [/*#__PURE__*/jsx_runtime_.jsx(city_styled_Image, {
      children: /*#__PURE__*/jsx_runtime_.jsx(external_react_lazy_load_image_component_.LazyLoadImage, {
        effect: "blur",
        width: "100%",
        height: "100%",
        src: Image
      })
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(city_styled_Title, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(city_styled_Name, {
        children: Name
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Count, {
        children: [Trips, " Tours"]
      })]
    })]
  });
};
;// CONCATENATED MODULE: ./components/top-destinations/styled.tsx

const top_destinations_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-h8awf8-0"
})(["width:100%;display:flex;flex-direction:column;paddin-top:2rem;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const top_destinations_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-h8awf8-1"
})(["position:relative;margin-top:3rem;color:", ";font-size:", ";line-height:120%;font-weight:800;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(24));
const top_destinations_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-h8awf8-2"
})(["width:100%;display:flex;align-items:center;justify-content:flex-start;"]);
;// CONCATENATED MODULE: ./components/top-destinations/index.tsx
//# React
 //# Interfaces

//# Components

 //# Styles components

 //# hooks

 //# Translate




//# Component => top trips
const TopCities = () => {
  const {
    cities
  } = (0,external_react_.useContext)(useContext/* HomeContext */.Q8);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("home");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(top_destinations_styled_Container, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(top_destinations_styled_Title, {
      children: t("top_destination")
    }), /*#__PURE__*/jsx_runtime_.jsx(top_destinations_styled_Content, {
      children: /*#__PURE__*/jsx_runtime_.jsx(scroll_snap/* ScrollSnap */.a, {
        defaultSize: 0.33,
        responsive: top_destinations_responsive,
        children: cities.map((city, e) => {
          return /*#__PURE__*/jsx_runtime_.jsx(City, {
            city: city
          }, e);
        })
      })
    })]
  });
}; //# Responsive

const top_destinations_responsive = [{
  media: 1184,
  space: 16,
  firstMargin: 24,
  size: 0.25
}, {
  media: 1024,
  space: 8,
  firstMargin: 24,
  size: 0.33333
}, {
  media: 767,
  space: 8,
  firstMargin: 24,
  size: 0.66667
}];
;// CONCATENATED MODULE: ./components/top-styles/styled.tsx

const top_styles_styled_Container = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-13j8i34-0"
})(["width:100%;display:flex;background:white;flex-direction:column;paddin-top:2rem;row-gap:", ";"], ({
  theme: {
    toRem
  }
}) => toRem(16));
const top_styles_styled_Title = /*#__PURE__*/external_styled_components_default().h2.withConfig({
  componentId: "sc-13j8i34-1"
})(["position:relative;margin-top:3rem;color:", ";font-size:", ";line-height:120%;font-weight:800;text-align:center;"], ({
  theme: {
    colors
  }
}) => colors.black.rgb.BLk2C3E50, ({
  theme: {
    toRem
  }
}) => toRem(24));
const top_styles_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-13j8i34-2"
})(["width:100%;display:flex;align-items:center;justify-content:flex-start;"]);
;// CONCATENATED MODULE: ./components/top-styles/index.tsx
//# React
 //# hooks

 //# Translate

 //# Interfaces

//# Components
 //# Styles components



 //# Component => top trips



const TopStyles = () => {
  const {
    styles
  } = (0,external_react_.useContext)(useContext/* HomeContext */.Q8);
  const {
    t
  } = (0,external_next_i18next_.useTranslation)("home");
  return /*#__PURE__*/jsx_runtime_.jsx(top_styles_styled_Container, {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(global/* Center */.M5, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(top_styles_styled_Title, {
        children: [t("top_styles"), " "]
      }), /*#__PURE__*/jsx_runtime_.jsx(top_styles_styled_Content, {
        children: /*#__PURE__*/jsx_runtime_.jsx(scroll_snap/* ScrollSnap */.a, {
          defaultSize: 0.33,
          responsive: top_styles_responsive,
          children: styles.map((style, e) => {
            return /*#__PURE__*/jsx_runtime_.jsx(Attraction, {
              attraction: style
            }, e);
          })
        })
      })]
    })
  });
}; //# Responsive

const top_styles_responsive = [{
  media: 1184,
  space: 16,
  firstMargin: 24,
  size: 0.25
}, {
  media: 1024,
  space: 8,
  firstMargin: 24,
  size: 0.33333
}, {
  media: 767,
  space: 8,
  firstMargin: 24,
  size: 0.66667
}];
// EXTERNAL MODULE: ./apollo/index.ts
var apollo = __webpack_require__(6635);
// EXTERNAL MODULE: ./gql/city/query.ts
var city_query = __webpack_require__(3557);
// EXTERNAL MODULE: ./gql/trip/query.ts
var trip_query = __webpack_require__(9607);
// EXTERNAL MODULE: external "next/error"
var error_ = __webpack_require__(8354);
var error_default = /*#__PURE__*/__webpack_require__.n(error_);
// EXTERNAL MODULE: ./gql/styles/query.ts
var styles_query = __webpack_require__(6107);
// EXTERNAL MODULE: ./gql/attraction/query.ts
var attraction_query = __webpack_require__(8517);
// EXTERNAL MODULE: ./components/layout/index.tsx + 24 modules
var layout = __webpack_require__(39);
// EXTERNAL MODULE: ./gql/hrflag/query.ts
var hrflag_query = __webpack_require__(719);
;// CONCATENATED MODULE: ./components/molecules/Highlighted/styled.tsx

const Layout = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-d2uavh-0"
})(["display:grid;", "{grid-template-columns:1fr;margin-top:16rem;}grid-gap:1rem;", "{grid-template-columns:repeat(auto-fit,minmax(250px,1fr));margin-top:5rem;}"], ({
  theme
}) => theme.breakpoints.sm, ({
  theme
}) => theme.breakpoints.md);
const Highlighted_styled_Content = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-d2uavh-1"
})(["display:grid;grid-template-columns:3fr 6fr;"]);
const Image = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-d2uavh-2"
})(["padding:10px;"]);
const styled_Text = /*#__PURE__*/external_styled_components_default().div.withConfig({
  componentId: "sc-d2uavh-3"
})(["display:flex;align-items:center;"]);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: external "react-i18next"
var external_react_i18next_ = __webpack_require__(7789);
;// CONCATENATED MODULE: ./components/molecules/Highlighted/index.tsx






const Highlighted = () => {
  const {
    t
  } = (0,external_react_i18next_.useTranslation)("common");
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Layout, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)(Highlighted_styled_Content, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Image, {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          width: "85%",
          height: "100%",
          src: "https://www.conde.travel/wp-content/uploads/2019/09/ico-amiglabes.svg",
          alt: "conde travel"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(styled_Text, {
        children: /*#__PURE__*/jsx_runtime_.jsx("h3", {
          children: t("hinglight.ambiente")
        })
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Highlighted_styled_Content, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Image, {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          width: "85%",
          height: "100%",
          src: "https://www.conde.travel/wp-content/uploads/2019/09/ico-respeto.svg",
          alt: "conde travel"
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Text, {
        children: [" ", /*#__PURE__*/jsx_runtime_.jsx("h3", {
          children: t("hinglight.respet")
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Highlighted_styled_Content, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Image, {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          width: "85%",
          height: "100%",
          src: "https://www.conde.travel/wp-content/uploads/2019/09/ico-conciencia.svg",
          alt: "conde travel"
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Text, {
        children: [" ", /*#__PURE__*/jsx_runtime_.jsx("h3", {
          children: t("hinglight.cultura")
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Highlighted_styled_Content, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Image, {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          width: "85%",
          height: "100%",
          src: "https://www.conde.travel/wp-content/uploads/2019/09/ico-aventura.svg",
          alt: "conde travel"
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(styled_Text, {
        children: [" ", /*#__PURE__*/jsx_runtime_.jsx("h3", {
          children: t("hinglight.aventura")
        })]
      })]
    })]
  });
};

/* harmony default export */ const molecules_Highlighted = (Highlighted);
;// CONCATENATED MODULE: ./components/molecules/index.ts

;// CONCATENATED MODULE: ./pages/home/index.tsx
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# Next





 //# Global




 //# Gql -


 //# hooks












//# Page => home
const Home = ({
  cities,
  trips,
  styles,
  atractions,
  home,
  languages,
  fallback
}) => {
  if (fallback) return /*#__PURE__*/jsx_runtime_.jsx((error_default()), {
    statusCode: 404
  });
  const {
    Slug,
    Title,
    Image,
    DecriptionGoogle,
    TitleGoogle
  } = home;
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(useContext/* HomeContext.Provider */.Q8.Provider, {
    value: {
      cities,
      trips,
      styles,
      atractions
    },
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("title", {
        children: [TitleGoogle, " - Conde Travel"]
      }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
        name: "description",
        content: DecriptionGoogle
      }), /*#__PURE__*/jsx_runtime_.jsx("link", {
        rel: "icon",
        href: "/favicon.ico"
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx(useContext/* HeaderContext.Provider */.pI.Provider, {
      value: {
        languages
      },
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(layout/* default */.Z, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Banner, {
          title: Title,
          slogan: Slug,
          img: Image
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(global/* Center */.M5, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(molecules_Highlighted, {}), /*#__PURE__*/jsx_runtime_.jsx(TopCities, {}), /*#__PURE__*/jsx_runtime_.jsx(TopTrips, {})]
        }), /*#__PURE__*/jsx_runtime_.jsx(TopAttractions, {}), /*#__PURE__*/jsx_runtime_.jsx(TopStyles, {})]
      })
    })]
  });
};

const getServerSideProps = async ({
  query
}) => {
  var _await$client$query$d, _await$client$query$d2, _await$client$query$d3, _await$client$query$d4, _await$client$query$d5, _await$client$query$d6, _await$client$query$d7, _await$client$query$d8, _await$client$query$d9, _await$client$query$d10, _await$client$query$d11, _await$client$query$d12;

  const locale = (query === null || query === void 0 ? void 0 : query.locale) || "en";
  const client = (0,apollo/* initializeApollo */.in)();
  const home = await ((_await$client$query$d = (await client.query({
    query: querys/* GET_HOME */.Yk,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d === void 0 ? void 0 : (_await$client$query$d2 = _await$client$query$d.getLanguageHome) === null || _await$client$query$d2 === void 0 ? void 0 : _await$client$query$d2.data);

  if (home === null) {
    return {
      props: {
        fallback: true
      }
    };
  }

  const cities = await ((_await$client$query$d3 = (await client.query({
    query: city_query/* GET_CITIES_HOME */.pd,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d3 === void 0 ? void 0 : (_await$client$query$d4 = _await$client$query$d3.getCitieshome) === null || _await$client$query$d4 === void 0 ? void 0 : _await$client$query$d4.data);
  const trips = await ((_await$client$query$d5 = (await client.query({
    query: trip_query/* GET_TRIPS_HOME */.Ew,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d5 === void 0 ? void 0 : (_await$client$query$d6 = _await$client$query$d5.getTripshome) === null || _await$client$query$d6 === void 0 ? void 0 : _await$client$query$d6.data);
  const styles = await ((_await$client$query$d7 = (await client.query({
    query: styles_query/* GET_STYLES_HOME */.SC,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d7 === void 0 ? void 0 : (_await$client$query$d8 = _await$client$query$d7.getStylesHome) === null || _await$client$query$d8 === void 0 ? void 0 : _await$client$query$d8.data);
  const atractions = await ((_await$client$query$d9 = (await client.query({
    query: attraction_query/* GET_ATTRACTIONS_HOME */.XK,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d9 === void 0 ? void 0 : (_await$client$query$d10 = _await$client$query$d9.getAttactionshome) === null || _await$client$query$d10 === void 0 ? void 0 : _await$client$query$d10.data);
  const languages = await ((_await$client$query$d11 = (await client.query({
    query: hrflag_query/* GET_HREFLA_HOME */.G,
    variables: {
      slug: locale
    }
  })).data) === null || _await$client$query$d11 === void 0 ? void 0 : (_await$client$query$d12 = _await$client$query$d11.gethrefhome) === null || _await$client$query$d12 === void 0 ? void 0 : _await$client$query$d12.data);
  return {
    props: _objectSpread(_objectSpread({}, await (0,serverSideTranslations_.serverSideTranslations)(locale, ["home", "common"])), {}, {
      cities,
      trips,
      styles,
      atractions,
      home,
      languages
    })
  };
};
/* harmony default export */ const home = (Home);

/***/ }),

/***/ 8074:
/***/ ((module) => {

module.exports = require("@apollo/client");

/***/ }),

/***/ 1141:
/***/ ((module) => {

module.exports = require("deepmerge");

/***/ }),

/***/ 1071:
/***/ ((module) => {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 8475:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 3295:
/***/ ((module) => {

module.exports = require("next-i18next/serverSideTranslations");

/***/ }),

/***/ 247:
/***/ ((module) => {

module.exports = require("next-routes");

/***/ }),

/***/ 822:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 8354:
/***/ ((module) => {

module.exports = require("next/error");

/***/ }),

/***/ 701:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 6731:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 9297:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 62:
/***/ ((module) => {

module.exports = require("react-cool-onclickoutside");

/***/ }),

/***/ 2662:
/***/ ((module) => {

module.exports = require("react-hook-form");

/***/ }),

/***/ 7789:
/***/ ((module) => {

module.exports = require("react-i18next");

/***/ }),

/***/ 9290:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 9997:
/***/ ((module) => {

module.exports = require("react-modal");

/***/ }),

/***/ 5282:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 9914:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [675,108,434,653,254,844,152,607,585,517,107], () => (__webpack_exec__(4389)));
module.exports = __webpack_exports__;

})();
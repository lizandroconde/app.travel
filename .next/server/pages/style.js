"use strict";
(() => {
var exports = {};
exports.id = 472;
exports.ids = [472];
exports.modules = {

/***/ 3355:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "j": () => (/* binding */ Banner)
/* harmony export */ });
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7789);
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_i18next__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9290);
/* harmony import */ var react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styled__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4886);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);
//# React lazy load

 //# Styles components

 //# Types proops



//# Component => banner trip
const Banner = ({
  title,
  img
}) => {
  const {
    t
  } = (0,react_i18next__WEBPACK_IMPORTED_MODULE_0__.useTranslation)("common");
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(_styled__WEBPACK_IMPORTED_MODULE_2__/* .Container */ .W2, {
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(_styled__WEBPACK_IMPORTED_MODULE_2__/* .Photo */ .Pz, {
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(react_lazy_load_image_component__WEBPACK_IMPORTED_MODULE_1__.LazyLoadImage, {
        effect: "black-and-white",
        width: "100%",
        height: "100%",
        src: img,
        title: title,
        alt: title
      })
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(_styled__WEBPACK_IMPORTED_MODULE_2__/* .Content */ .VY, {
      background: true,
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(_styled__WEBPACK_IMPORTED_MODULE_2__/* .Center */ .M5, {
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(_styled__WEBPACK_IMPORTED_MODULE_2__/* .Title */ .Dx, {
          children: [" ", t("titles.city"), " ", title]
        })
      })
    })]
  });
};

/***/ }),

/***/ 3170:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getServerSideProps": () => (/* binding */ getServerSideProps),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3295);
/* harmony import */ var next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(701);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _apollo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6635);
/* harmony import */ var _themes_global__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5812);
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(39);
/* harmony import */ var _hooks_useContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(960);
/* harmony import */ var _components_banner_city__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3355);
/* harmony import */ var _components_organisms_Trips_City__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8719);
/* harmony import */ var _gql_styles_query__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6107);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(8354);
/* harmony import */ var next_error__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_error__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Next

 // Apollo client

 // Interfaces

// Components
// Global










//# Page => Trip
const Trip = ({
  info,
  activities,
  fallback,
  languages
}) => {
  var _info$Image;

  if (fallback) return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx((next_error__WEBPACK_IMPORTED_MODULE_9___default()), {
    statusCode: 404
  });
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_hooks_useContext__WEBPACK_IMPORTED_MODULE_5__/* .HeaderContext.Provider */ .pI.Provider, {
    value: {
      languages
    },
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)(_components_layout__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxs)("title", {
          children: [info === null || info === void 0 ? void 0 : info.Title, " - Conde Travel"]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx("meta", {
          name: "description",
          content: info === null || info === void 0 ? void 0 : info.Description
        })]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_components_banner_city__WEBPACK_IMPORTED_MODULE_6__/* .Banner */ .j, {
        title: info === null || info === void 0 ? void 0 : info.Name,
        img: info === null || info === void 0 ? void 0 : (_info$Image = info.Image) === null || _info$Image === void 0 ? void 0 : _info$Image.url
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx("main", {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_themes_global__WEBPACK_IMPORTED_MODULE_3__/* .Center */ .M5, {
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_10__.jsx(_components_organisms_Trips_City__WEBPACK_IMPORTED_MODULE_7__/* .default */ .Z, {
            trips: activities
          })
        })
      })]
    })
  });
};

const getServerSideProps = async ({
  query: {
    locale,
    type
  }
}) => {
  var _await$client$query$d, _await$client$query$d2, _await$client$query$d3, _await$client$query$d4, _await$client$query$d5, _await$client$query$d6;

  const client = (0,_apollo__WEBPACK_IMPORTED_MODULE_2__/* .initializeApollo */ .in)();
  const info = await ((_await$client$query$d = (await client.query({
    query: _gql_styles_query__WEBPACK_IMPORTED_MODULE_8__/* .GET_ONE_STYLE */ ._o,
    variables: {
      slug: locale,
      type
    }
  })).data) === null || _await$client$query$d === void 0 ? void 0 : (_await$client$query$d2 = _await$client$query$d.getOneStyle) === null || _await$client$query$d2 === void 0 ? void 0 : _await$client$query$d2.data);

  if (info === null) {
    return {
      props: {
        fallback: true
      }
    };
  }

  const activities = await ((_await$client$query$d3 = (await client.query({
    query: _gql_styles_query__WEBPACK_IMPORTED_MODULE_8__/* .GET_ALL_TRIP_STYLE */ .WT,
    variables: {
      slug: locale,
      type
    }
  })).data) === null || _await$client$query$d3 === void 0 ? void 0 : (_await$client$query$d4 = _await$client$query$d3.getTripsStyle) === null || _await$client$query$d4 === void 0 ? void 0 : _await$client$query$d4.data);
  const languages = await ((_await$client$query$d5 = (await client.query({
    query: _gql_styles_query__WEBPACK_IMPORTED_MODULE_8__/* .GET_HREFLA_STYLE */ .N6,
    variables: {
      slug: locale,
      type
    }
  })).data) === null || _await$client$query$d5 === void 0 ? void 0 : (_await$client$query$d6 = _await$client$query$d5.getStyleHrefla) === null || _await$client$query$d6 === void 0 ? void 0 : _await$client$query$d6.data);
  return {
    props: _objectSpread(_objectSpread({}, await (0,next_i18next_serverSideTranslations__WEBPACK_IMPORTED_MODULE_0__.serverSideTranslations)(locale, ["trip", "common"])), {}, {
      info,
      activities,
      languages
    })
  };
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Trip);

/***/ }),

/***/ 8074:
/***/ ((module) => {

module.exports = require("@apollo/client");

/***/ }),

/***/ 1141:
/***/ ((module) => {

module.exports = require("deepmerge");

/***/ }),

/***/ 1071:
/***/ ((module) => {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 8475:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 3295:
/***/ ((module) => {

module.exports = require("next-i18next/serverSideTranslations");

/***/ }),

/***/ 247:
/***/ ((module) => {

module.exports = require("next-routes");

/***/ }),

/***/ 822:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 8354:
/***/ ((module) => {

module.exports = require("next/error");

/***/ }),

/***/ 701:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 6731:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 9297:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 62:
/***/ ((module) => {

module.exports = require("react-cool-onclickoutside");

/***/ }),

/***/ 2662:
/***/ ((module) => {

module.exports = require("react-hook-form");

/***/ }),

/***/ 7789:
/***/ ((module) => {

module.exports = require("react-i18next");

/***/ }),

/***/ 9290:
/***/ ((module) => {

module.exports = require("react-lazy-load-image-component");

/***/ }),

/***/ 9997:
/***/ ((module) => {

module.exports = require("react-modal");

/***/ }),

/***/ 5282:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 9914:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [675,108,254,844,152,107], () => (__webpack_exec__(3170)));
module.exports = __webpack_exports__;

})();
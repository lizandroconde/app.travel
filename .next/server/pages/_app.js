"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 5502:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// NAMESPACE OBJECT: ./themes/colors/index.ts
var colors_namespaceObject = {};
__webpack_require__.r(colors_namespaceObject);
__webpack_require__.d(colors_namespaceObject, {
  "black": () => (black),
  "blue": () => (blue),
  "gray": () => (gray),
  "green": () => (green),
  "orange": () => (orange),
  "white": () => (white),
  "yellow": () => (yellow)
});

// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(5675);
// EXTERNAL MODULE: ./server/routes.ts
var routes = __webpack_require__(2914);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(9914);
// EXTERNAL MODULE: ./themes/global/index.ts
var global = __webpack_require__(5812);
;// CONCATENATED MODULE: ./themes/colors/index.ts
//# Color => black
const black = {
  rgb: {
    BL000: "#000",
    BLk2C3E50: "#2C3E50",
    BLK555: "#555",
    BLK4A5A5A: "#4A5A5A"
  },
  rgba: {
    BK000: deg => `rgba(0,0,0,${deg})`
  }
}; //# Color => white

const white = {
  rgb: {
    WFFF: "#FFF"
  },
  rgba: {
    W255: deg => `rgb(255, 255, 255, ${deg})`
  }
}; //# Color => orange

const orange = {
  rgb: {
    OGFF946D: "#FF946D",
    OF28023: "#F28023"
  }
}; //# Color => green

const green = {
  rgb: {
    GR72A842: "#72A842"
  }
}; //# Color => gray

const gray = {
  rgb: {
    GRYEBEEF2: "#EBEEF2",
    GRYC7D0D9: "#C7D0D9",
    GRYDDD: "#DDD",
    GRYF2F3F5: "#F2F3F5",
    GRYCCC: "#CCC",
    GRY151743: "rgb(115,117,143)",
    GRYE0E0E0: "#E0E0E0",
    GRY9facba: "#9facba",
    GRYe4e8ec: "#e4e8ec",
    GRYf5f6f9: "#f5f6f9",
    GRY81829c: "#81829c",
    GRY9aa7b5: "#9aa7b5",
    GRYa6b2bf: "#a6b2bf",
    GR23501: "rgba(235, 235, 235, 1)"
  }
}; //# Color => blue

const blue = {
  rgb: {
    BL286283: "#286283",
    BL4775250: "rgb(47,75,250)",
    BL35aafa: "#35aafa",
    BL3b556a: "#3b556a"
  }
}; //# Color => yellow

const yellow = {
  rgb: {
    YWffe958: "#ffe958"
  }
};
;// CONCATENATED MODULE: ./themes/media/index.ts
//# Media query of responsive
const media = (maxWidth, minWidth, min, all) => {
  if (min) {
    return `@media screen and (min-width: ${minWidth}px)`;
  } else if (all) {
    return `@media screen and (max-width: ${maxWidth}px) and (min-width: ${minWidth}px)`;
  } else {
    return `@media screen and (max-width: ${maxWidth}px)`;
  }
}; //# Export the options Media querys


const MediaQuery = {
  to: (maxWidth = 0, minWidth = 0, min = false, all = false) => {
    return media(maxWidth, minWidth, min, all);
  }
};
;// CONCATENATED MODULE: ./themes/index.ts
//# Export theme type => theme private
//# Import the all colors
 //# Media

 //# Import the all box shadows

const ThemePrivate = {
  media: MediaQuery,
  colors: colors_namespaceObject,
  toRem: px => {
    const REM = 1;
    const PX = 16;
    const rem = (px * REM / PX).toFixed(3);
    return `${Number(rem)}rem`;
  },
  spaces: number => {
    const value = number * 8 + 'px';
    return value;
  },
  breakpoints: {
    sm: '@media (max-width: 600px)',
    md: '@media (min-width: 601px)',
    ll: '@media (min-width: 905px)',
    lg: '@media (min-width: 1240px)'
  }
};
;// CONCATENATED MODULE: external "nprogress"
const external_nprogress_namespaceObject = require("nprogress");
var external_nprogress_default = /*#__PURE__*/__webpack_require__.n(external_nprogress_namespaceObject);
;// CONCATENATED MODULE: external "next-google-fonts"
const external_next_google_fonts_namespaceObject = require("next-google-fonts");
// EXTERNAL MODULE: external "next-i18next"
var external_next_i18next_ = __webpack_require__(8475);
// EXTERNAL MODULE: external "@apollo/client"
var client_ = __webpack_require__(8074);
// EXTERNAL MODULE: ./apollo/index.ts
var apollo = __webpack_require__(6635);
// EXTERNAL MODULE: external "simple-react-lightbox"
var external_simple_react_lightbox_ = __webpack_require__(2175);
var external_simple_react_lightbox_default = /*#__PURE__*/__webpack_require__.n(external_simple_react_lightbox_);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./pages/_app.tsx
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//# Next App

 //# Styled components

 //# Global styled



 //nprogress module

 //styles of nprogress
//# Google fonts


 //# Apollo


 //# Styles lazy load





routes/* Router.events.on */.F0.events.on('routeChangeStart', () => external_nprogress_default().start());
routes/* Router.events.on */.F0.events.on('routeChangeComplete', () => external_nprogress_default().done());
routes/* Router.events.on */.F0.events.on('routeChangeError', () => external_nprogress_default().done()); //# Page => App

const MyApp = ({
  Component,
  pageProps
}) => {
  //# Apollo client
  const apolloClient = (0,apollo/* useApollo */.Uk)(pageProps);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(client_.ApolloProvider, {
    client: apolloClient,
    children: [/*#__PURE__*/jsx_runtime_.jsx((external_simple_react_lightbox_default()), {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_styled_components_.ThemeProvider, {
        theme: ThemePrivate,
        children: [/*#__PURE__*/jsx_runtime_.jsx(global/* GlobalStyle */.ZL, {}), /*#__PURE__*/jsx_runtime_.jsx(external_next_google_fonts_namespaceObject.GoogleFonts, {
          href: "https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;500;600;700;800;900&display=swap"
        }), /*#__PURE__*/jsx_runtime_.jsx(Component, _objectSpread({}, pageProps))]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(global/* SocialIcons */.HE, {
      children: /*#__PURE__*/jsx_runtime_.jsx("a", {
        href: "https://api.whatsapp.com/send?1=pt_BR&phone=51984603305&text=Hi!%20Conde%20Travel",
        target: "_blank",
        rel: "noreferrer",
        children: /*#__PURE__*/jsx_runtime_.jsx(next_image.default, {
          src: "https://images.conde.travel/assets/whatsapp.png",
          alt: "whatsapp",
          width: "100%",
          height: "100%"
        })
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      style: {
        maxHeight: "0px",
        overflow: "hidden"
      },
      children: /*#__PURE__*/jsx_runtime_.jsx("script", {
        src: "https://cdn.wetravel.com/widgets/embed_calendar.js",
        "data-env": "https://www.wetravel.com",
        async: true
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("script", {
      dangerouslySetInnerHTML: {
        __html: ` window.__lc = window.__lc || {};
              window.__lc.license = 13051974;
              ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))`
      }
    })]
  });
};

/* harmony default export */ const _app = ((0,external_next_i18next_.appWithTranslation)(MyApp));

/***/ }),

/***/ 8074:
/***/ ((module) => {

module.exports = require("@apollo/client");

/***/ }),

/***/ 1141:
/***/ ((module) => {

module.exports = require("deepmerge");

/***/ }),

/***/ 1071:
/***/ ((module) => {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 8475:
/***/ ((module) => {

module.exports = require("next-i18next");

/***/ }),

/***/ 247:
/***/ ((module) => {

module.exports = require("next-routes");

/***/ }),

/***/ 822:
/***/ ((module) => {

module.exports = require("next/dist/server/image-config.js");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/head.js");

/***/ }),

/***/ 556:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/to-base-64.js");

/***/ }),

/***/ 9297:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 5282:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 2175:
/***/ ((module) => {

module.exports = require("simple-react-lightbox");

/***/ }),

/***/ 9914:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [675,254], () => (__webpack_exec__(5502)));
module.exports = __webpack_exports__;

})();
//# React
import { useMemo } from "react";
//# Apollo
import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  concat,
  InMemoryCache,
  NormalizedCacheObject,
} from "@apollo/client";
import fetch from "isomorphic-unfetch";
import merge from "deepmerge";
//# Http link connection
const ENPOINT: string = process.env.ENPOINT as string;
const httpLink: HttpLink = new HttpLink({ uri: ENPOINT, fetch });
//# Middleware from added authorization to the headers
const authMiddleware = new ApolloLink((operation, forward) => {
  //# Add the authorization to the headers
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
    },
  }));
  return forward(operation);
});
//# Create apollo client
export const APOLLO_STATE_PROP_NAME = "__APOLLO_STATE__";
let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;
const createApolloClient = () => {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: concat(authMiddleware, httpLink),
    cache: new InMemoryCache({
      addTypename: true,
      resultCaching: true,
    }).restore({}),
  });
};
//# Initial apollo in pages
export const initializeApollo = (
  initialState: NormalizedCacheObject | null = null
): ApolloClient<NormalizedCacheObject> => {
  const _apolloClient = apolloClient ?? createApolloClient();
  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache: NormalizedCacheObject = _apolloClient.extract();
    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache);
    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) {
    apolloClient = _apolloClient;
  }
  return _apolloClient;
};
//# Use apollo in _app
type IApolloProps = {
  __APOLLO_STATE__: NormalizedCacheObject | null | undefined;
};
export const useApollo = (
  pageProps: IApolloProps
): ApolloClient<NormalizedCacheObject> => {
  const state: NormalizedCacheObject | null | undefined =
    pageProps[APOLLO_STATE_PROP_NAME];
  const store: ApolloClient<NormalizedCacheObject> = useMemo(
    () => initializeApollo(state),
    [state]
  );
  return store;
};
